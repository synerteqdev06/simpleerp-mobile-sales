package com.synerteqsys.simpleerpmobilesales.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.synerteqsys.simpleerpmobilesales.R;
import com.synerteqsys.simpleerpmobilesales.data.UserPreference;
import com.synerteqsys.simpleerpmobilesales.data.models.User;
import com.synerteqsys.simpleerpmobilesales.utils.ActionDoneListener;
import com.synerteqsys.simpleerpmobilesales.utils.Constants;
import com.synerteqsys.simpleerpmobilesales.utils.LoginTask;
import com.synerteqsys.simpleerpmobilesales.utils.database.UserHelper;

/**
 * A login screen that offers login via username/password.
 */
public class LoginActivity extends AppCompatActivity {
    private EditText email;
    private EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        email.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    password.requestFocus();
                    return true;
                }
                return false;
            }
        });
        //TODO: once cookies are stored in disk, check if user is logged in or not
//        Context appContext = getApplicationContext();
//        //check if the user is still logged in
//        if(UserPreference.getBoolPreference(appContext, Constants.LOGGED_IN)){
//            Intent intent = new Intent(this, MainMenuActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(intent);
//            finish();
//        }
        UserPreference.clearPreferences(getApplicationContext());
    }

    public void loginClick(View v) {
        String suser = email.getText().toString();
        String spassword = password.getText().toString();
        Log.d(Constants.TAG, "signing in");
        if (suser.length() == 0 || spassword.length() == 0) {
            Toast.makeText(getApplicationContext(), getString(R.string.username_password_mandatory), Toast.LENGTH_SHORT).show();
        } else {
            logIn(suser, spassword);
        }
    }

    public void logIn(final String username, final String pword) {
        ProgressDialog pd = new ProgressDialog(this);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setMessage(getText(R.string.login_progress));
        pd.setIndeterminate(true);
        pd.setCancelable(false);

        LoginTask task = new LoginTask(this, username, pword);
        task.setProgressDialog(pd);
        final Activity self = this;
        task.setPostActionListener(new ActionDoneListener() {
            @Override
            public void handleDone(Object o) {
            Log.d(Constants.TAG, "Login Task Complete");
            Context appContext = getApplicationContext();
            User user = UserHelper.searchUser(appContext, username);

            // try logging in the user locally if they can't log in online
            if(UserPreference.getStringPreference(appContext, Constants.MODE).equals(Constants.OFFLINE)){
                boolean loggedIn = user.matchesPassword(pword);
                UserPreference.setBoolPreference(appContext, Constants.LOGGED_IN, loggedIn);
            }

            // if the user is successfully logged in (online or offline), open the main menu
            if(UserPreference.getBoolPreference(appContext, Constants.LOGGED_IN)){
                UserPreference.setStringPreference(appContext, Constants.USER_ID, user.getUserId());
                UserPreference.setStringPreference(appContext, Constants.USER_NAME, user.getUserName());
                UserPreference.setStringPreference(appContext, Constants.PASSWORD, pword);
                UserPreference.setStringPreference(appContext, Constants.SERVER, user.getServerUrl());

                Intent intent = new Intent(self, MainMenuActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                ((LoginTask) o).getProgressDialog().dismiss();
                finish();
            } else {
                ((LoginTask) o).getProgressDialog().dismiss();
                Toast.makeText(self, getString(R.string.invalid_username_password), Toast.LENGTH_SHORT).show();
            }
            }
        });
        task.setErrorMessage(getString(R.string.error_login));
        task.execute();
    }

}