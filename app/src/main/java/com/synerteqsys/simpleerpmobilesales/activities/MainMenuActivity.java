package com.synerteqsys.simpleerpmobilesales.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.synerteqsys.simpleerpmobilesales.R;
import com.synerteqsys.simpleerpmobilesales.data.UserPreference;
import com.synerteqsys.simpleerpmobilesales.services.UploadDownloadService;
import com.synerteqsys.simpleerpmobilesales.utils.Constants;
import com.synerteqsys.simpleerpmobilesales.utils.NetworkUtils;
import com.synerteqsys.simpleerpmobilesales.utils.database.DirectSaleHelper;
import com.synerteqsys.simpleerpmobilesales.utils.database.UserHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by dev06 on 02/01/2018.
 */

public class MainMenuActivity extends AppCompatActivity{
    private static String mode;
    private String warehouse;

    private Button directSaleBtn;
    private Button saleOrderBtn;
    private Button purchaseBtn;
    private Button priceSearchBtn;
    private Button deliveryBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        directSaleBtn = findViewById(R.id.directSaleBtn);
        saleOrderBtn = findViewById(R.id.saleOrderBtn);
        purchaseBtn = findViewById(R.id.purchaseBtn);
        priceSearchBtn = findViewById(R.id.priceSearchBtn);
        deliveryBtn = findViewById(R.id.deliveryBtn);

        String functions = UserHelper.getUserFunctions(getApplicationContext());
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = null;
        try {
            map = mapper.readValue(functions, new TypeReference<Map<String,Object>>(){});
            ArrayList<String> vals = (ArrayList)map.get("functions");
            for(String i : vals){
                switch(i){
                    case "price_search": priceSearchBtn.setVisibility(View.VISIBLE); break;
                    case "new_po": purchaseBtn.setVisibility(View.VISIBLE); break;
                    case "new_so": saleOrderBtn.setVisibility(View.VISIBLE); break;
                    case "new_ds": directSaleBtn.setVisibility(View.VISIBLE); break;
                    case "delivery_verify": deliveryBtn.setVisibility(View.VISIBLE); break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        TextView welcomeText = findViewById(R.id.welcomeMsg);
        welcomeText.setText(getString(R.string.user_greet, UserPreference.getStringPreference(getApplicationContext(), Constants.USER_NAME)));

        warehouse = UserPreference.getWarehouse(getApplicationContext());
        mode = UserPreference.getStringPreference(getApplicationContext(), Constants.MODE);
        if(!(warehouse == null || warehouse.isEmpty()) && mode.equals(Constants.ONLINE)){
            // we start downloading data
            NetworkUtils.downloadData(this);
        }

        //start the uploading service once the user is successfully logged in
        Intent uploadS = new Intent(getApplicationContext(), UploadDownloadService.class);
        startService(uploadS);
    }

    @Override
    protected void onStart() {
        super.onStart();

        warehouse = UserPreference.getWarehouse(getApplicationContext());
        // first check if the warehouse of the machine has been set
        if(warehouse == null || warehouse.isEmpty()){
            directSaleBtn.setEnabled(false);
            saleOrderBtn.setEnabled(false);
            purchaseBtn.setEnabled(false);
            priceSearchBtn.setEnabled(false);
            deliveryBtn.setEnabled(false);
            Toast toast = Toast.makeText(getApplicationContext(), getText(R.string.no_warehouse), Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        } else {
            directSaleBtn.setEnabled(true);
            saleOrderBtn.setEnabled(true);
            purchaseBtn.setEnabled(true);
            priceSearchBtn.setEnabled(true);
            deliveryBtn.setEnabled(true);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        TextView offlineText = findViewById(R.id.offlineMsg);
        mode = UserPreference.getStringPreference(getApplicationContext(), Constants.MODE);
        if(mode.equals(Constants.ONLINE))
            offlineText.setVisibility(View.INVISIBLE);
        else
            offlineText.setVisibility(View.VISIBLE);
    }

    public void modifySettings(View v){
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    public void logout(View v){
        final Activity self = this;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.logout_operation);
        builder.setMessage(R.string.logout_msg);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            if (dialog != null) {
                dialog.dismiss();
            }
            // stop the upload-download service
            Intent serviceIntent = new Intent(getApplicationContext(), UploadDownloadService.class);
            stopService(serviceIntent);
            // clear the login info
            UserPreference.clearPreferences(getApplicationContext());
            // go back to the login screen
            Intent intent = new Intent(self, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();

            }
        });
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.show();

    }

    public void startDirectSale(View v){
        String saleSession = DirectSaleHelper.createSaleSession(getApplicationContext());
        UserPreference.setStringPreference(getApplicationContext(), Constants.DS_SESSION, saleSession);

        Intent intent = new Intent(this, ItemScanActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("type", Constants.DIRECT_SALE_ORDER);
        startActivity(intent);

    }

    public void createPurchaseOrder(View v){
        Intent intent = new Intent(this, ItemScanActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("type", Constants.PURCHASE_ORDER);
        startActivity(intent);
    }

    public void createSaleOrder(View v){
        Intent intent = new Intent(this, CustomerActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void selectDelivery(View v){
        Intent intent = new Intent(this, DeliverySelectActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void selectPricelist(View v){
        Intent intent = new Intent(this, ItemScanActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("type", Constants.PRICE_SEARCH);
        startActivity(intent);
    }

}
