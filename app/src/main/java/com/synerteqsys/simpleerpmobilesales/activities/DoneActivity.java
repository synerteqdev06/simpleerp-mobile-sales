package com.synerteqsys.simpleerpmobilesales.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.synerteqsys.simpleerpmobilesales.R;
import com.synerteqsys.simpleerpmobilesales.data.models.DirectSale;
import com.synerteqsys.simpleerpmobilesales.utils.database.DirectSaleHelper;
import com.synerteqsys.simpleerpmobilesales.utils.ui.ItemAdapter;
import com.synerteqsys.simpleerpmobilesales.data.ScannedItem;
import com.synerteqsys.simpleerpmobilesales.data.UserPreference;
import com.synerteqsys.simpleerpmobilesales.utils.Constants;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class DoneActivity extends AppCompatActivity {
    private ArrayList<ScannedItem> items;
    private float mTotalDue = 0;
    public static DecimalFormat moneyFormat=new DecimalFormat("₱#,##0.00");
    EditText mEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_done);

        mEdit = findViewById(R.id.givenInput);
        Bundle intentBundle = getIntent().getExtras();
        mTotalDue = intentBundle.getFloat("total");
        mEdit.setText(Float.toString(mTotalDue));
        TextView totalAmtL = findViewById(R.id.totalAmtLabel);
        totalAmtL.setText(ItemAdapter.moneyFormat.format(mTotalDue));

        items = (ArrayList<ScannedItem>) intentBundle.getSerializable("list");

        initializeList();
        initializeBtn(this);

    }

    private void initializeList() {
        ListView mListView = findViewById(R.id.searchList);
        ItemAdapter adapter = new ItemAdapter(items, R.layout.row_item, getApplicationContext());
        mListView.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, ItemScanActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("type", Constants.DIRECT_SALE_ORDER);
        intent.putExtra("list", items);
        startActivity(intent);
        finish();
    }

    private void initializeBtn(final Activity self){

        Button saleDoneB = findViewById(R.id.saleDoneBtn);
        saleDoneB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog alertDialog = new AlertDialog.Builder(self).create();
                alertDialog.setTitle(getString(R.string.done));
                float change;

                try {
                    float amt = getAmtPaid();
                    change = amt - mTotalDue;
                    if(change < 0){
                        Toast.makeText(getApplicationContext(), getString(R.string.not_enough), Toast.LENGTH_SHORT).show();
                    } else {
                        alertDialog.setMessage(getString(R.string.total_change) + " " + moneyFormat.format(change));

                        alertDialog.setButton(getString(R.string.finish), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                insertSaleOrders();

                                Intent intent = new Intent(getApplication(), ItemScanActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.putExtra("type", Constants.DIRECT_SALE_ORDER);
                                startActivity(intent);
                                finish();
                            }
                        });

                        alertDialog.show();

                    }
                } catch(NumberFormatException e){
                    Toast.makeText(getApplicationContext(), getString(R.string.invalid_amount), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void insertSaleOrders(){
        String sessionId = UserPreference.getStringPreference(getApplicationContext(), Constants.DS_SESSION);
        float amtPaid = getAmtPaid();
        DirectSale ds = new DirectSale(mTotalDue, amtPaid);
        ds.setDirectSaleSessionId(sessionId);
        String orderId = DirectSaleHelper.createDirectSaleOrder(getApplicationContext(), ds);
        Log.d(Constants.TAG, "Session " + sessionId + ": " + orderId);

        for(ScannedItem item : items){
            DirectSaleHelper.addDirectSaleOrderItem(getApplicationContext(), orderId, item.getId(), item.getQty(), item.getPrice(), item.getPackagingId());
        }
    }

    private float getAmtPaid() throws NumberFormatException{
        float amt = Float.parseFloat(mEdit.getText().toString());

        return amt;
    }


}
