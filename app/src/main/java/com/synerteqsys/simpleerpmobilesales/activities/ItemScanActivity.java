package com.synerteqsys.simpleerpmobilesales.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.vision.MultiDetector;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.android.gms.vision.text.TextRecognizer;
import com.synerteqsys.simpleerpmobilesales.R;
import com.synerteqsys.simpleerpmobilesales.data.ScannedItem;
import com.synerteqsys.simpleerpmobilesales.data.UserPreference;
import com.synerteqsys.simpleerpmobilesales.data.models.DirectSaleSession;
import com.synerteqsys.simpleerpmobilesales.data.models.PriceListOption;
import com.synerteqsys.simpleerpmobilesales.data.models.Product;
import com.synerteqsys.simpleerpmobilesales.utils.Constants;
import com.synerteqsys.simpleerpmobilesales.utils.camera.CameraSource;
import com.synerteqsys.simpleerpmobilesales.utils.camera.CameraSourcePreview;
import com.synerteqsys.simpleerpmobilesales.utils.camera.GraphicOverlay;
import com.synerteqsys.simpleerpmobilesales.utils.database.DirectSaleHelper;
import com.synerteqsys.simpleerpmobilesales.utils.database.PriceListHelper;
import com.synerteqsys.simpleerpmobilesales.utils.database.ProductHelper;
import com.synerteqsys.simpleerpmobilesales.utils.database.PurchaseOrderHelper;
import com.synerteqsys.simpleerpmobilesales.utils.database.SaleOrderHelper;
import com.synerteqsys.simpleerpmobilesales.utils.database.SerialHelper;
import com.synerteqsys.simpleerpmobilesales.utils.ui.BoxDetector;
import com.synerteqsys.simpleerpmobilesales.utils.ui.DetectProcessor;
import com.synerteqsys.simpleerpmobilesales.utils.ui.ItemAdapter;
import com.synerteqsys.simpleerpmobilesales.utils.ui.ProductListAdapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class ItemScanActivity extends AppCompatActivity implements DetectProcessor {
    // Intent request code to handle updating play services if needed.
    private static final int RC_HANDLE_GMS = 9001;

    // Permission request codes need to be < 256
    private static final int RC_HANDLE_CAMERA_PERM = 2;

    // Constants used to pass extra data in the intent
    public static final String AutoFocus = "AutoFocus";
    public static final String UseFlash = "UseFlash";
    public static final String TextBlockObject = "String";
    private static final int MIN_CODE_LENGTH = 3;
    private static final long REPEAT_SCAN_THRESHOLD = 1000;

    private CameraSource mCameraSource;
    private CameraSourcePreview mPreview;
    private GraphicOverlay mGraphicOverlay;
    private SwipeMenuListView mListView;
    private ArrayList<com.synerteqsys.simpleerpmobilesales.data.ScannedItem> items;
    private Map<String, ScannedItem> lookup;
    private ItemAdapter adapter;
    private TextView mTotal;
    private Button mDoneButton;
    private Button mCancelButton;
    private Button mSearchButton;
    private TextView itemScanLabel;
    private ToneGenerator toneGen = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);

    // Helper objects for detecting pinch
    private ScaleGestureDetector scaleGestureDetector;

    // A TextToSpeech engine for speaking a String value.
    private TextToSpeech tts;
    private boolean speechEnabled;

    ArrayAdapter<String> itemsAdapter;
    private int boxWidth;
    private int boxHeight;
    private long lastScanTime = 0;
    private String lastValue = "";
    private boolean mDialogShown;
    private float mTotalDue = 0;
    private boolean searchSerial = false;

    private String orderType;

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

    /**
     * Initializes the UI and creates the detector pipeline.
     */
    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        orderType = getIntent().getExtras().getString("type");
        Log.d(Constants.TAG, "Creating " + orderType);

        setContentView(R.layout.activity_item_scan);
        final Activity self = this;
        initializeCamera();
        initializeList(self);
        initializeSpeech();
        initializeButtons(self);
        initializeSearch();
        initializeLabel();
        scaleGestureDetector = new ScaleGestureDetector(this, new ItemScanActivity.ScaleListener());
        if(orderType.equals(Constants.PRICE_SEARCH))
            initializePricelist();
    }

    /**
     * Initialize the keyboard search if it has been enabled
     */
    private void initializeSearch() {
        mSearchButton = findViewById(R.id.searchBtn);
        // check if the keyboard input is enabled
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        final boolean keyboardEnabled = sharedPref.getBoolean("keyboard_pref", false);
        final LinearLayout keyboard = findViewById(R.id.keyboardSearch);
        if(keyboardEnabled) {
            mSearchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(keyboard.getVisibility() == View.GONE){
                        keyboard.setVisibility(View.VISIBLE);
                        mSearchButton.setText(R.string.close_search);
                        mPreview.setVisibility(View.GONE);
                    } else {
                        keyboard.setVisibility(View.GONE);
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(mSearchButton.getWindowToken(), 0);
                        mSearchButton.setText(R.string.search_product);
                        mPreview.setVisibility(View.VISIBLE);
                    }
                }
            });
            ListView prodListView = findViewById(R.id.prodList);
            EditText prodSearch = findViewById(R.id.searchProduct);
            List<Product> prodDbList;
            if(orderType.equals(Constants.DIRECT_SALE_ORDER)) {
                prodDbList = Arrays.asList(ProductHelper.getProductsWithPackaging(getApplicationContext()));
            } else if(orderType.equals(Constants.DELIVERY)) {
                prodDbList = Arrays.asList(ProductHelper.getProductsWithSerial(getApplicationContext()));
            } else {
                // get the products without the packaging
                prodDbList = Arrays.asList(ProductHelper.getProductsNoPackaging(getApplicationContext()));
            }
            ProductListAdapter prodAdapter = new ProductListAdapter(this,prodDbList);
            prodListView.setAdapter(prodAdapter);
            final ListView finalProdListView = prodListView;
            prodListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long id) {
                    Product prod = (Product) finalProdListView.getItemAtPosition(pos);
                    Log.d(Constants.TAG, prod.getProdName() + " selected");
                    addProductToList(prod.getBarcode());
                }
            });

            final ProductListAdapter finalAdapter = prodAdapter;
            prodSearch.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int start, int before, int count) { }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    finalAdapter.filter(charSequence);
                    finalAdapter.notifyDataSetChanged();
                }

                @Override
                public void afterTextChanged(Editable editable) { }
            });
        } else {
            //remove the search button
            mSearchButton.setVisibility(View.GONE);
        }
    }

    private void initializeCamera() {
        mPreview = findViewById(R.id.preview);
        mGraphicOverlay = findViewById(R.id.graphicOverlay);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;
        boxWidth = width / 2;
        boxHeight = height / 5;
        mGraphicOverlay.setBoxHeight(boxHeight);
        mGraphicOverlay.setBoxWidth(boxWidth);
        // Set good defaults for capturing text.
        boolean autoFocus = true;
        boolean useFlash = false;

        // Check for the camera permission before accessing the camera.  If the
        // permission is not granted yet, request permission.
        int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (rc == PackageManager.PERMISSION_GRANTED) {
            createCameraSource(autoFocus, useFlash);
        } else {
            requestCameraPermission();
        }

    }

    private void initializeButtons(final Activity self) {
        mTotal = findViewById(R.id.lblTotal);
        updateTotal();
        if(orderType.equals(Constants.DELIVERY))
            mTotal.setVisibility(View.GONE);

        mDoneButton = findViewById(R.id.btnDone);
        mDoneButton.setEnabled(!items.isEmpty());
        adapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
               mDoneButton.setEnabled(adapter.getCount()>0);
            }
        });
        mDoneButton.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View view) {
                                              if (orderType.equals(Constants.PURCHASE_ORDER) ||
                                                  orderType.equals(Constants.SALE_ORDER)){
                                                  getRemarks();
                                              } else {
                                                  Intent data = new Intent(getApplication(), DoneActivity.class);
                                                  if(orderType.equals(Constants.DELIVERY)) {
                                                      data = new Intent(getApplication(), DeliveryVerifyActivity.class);
                                                      data.putExtra("delivery", getIntent().getExtras().getString("delivery"));
                                                  }
                                                  data.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                  data.putExtra("type", orderType);
                                                  data.putExtra("list", items);
                                                  data.putExtra("total", mTotalDue);
                                                  startActivity(data);
                                                  finish();
                                              }
                                          }
                                      }
        );
        mCancelButton = findViewById(R.id.btnCancel);
        mCancelButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                cancel();
            }
        });
    }

    private void cancel(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.cancel_operation);
        builder.setMessage(R.string.cancel_message);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog != null) {
                    dialog.dismiss();
                }
                if(orderType.equals(Constants.DIRECT_SALE_ORDER)) {
                    String sessionId = UserPreference.getStringPreference(getApplicationContext(), Constants.DS_SESSION);
                    Log.d(Constants.TAG, "Closing session " + sessionId);
                    DirectSaleHelper.closeSaleSession(getApplicationContext(), sessionId);

                    DirectSaleSession[] sessions = DirectSaleHelper.getDirectSaleSessions(getApplicationContext());
                    System.out.println(sessions);
                }

                if(orderType.equals(Constants.SALE_ORDER)) {
                    Intent custIntent = new Intent(getApplication(), CustomerActivity.class);
                    custIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(custIntent);
                } else if(orderType.equals(Constants.DELIVERY)) {
                    Intent custIntent = new Intent(getApplication(), DeliverySelectActivity.class);
                    custIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(custIntent);
                } else {
                    Intent menuIntent = new Intent(getApplication(), MainMenuActivity.class);
                    setResult(RESULT_CANCELED, menuIntent);
                    startActivity(menuIntent);
                }
                finish();
            }
        });

        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.show();
    }

    private void initializeSpeech() {
        TextToSpeech.OnInitListener listener =
                new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(final int status) {
                        if (status == TextToSpeech.SUCCESS) {
                            Log.d("TTS", "Text to speech engine started successfully.");
                            tts.setLanguage(Locale.US);
                        } else {
                            Log.d("TTS", "Error starting the text to speech engine.");
                            Snackbar.make(mGraphicOverlay, R.string.unable_to_init_speech_engine,
                                    Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    }
                };
        tts = new TextToSpeech(this.getApplicationContext(), listener);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        speechEnabled = sharedPref.getBoolean("speech_pref", false);
    }

    private void initializeLabel(){
        itemScanLabel = findViewById(R.id.itemScanLabel);
        CharSequence label = "";
        switch(orderType){
            case Constants.PRICE_SEARCH: label = getText(R.string.price_search_btn); break;
            case Constants.DELIVERY:
                label = getText(R.string.delivery_ver_btn);
                Switch serialSwitch = findViewById(R.id.serialSwitch);
                serialSwitch.setVisibility(View.VISIBLE);
                serialSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        searchSerial = isChecked;
                        ListView prodListView = findViewById(R.id.prodList);
                        ProductListAdapter proAdapter = (ProductListAdapter)prodListView.getAdapter();
                        proAdapter.notifyDataSetChanged();
                    }
                });
                break;
            case Constants.SALE_ORDER: label = getText(R.string.create_sale_btn); break;
            case Constants.PURCHASE_ORDER: label = getText(R.string.purchase_btn); break;
            case Constants.DIRECT_SALE_ORDER: label = getText(R.string.direct_sales_btn); break;

        }
        itemScanLabel.setText(label);
    }

    private void initializeList(final Activity self) {
        mListView = findViewById(R.id.listview);
        items = new ArrayList<>();
        lookup = new HashMap<>();
        if(orderType.equals(Constants.PRICE_SEARCH)) {
            adapter = new ItemAdapter(items, R.layout.product_price, getApplicationContext());
        } else if(orderType.equals(Constants.DELIVERY)) {
            adapter = new ItemAdapter(items, R.layout.delivery_item, getApplicationContext());
        } else {
            adapter = new ItemAdapter(items, R.layout.row_item, getApplicationContext());
        }
        mListView.setAdapter(adapter);
        mTotalDue = 0;

        Bundle intentBundle = getIntent().getExtras();
        ArrayList<ScannedItem> extraItems = (ArrayList<ScannedItem>) intentBundle.getSerializable("list");
        if(extraItems != null){
            items.addAll(extraItems);
            for(ScannedItem item : extraItems){
                lookup.put(item.getSearchId(), item);
                mTotalDue += item.getSubtotal();
            }
        }

        if(!orderType.equals(Constants.PRICE_SEARCH)) {
            SwipeMenuCreator creator = new SwipeMenuCreator() {

                @Override
                public void create(SwipeMenu menu) {
                    // create "open" item
                    SwipeMenuItem openItem = new SwipeMenuItem(
                            getApplicationContext());
                    // create "delete" item
                    SwipeMenuItem deleteItem = new SwipeMenuItem(
                            getApplicationContext());
                    // set item background
                    deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                            0x3F, 0x25)));
                    // set item width
                    deleteItem.setWidth(dp2px(90));
                    // set a icon
                    deleteItem.setIcon(android.R.drawable.ic_delete);
                    // add to menu
                    menu.addMenuItem(deleteItem);
                }
            };

            // set creator
            mListView.setMenuCreator(creator);
            mListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                    switch (index) {
                        case 0:
                            com.synerteqsys.simpleerpmobilesales.data.ScannedItem item = adapter.getItem(position);
                            mTotalDue = mTotalDue - item.getSubtotal();
                            updateTotal();
                            adapter.remove(item);
                            lookup.remove(item.getSearchId());
                            mListView.setAdapter(adapter);
                            break;
                    }
                    // false : close the menu; true : not close the menu
                    return false;
                }
            });

            mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    final com.synerteqsys.simpleerpmobilesales.data.ScannedItem item = adapter.getItem(position);
                    AlertDialog.Builder editDialogBuilder = new AlertDialog.Builder(self);
                    editDialogBuilder.setTitle(item.getDesc());
                    final EditText qtyInput = new EditText(self);
                    qtyInput.setText(String.valueOf(item.getQty()));
                    qtyInput.setSelectAllOnFocus(true);
                    qtyInput.setInputType(InputType.TYPE_CLASS_NUMBER);
                    qtyInput.setRawInputType(Configuration.KEYBOARD_12KEY);
                    editDialogBuilder.setView(qtyInput);


                    editDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                        }
                    });
                    editDialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            //Put actions for CANCEL button here, or leave in blank
                        }
                    });
                    final AlertDialog dialog = editDialogBuilder.create();
                    qtyInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                        @Override
                        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                            if (actionId == EditorInfo.IME_ACTION_DONE) {
                                handleDialogClose(qtyInput, item, dialog);
                            }
                            return false;
                        }
                    });
                    dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                    mDialogShown = true;
                    dialog.show();
                    dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            mDialogShown = false;
                        }
                    });
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            handleDialogClose(qtyInput, item, dialog);
                        }
                    });
                }

                private void handleDialogClose(EditText input, com.synerteqsys.simpleerpmobilesales.data.ScannedItem item, AlertDialog dialog) {
                    String val = input.getText().toString();
                    if (val.length() > 0 && Float.parseFloat(val) > 0) {
                        updateItem(item, input.getText().toString());
                        dialog.dismiss();
                    } else {
                        Toast.makeText(self, R.string.must_provide_quantity, Toast.LENGTH_LONG).show();
                    }
                }
            });
        }

    }

    private void initializePricelist(){
        mDoneButton.setVisibility(View.GONE);

        RadioGroup pricelistRadio = findViewById(R.id.pricelistRadio);
        pricelistRadio.setOrientation(RadioGroup.HORIZONTAL);
        PriceListOption[] pListOpt = PriceListHelper.getPricelistOptions(getApplicationContext());
        for(int i = 0; i < pListOpt.length; i++){
            RadioButton option = createPlistButton(pListOpt[i]);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            layoutParams.setMargins(10, 10, 10, 0);
            layoutParams.weight = 1f;

            pricelistRadio.addView(option, layoutParams);
        }

        pricelistRadio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int selected) {
                for(int button = 0; button < radioGroup.getChildCount(); button++){
                    View b = radioGroup.getChildAt(button);
                    if(b instanceof RadioButton){
                        RadioButton radio = (RadioButton) b;
                        if(radio.getId() == selected){
                            radio.setTextColor(getColor(R.color.buttonFont));
                        } else {
                            radio.setTextColor(getColor(R.color.colorPrimaryDark));
                        }
                    }
                }
                if(adapter.getCount() > 0) {
                    ScannedItem item = adapter.getItem(0);
                    selectProduct(item);
                }
            }
        });

        // set the first child as checked automatically
        if(pricelistRadio.getChildCount() > 0)
            pricelistRadio.check(pricelistRadio.getChildAt(0).getId());
    }

    private RadioButton createPlistButton(PriceListOption plist){
        RadioButton option = new RadioButton(this);
        option.setText(plist.getPriceListName());
        option.setTextSize(16f);
        option.setTextColor(getColor(R.color.colorPrimaryDark));
        option.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        option.setButtonDrawable(new StateListDrawable());
        option.setBackground(getDrawable(R.drawable.radiobtn));

        option.setPadding(5,5,5,5);

        return option;
    }

    //update the quantity of an existing scannedItem
    private void updateItem(com.synerteqsys.simpleerpmobilesales.data.ScannedItem item, String value) {
        float oldTotal = item.getSubtotal();
        item.setQty(Integer.parseInt(value, 10));
        item.setSubtotal(item.getQty() * item.getPrice());
        mTotalDue = mTotalDue - oldTotal + item.getSubtotal();
        updateTotal();
        adapter.notifyDataSetChanged();
    }


    /**
     * Handles the requesting of the camera permission.  This includes
     * showing a "Snackbar" message of why the permission is needed then
     * sending the request.
     */
    private void requestCameraPermission() {
        Log.w(Constants.TAG, "Camera permission is not granted. Requesting permission");

        final String[] permissions = new String[]{Manifest.permission.CAMERA};

        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, permissions, RC_HANDLE_CAMERA_PERM);
            return;
        }

        final Activity thisActivity = this;

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompat.requestPermissions(thisActivity, permissions,
                        RC_HANDLE_CAMERA_PERM);
            }
        };

        Snackbar.make(mGraphicOverlay, R.string.permission_camera_rationale,
                Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.ok, listener)
                .show();
    }


    //  @Override
    public boolean onTouchEvent(MotionEvent e) {
        boolean b = scaleGestureDetector.onTouchEvent(e);
        return b || super.onTouchEvent(e);
    }

    /**
     * Creates and starts the camera.  Note that this uses a higher resolution in comparison
     * to other detection examples to enable the ocr detector to detect small text samples
     * at long distances.
     * <p>
     * Suppressing InlinedApi since there is a check that the minimum version is met before using
     * the constant.
     */
    @SuppressLint("InlinedApi")
    private void createCameraSource(boolean autoFocus, boolean useFlash) {
        Context context = getApplicationContext();

        // Create the multiDetector
        TextRecognizer textRecognizer = new TextRecognizer.Builder(context).build();
        com.synerteqsys.simpleerpmobilesales.data.TextTrackerFactory textFactory = new com.synerteqsys.simpleerpmobilesales.data.TextTrackerFactory(this);
        textRecognizer.setProcessor(new MultiProcessor.Builder<>(textFactory).build());

        BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(context).build();
        com.synerteqsys.simpleerpmobilesales.data.BarcodeTrackerFactory barcodeFactory = new com.synerteqsys.simpleerpmobilesales.data.BarcodeTrackerFactory(this);
        barcodeDetector.setProcessor(
                new MultiProcessor.Builder<>(barcodeFactory).build());
        MultiDetector multiDetector = new MultiDetector.Builder().add(textRecognizer).add(barcodeDetector).build();

        // Check if the TextRecognizer is operational.
        if (!multiDetector.isOperational()) {
            Log.w(Constants.TAG, "Detector dependencies are not yet available.");

            // Check for low storage.  If there is low storage, the native library will not be
            // downloaded, so detection will not become operational.
            IntentFilter lowstorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
            boolean hasLowStorage = registerReceiver(null, lowstorageFilter) != null;

            if (hasLowStorage) {
                Toast.makeText(this, R.string.low_storage_error, Toast.LENGTH_LONG).show();
                Log.w(Constants.TAG, getString(R.string.low_storage_error));
            }
        }
        BoxDetector detector = new BoxDetector(multiDetector, boxWidth, boxHeight);
        // Create the mCameraSource using the TextRecognizer.
        mCameraSource =
                new CameraSource.Builder(getApplicationContext(), detector)
                        .setFacing(CameraSource.CAMERA_FACING_BACK)
                        .setRequestedPreviewSize(1280, 1024)
                        .setRequestedFps(15.0f)
                        .setFlashMode(useFlash ? Camera.Parameters.FLASH_MODE_TORCH : null)
                        .setFocusMode(autoFocus ? Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE : null)
                        .build();
    }

    /**
     * Restarts the camera.
     */
    @Override
    protected void onResume() {
        super.onResume();
        startCameraSource();
    }

    /**
     * Stops the camera.
     */
    @Override
    protected void onPause() {
        super.onPause();
        if (mPreview != null) {
            mPreview.stop();
        }
    }

    /**
     * Releases the resources associated with the camera source, the associated detectors, and the
     * rest of the processing pipeline.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPreview != null) {
            mPreview.release();
        }
    }

    /**
     * Cancels the whole operation
     */
    @Override
    public void onBackPressed() {
        cancel();
//        super.onBackPressed();
    }

    /**
     * Callback for the result from requesting permissions. This method
     * is invoked for every call on {@link #requestPermissions(String[], int)}.
     * <p>
     * <strong>Note:</strong> It is possible that the permissions request interaction
     * with the user is interrupted. In this case you will receive empty permissions
     * and results arrays which should be treated as a cancellation.
     * </p>
     *
     * @param requestCode  The request code passed in {@link #requestPermissions(String[], int)}.
     * @param permissions  The requested permissions. Never null.
     * @param grantResults The grant results for the corresponding permissions
     *                     which is either {@link PackageManager#PERMISSION_GRANTED}
     *                     or {@link PackageManager#PERMISSION_DENIED}. Never null.
     * @see #requestPermissions(String[], int)
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != RC_HANDLE_CAMERA_PERM) {
            Log.d(Constants.TAG, "Got unexpected permission result: " + requestCode);
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.d(Constants.TAG, "Camera permission granted - initialize the camera source");
            // We have permission, so create the camerasource
            boolean autoFocus = getIntent().getBooleanExtra(AutoFocus, false);
            boolean useFlash = getIntent().getBooleanExtra(UseFlash, false);
            createCameraSource(autoFocus, useFlash);
            return;
        }

        Log.e(Constants.TAG, "Permission not granted: results len = " + grantResults.length +
                " Result code = " + (grantResults.length > 0 ? grantResults[0] : "(empty)"));

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(Constants.TAG)
                .setMessage(R.string.no_camera_permission)
                .setPositiveButton(R.string.ok, listener)
                .show();
    }

    /**
     * Starts or restarts the camera source, if it exists.  If the camera source doesn't exist yet
     * (e.g., because onResume was called before the camera source was created), this will be called
     * again when the camera source is created.
     */
    private void startCameraSource() throws SecurityException {
        // check that the device has play services available.
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                getApplicationContext());
        if (code != ConnectionResult.SUCCESS) {
            Dialog dlg =
                    GoogleApiAvailability.getInstance().getErrorDialog(this, code, RC_HANDLE_GMS);
            dlg.show();
        }

        if (mCameraSource != null) {
            try {
                mPreview.start(mCameraSource, mGraphicOverlay);
            } catch (IOException e) {
                Log.e(Constants.TAG, "Unable to start camera source.", e);
                mCameraSource.release();
                mCameraSource = null;
            }
        }
    }

    //handles the event when a text/barcode is detected
    @Override
    public void invoke(String value, String source) {
        //when dialog is shown, stop processing scans
        if (mDialogShown) {
            return;
        }
        //when the detected code is shorter than the minimum, discard.
        if (value == null || value.length() < MIN_CODE_LENGTH) {
            return;
        }
        //if the same value is being scanned, make sure that it is at least 1 second apart.
        if (value.equals(lastValue) && (System.currentTimeMillis() - lastScanTime) < REPEAT_SCAN_THRESHOLD) {
            return;
        }
        addProductToList(value);

    }

    /**
     * Used to add products to the list but not using the packaging id
     * @param value the key of the product being searched
     */
    public void addProductToList(String value){
        addProductToList(value, false);
    }

    /**
     * Searches for the product given the value and adds it to the list or shows the price to the user
     * @param value the value that will be used to search the product (it can be the sku, barcode, or id)
     * @param isId indicates if the value is the packaging ID
     */
    public void addProductToList(String value, boolean isId){
        Context appContext = getApplicationContext();
        Log.d(Constants.TAG, "Scanned value: " + value);

        // if the value is null, don't search
        if(value == null) return;

        //if the code matches one of the products, then we add that product to the list.
        Product p = null;

        // if the order being made is a sale order, get the prices from the price list
        if(orderType.equals(Constants.SALE_ORDER)){
            String custId = getIntent().getExtras().getString("customer");
            p = ProductHelper.findProduct(appContext, value, custId, isId);
        } else {
            if(searchSerial)
                p = SerialHelper.findSerial(appContext, value);
            else
                p = ProductHelper.findProduct(appContext, value, isId);
        }

        if (p != null) {
            lastValue = value;
            lastScanTime = System.currentTimeMillis();
            ScannedItem item = new ScannedItem();
            item.setDesc(p.getProdName());
            if(orderType.equals(Constants.PRICE_SEARCH)) {
                item = scannedItemFromProd(p, value);
                item.setQty(1);
                selectProduct(item);
            } else {
                item = lookup.get(p.getPackagingId()); //packaging id is the search id for all cases
                if (item == null) {
                    item = scannedItemFromProd(p, value);
                    mTotalDue = mTotalDue + item.getSubtotal();
                    final ScannedItem temp = item;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.add(temp);
                            lookup.put(temp.getSearchId(), temp);
                            updateTotal();
                        }
                    });
                } else {
                    item.setQty(item.getQty() + 1);
                    item.setSubtotal(item.getQty() * item.getPrice());
                    mTotalDue = mTotalDue + item.getPrice();
                    runOnUiThread(
                            new Runnable() {
                                @Override
                                public void run() {
                                    adapter.notifyDataSetChanged();
                                    updateTotal();
                                }
                            }
                    );
                }
            }
            if(speechEnabled) {
                tts.speak(item.getDesc(), TextToSpeech.QUEUE_FLUSH, null, "DEFAULT");
            } else {

                toneGen.startTone(ToneGenerator.TONE_CDMA_PIP,150);
            }
        }
    }

    private ScannedItem scannedItemFromProd(Product p, String value){
        ScannedItem item = new ScannedItem();
        item.setId(p.getProdId());
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Set<String> identifiers =  sharedPref.getStringSet("scanner_pref", null);
        item.setSearchId(p.getPackagingId());
        if(!(value.equals(p.getPackagingId()) || value.equals(p.getProdId())))
        // if the value searched isn't the id of the item, set it as the search_id (for display)
            item.setDisplayId(value);
        else if(identifiers.contains("barcode") && p.getBarcode() != null)
            item.setDisplayId(p.getBarcode());
        else
            item.setDisplayId(p.getSku());

        item.setDesc(p.getProdName());
        item.setPrice((float) (p.getSrp()));
        item.setQty(1);
        item.setUnit(p.getBaseUnit());
        if (orderType.equals(Constants.DIRECT_SALE_ORDER))
            item.setPackagingId(p.getPackagingId());
        item.setSubtotal(item.getQty() * item.getPrice());
        return item;
    }

    /**
     * For Price Search function. Selects the product and shows the price (given the selected price list)
     * @param item the selected item from the list
     */
    private void selectProduct(ScannedItem item){
        RadioGroup pricelistRadio = findViewById(R.id.pricelistRadio);
        RadioButton selected = findViewById(pricelistRadio.getCheckedRadioButtonId());
        String pricelist="";
        if(selected!=null)
            pricelist = PriceListHelper.findPriceListId(getApplicationContext(), selected.getText().toString());

        Double price = PriceListHelper.findProductPrice(getApplicationContext(), item.getId(), pricelist);
        if(price != null)
            mTotalDue = price.floatValue();
        else
            mTotalDue = item.getPrice();
        final ScannedItem fItem = item;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.clear();
                adapter.notifyDataSetChanged();
                adapter.add(fItem);
                updateTotal();
            }
        });
    }

    private void updateTotal() {
        mTotal.setText(ItemAdapter.moneyFormat.format(mTotalDue));
    }

    /**
     * Creates an alert dialog where the user can input any remarks regarding
     * the sale/purchase order
     */
    private void getRemarks(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.get_remarks));

        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                createOrderFromItems(input.getText().toString());
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    /**
     * Creates a sale/purchase order containing the items that were scanned
     *
     * @param remarks remarks that the user has inputted
     */
    private void createOrderFromItems(String remarks){
        Context context = getApplicationContext();
        if(orderType.equals(Constants.PURCHASE_ORDER)) {
            String purchaseId = PurchaseOrderHelper.createPurchaseOrder(context);
            PurchaseOrderHelper.addRemarksToPurchase(context, purchaseId, remarks);
            for (ScannedItem item : items)
                PurchaseOrderHelper.addPurchaseOrderItem(context, purchaseId, item.getId(), item.getQty());

            Intent intent = new Intent(getApplication(), MainMenuActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else if (orderType.equals(Constants.SALE_ORDER)){
            String custId = getIntent().getExtras().getString("customer");
            String custWhId = getIntent().getExtras().getString("custWh");
            String saleId = SaleOrderHelper.createSaleOrder(context, custId, custWhId);
            SaleOrderHelper.addRemarksToSale(context, saleId, remarks);
            for (ScannedItem item : items)
                SaleOrderHelper.addSaleOrderLine(context, saleId, item.getId(), item.getQty(), item.getPrice());

            Intent intent = new Intent(getApplication(), CustomerActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
    }

    private class ScaleListener implements ScaleGestureDetector.OnScaleGestureListener {

        /**
         * Responds to scaling events for a gesture in progress.
         * Reported by pointer motion.
         *
         * @param detector The detector reporting the event - use this to
         *                 retrieve extended info about event state.
         * @return Whether or not the detector should consider this event
         * as handled. If an event was not handled, the detector
         * will continue to accumulate movement until an event is
         * handled. This can be useful if an application, for example,
         * only wants to update scaling factors if the change is
         * greater than 0.01.
         */
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            if (mCameraSource != null) {
                mCameraSource.doZoom(detector.getScaleFactor());
            }
            return false;
        }

        /**
         * Responds to the beginning of a scaling gesture. Reported by
         * new pointers going down.
         *
         * @param detector The detector reporting the event - use this to
         *                 retrieve extended info about event state.
         * @return Whether or not the detector should continue recognizing
         * this gesture. For example, if a gesture is beginning
         * with a focal point outside of a region where it makes
         * sense, onScaleBegin() may return false to ignore the
         * rest of the gesture.
         */
        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            return true;
        }

        /**
         * Responds to the end of a scale gesture. Reported by existing
         * pointers going up.
         * <p/>
         * Once a scale has ended, {@link ScaleGestureDetector#getFocusX()}
         * and {@link ScaleGestureDetector#getFocusY()} will return focal point
         * of the pointers remaining on the screen.
         *
         * @param detector The detector reporting the event - use this to
         *                 retrieve extended info about event state.
         */
        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
            if (mCameraSource != null) {
                mCameraSource.doZoom(detector.getScaleFactor());
            }
        }

    }

    public boolean isSearchSerial() {
        return searchSerial;
    }
}
