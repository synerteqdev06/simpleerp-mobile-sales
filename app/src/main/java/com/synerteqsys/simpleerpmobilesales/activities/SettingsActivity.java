package com.synerteqsys.simpleerpmobilesales.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.view.MenuItem;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.synerteqsys.simpleerpmobilesales.R;
import com.synerteqsys.simpleerpmobilesales.data.UserData;
import com.synerteqsys.simpleerpmobilesales.data.UserPreference;
import com.synerteqsys.simpleerpmobilesales.data.models.Warehouse;
import com.synerteqsys.simpleerpmobilesales.utils.ActionDoneListener;
import com.synerteqsys.simpleerpmobilesales.utils.AsyncHttpGetTask;
import com.synerteqsys.simpleerpmobilesales.utils.Constants;
import com.synerteqsys.simpleerpmobilesales.utils.database.DatabaseHelper;
import com.synerteqsys.simpleerpmobilesales.utils.LocaleUtils;
import com.synerteqsys.simpleerpmobilesales.utils.NetworkUtils;
import com.synerteqsys.simpleerpmobilesales.utils.ResponseParser;
import com.synerteqsys.simpleerpmobilesales.utils.database.UserHelper;

import java.io.IOException;

public class SettingsActivity extends AppCompatPreferenceActivity {
    protected static String serverUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        serverUrl = UserPreference.getStringPreference(getApplicationContext(), Constants.SERVER);
        getFragmentManager().beginTransaction().replace(
                android.R.id.content,
                new MainPreferenceFragment()).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId() == android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    public static class MainPreferenceFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {
        ListPreference whListPref;
        @Override
        public void onCreate(final Bundle savedInstanceState){

            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.settings_main);

            whListPref = (ListPreference) findPreference("warehouse_pref");
            downloadWarehouses();
            whListPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                    alertDialog.setTitle(getString(R.string.warehouse_change));
                    alertDialog.setMessage(getString(R.string.warehouse_warn));
                    alertDialog.setButton(getText(R.string.warehouse_cont), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    });
                    alertDialog.show();

                    return true;
                }
            });
        }

        @Override
        public void onResume() {
            super.onResume();
            // Set up a listener whenever a key changes
            getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onPause() {
            super.onPause();
            // Set up a listener whenever a key changes
            getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if(key.equals("warehouse_pref")){
                DatabaseHelper.clearDatabase(this.getContext().getApplicationContext());
                UserHelper.updateLastDlTime(this.getContext().getApplicationContext(), "");
                NetworkUtils.downloadData(this.getActivity());
            } else if (key.equals("lang_pref")){
                LocaleUtils.resetLocale(this.getContext().getApplicationContext());
                this.getActivity().finish();
                startActivity(this.getActivity().getIntent());
            }
        }

        private void downloadWarehouses() {
            boolean isOnlineMode = UserPreference.getStringPreference(getContext(), Constants.MODE).equals(Constants.ONLINE);
            if (NetworkUtils.hasNetwork(getContext()) && isOnlineMode) {
                AsyncHttpGetTask getProd = new AsyncHttpGetTask(getActivity(), new ResponseParser() {
                    @Override
                    public void parse(Context context, String response) throws IOException {
                    ObjectMapper mapper = new ObjectMapper();
                    try {
                        //replace "data" key with "warehouse" so that the parsing goes correctly
                        if (response.contains("{\"data\":[")) {
                            response = response.replaceFirst("data", "warehouses");
                        }
                        UserData data = mapper.readValue(response, UserData.class);
                        Warehouse[] whList = data.getWarehouses();
                        String[] whName = new String[whList.length];
                        String[] whId = new String[whList.length];
                        for (int whCnt = 0; whCnt < whList.length; whCnt++) {
                            whName[whCnt] = whList[whCnt].getWarehouseName();
                            whId[whCnt] = whList[whCnt].getWarehouseId();
                        }
                        whListPref.setEntries(whName);
                        whListPref.setEntryValues(whId);
                        whListPref.setEnabled(true);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    }
                });
                getProd.setPostActionListener(new ActionDoneListener() {
                    @Override
                    public void handleDone(Object o) {
                        // the entries haven't been set properly
                        if (whListPref.getEntries() == null){
                            whListPref.setEnabled(false);
                        }
                    }
                });
                getProd.setProgressMessage(getText(R.string.settings_loading).toString());
                getProd.execute(serverUrl + Constants.WAREHOUSE_URL);
            } else {
                whListPref.setEnabled(false);
            }
        }
    }
}
