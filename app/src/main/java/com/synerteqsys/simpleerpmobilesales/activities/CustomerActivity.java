package com.synerteqsys.simpleerpmobilesales.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.synerteqsys.simpleerpmobilesales.R;
import com.synerteqsys.simpleerpmobilesales.data.models.CustWarehouse;
import com.synerteqsys.simpleerpmobilesales.data.models.Customer;
import com.synerteqsys.simpleerpmobilesales.utils.database.CustomerHelper;
import com.synerteqsys.simpleerpmobilesales.utils.Constants;

import java.util.Arrays;
import java.util.List;

public class CustomerActivity extends AppCompatActivity {

    private ListView custListView;
    private EditText listFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Activity self = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_search);

        custListView = findViewById(R.id.searchList);
        listFilter = findViewById(R.id.searchText);
        final List<Customer> custList = Arrays.asList(CustomerHelper.getCustomers(getApplicationContext()));

        ArrayAdapter<Customer> arrayAdapter = new ArrayAdapter<Customer>(
                this, android.R.layout.simple_list_item_1, custList
        );
        custListView.setAdapter(arrayAdapter);
        custListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long id) {
                Customer cust = (Customer) custListView.getItemAtPosition(pos);
                Log.d(Constants.TAG, cust.getPartnerId() + " selected");
                startOrder(cust);
            }
        });

        final ArrayAdapter<Customer> adapter = arrayAdapter;
        listFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int before, int count) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) { }
        });
    }

    private void startOrder(Customer c){
        CustWarehouse[] custWh = CustomerHelper.getCustWh(getApplicationContext(), c.getPartnerId());
        Intent intent;
        if(custWh.length > 1) {
            intent = new Intent(this, CustWarehouseActivity.class);
        } else{
            CustWarehouse wh = custWh[0];
            intent = new Intent(this, ItemScanActivity.class);
            intent.putExtra("type", Constants.SALE_ORDER);
            intent.putExtra("custWh", wh.getCustWarehouseId());
        }

        intent.putExtra("customer", c.getPartnerId());
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    public void cancel(View v){
        Intent menuIntent = new Intent(this, MainMenuActivity.class);
        setResult(RESULT_CANCELED, menuIntent);
        startActivity(menuIntent);
        finish();
    }

}
