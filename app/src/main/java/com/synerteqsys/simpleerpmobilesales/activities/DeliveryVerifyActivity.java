package com.synerteqsys.simpleerpmobilesales.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.synerteqsys.simpleerpmobilesales.R;
import com.synerteqsys.simpleerpmobilesales.data.ScannedDelItem;
import com.synerteqsys.simpleerpmobilesales.data.ScannedItem;
import com.synerteqsys.simpleerpmobilesales.data.models.Delivery;
import com.synerteqsys.simpleerpmobilesales.data.models.DeliveryDet;
import com.synerteqsys.simpleerpmobilesales.data.models.Product;
import com.synerteqsys.simpleerpmobilesales.utils.Constants;
import com.synerteqsys.simpleerpmobilesales.utils.database.DeliveryHelper;
import com.synerteqsys.simpleerpmobilesales.utils.database.ProductHelper;
import com.synerteqsys.simpleerpmobilesales.utils.ui.DeliveryItemAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DeliveryVerifyActivity extends AppCompatActivity {
    private ArrayList<ScannedItem> items;
    private ArrayList<ScannedDelItem> delItems;
    private TextView deliveryNo;
    private Delivery delivery;
    private Boolean isMatched;

    Button backBtn;
    Button delDoneBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_verify);
        deliveryNo = findViewById(R.id.deliveryNo);

        Bundle intentBundle = getIntent().getExtras();
        String delId = intentBundle.getString("delivery");
        this.delivery = DeliveryHelper.getDelivery(getApplicationContext(), delId);
        deliveryNo.setText(getString(R.string.delivery_no, delivery.getRecordNo()));

        initializeBtn(this);

        items = (ArrayList<ScannedItem>) intentBundle.getSerializable("list");
        isMatched = true;

        // first, check if the scanned products can be found in the delivery
        Map<String, ScannedDelItem> lookup = new HashMap<>();
        for(ScannedItem item : items){
            DeliveryDet det = delivery.findDelItem(item.getId());
            int qtyApproved = 0;
            int qtyDone = item.getQty();
            // scanned item is not found in the delivery list; Verification won't be confirmed
            if(det != null) {
                qtyApproved = (int) det.getQtyApprovedTotal();
            }else {
                delDoneBtn.setEnabled(false);
                delDoneBtn.setTextColor(ContextCompat.getColor(this, R.color.buttonFontDisabled));
            }
            ScannedDelItem scanDel = new ScannedDelItem(item, qtyApproved);
            scanDel.setQtyDone(qtyDone);
            lookup.put(item.getId(), scanDel);
            // a scanned item's qty doesn't match the qty requested in the delivery
            if(item.getQty() != qtyApproved)
                isMatched = false;
        }
        // now check if there have been any products in the delivery that haven't been scanned
        for(DeliveryDet det : delivery.getDets()){
            if(!lookup.containsKey(det.getProdId())){
                Product p = ProductHelper.findBaseProduct(getApplicationContext(), det.getProdId());
                if(p == null){
                    p = new Product();
                    p.setProdName("-");
                    p.setSku("-");
                    p.setBaseUnit("");
                }
                det.setQtyDoneTotal(0);
                lookup.put(det.getProdId(), new ScannedDelItem(det, p));
                isMatched = false;
            }
        }

        delItems = new ArrayList<>(lookup.values());

        initializeList();

    }

    private void initializeList() {
        ListView mListView = findViewById(R.id.searchList);
        DeliveryItemAdapter adapter = new DeliveryItemAdapter(delItems, R.layout.delivery_scan_item, getApplicationContext());
        mListView.setAdapter(adapter);
    }

    private void initializeBtn(final Activity self){
        backBtn = findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(self, ItemScanActivity.class);
                intent.putExtra("type", Constants.DELIVERY);
                intent.putExtra("delivery", delivery.getDeliveryId());
                intent.putExtra("list", items);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        delDoneBtn = findViewById(R.id.deliveryDoneBtn);
        delDoneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(self);
                if(!isMatched) {
                    alertBuilder.setTitle(getString(R.string.delivery_warning));
                    alertBuilder.setMessage(getString(R.string.del_verify_warning));
                    alertBuilder.setNegativeButton(getText(R.string.back_btn), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                } else {
                    alertBuilder.setTitle(getString(R.string.delivery_verify));
                    alertBuilder.setMessage(getString(R.string.del_verify_match));
                }

                alertBuilder.setPositiveButton(getText(R.string.warehouse_cont), new DialogInterface.OnClickListener() {
                @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        DeliveryHelper.setDoneQtyOfDelivery(getApplicationContext(), delivery.getDeliveryId(), delItems);
                        DeliveryHelper.setDeliveryUploadStatus(getApplicationContext(), delivery.getDeliveryId(), "Y");
                        DeliveryHelper.setDeliveryVerifyStatus(getApplicationContext(), delivery.getDeliveryId(), "Y");
                        Intent intent = new Intent(self, DeliverySelectActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }
                });

                final AlertDialog alertDialog = alertBuilder.create();
                alertDialog.show();
            }
        });
    }

}

