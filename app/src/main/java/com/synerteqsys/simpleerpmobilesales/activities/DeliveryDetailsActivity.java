package com.synerteqsys.simpleerpmobilesales.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.synerteqsys.simpleerpmobilesales.R;
import com.synerteqsys.simpleerpmobilesales.data.ScannedDelItem;
import com.synerteqsys.simpleerpmobilesales.data.models.Delivery;
import com.synerteqsys.simpleerpmobilesales.data.models.DeliveryDet;
import com.synerteqsys.simpleerpmobilesales.data.models.Product;
import com.synerteqsys.simpleerpmobilesales.utils.Constants;
import com.synerteqsys.simpleerpmobilesales.utils.database.DeliveryHelper;
import com.synerteqsys.simpleerpmobilesales.utils.database.ProductHelper;
import com.synerteqsys.simpleerpmobilesales.utils.ui.DeliveryItemAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DeliveryDetailsActivity extends AppCompatActivity {
    private ArrayList<ScannedDelItem> delItems;
    private TextView deliveryNo;
    private Delivery delivery;

    Button backBtn;
    Button delDoneBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_verify);
        deliveryNo = findViewById(R.id.deliveryNo);

        Bundle intentBundle = getIntent().getExtras();
        String delId = intentBundle.getString("delivery");
        this.delivery = DeliveryHelper.getDelivery(getApplicationContext(), delId);
        deliveryNo.setText(getString(R.string.delivery_no, delivery.getRecordNo()));

        initializeBtn(this);

        // first, check if the scanned products can be found in the delivery
        Map<String, ScannedDelItem> lookup = new HashMap<>();

        // now check if there have been any products in the delivery that haven't been scanned
        for(DeliveryDet det : delivery.getDets()){
            if(!lookup.containsKey(det.getProdId())){
                Product p = ProductHelper.findBaseProduct(getApplicationContext(), det.getProdId());
                if(p == null){
                    p = new Product();
                    p.setProdName("-");
                    p.setSku("-");
                    p.setBaseUnit("");
                }
                lookup.put(det.getProdId(), new ScannedDelItem(det, p));
            }
        }

        delItems = new ArrayList<>(lookup.values());

        initializeList();

    }

    private void initializeList() {
        ListView mListView = findViewById(R.id.searchList);
        DeliveryItemAdapter adapter = new DeliveryItemAdapter(delItems, R.layout.delivery_scan_item, getApplicationContext());
        mListView.setAdapter(adapter);
    }

    private void initializeBtn(final Activity self){
        backBtn = findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent custIntent = new Intent(self, DeliverySelectActivity.class);
                custIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(custIntent);
                finish();
            }
        });

        delDoneBtn = findViewById(R.id.deliveryDoneBtn);
        delDoneBtn.setText(getText(R.string.warehouse_cont));
        delDoneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(self, ItemScanActivity.class);
                intent.putExtra("type", Constants.DELIVERY);
                intent.putExtra("delivery", delivery.getDeliveryId());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });
    }

}

