package com.synerteqsys.simpleerpmobilesales.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.synerteqsys.simpleerpmobilesales.R;
import com.synerteqsys.simpleerpmobilesales.data.models.CustWarehouse;
import com.synerteqsys.simpleerpmobilesales.utils.database.CustomerHelper;
import com.synerteqsys.simpleerpmobilesales.utils.Constants;

import java.util.Arrays;
import java.util.List;

public class CustWarehouseActivity extends AppCompatActivity {

    private ListView custWhListView;
    private EditText listFilter;

    private String custId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_search);

        custWhListView = findViewById(R.id.searchList);
        listFilter = findViewById(R.id.searchText);
        listFilter.setHint(R.string.search_cust_wh);
        custId = getIntent().getExtras().getString("customer");
        final List<CustWarehouse> custList = Arrays.asList(CustomerHelper.getCustWh(getApplicationContext(), custId));

        ArrayAdapter<CustWarehouse> arrayAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_list_item_1, custList
        );
        custWhListView.setAdapter(arrayAdapter);
        custWhListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long id) {
                CustWarehouse custWh = (CustWarehouse) custWhListView.getItemAtPosition(pos);
                Log.d(Constants.TAG, custWh.getWarehouseName() + " selected");
                startOrder(custWh);
            }
        });

        final ArrayAdapter<CustWarehouse> adapter = arrayAdapter;
        listFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int before, int count) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) { }
        });
    }

    private void startOrder(CustWarehouse wh){
        Intent intent = new Intent(this, ItemScanActivity.class);
        intent.putExtra("type", Constants.SALE_ORDER);
        intent.putExtra("custWh", wh.getCustWarehouseId());
        intent.putExtra("customer", custId);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    public void cancel(View v){
        Intent menuIntent = new Intent(this, CustomerActivity.class);
        setResult(RESULT_CANCELED, menuIntent);
        startActivity(menuIntent);
        finish();
    }
}
