package com.synerteqsys.simpleerpmobilesales.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.synerteqsys.simpleerpmobilesales.R;
import com.synerteqsys.simpleerpmobilesales.data.models.Delivery;
import com.synerteqsys.simpleerpmobilesales.utils.Constants;
import com.synerteqsys.simpleerpmobilesales.utils.database.DeliveryHelper;

import java.util.Arrays;
import java.util.List;

public class DeliverySelectActivity extends FragmentActivity{
    static final int ITEM_NO = 2;

    MyAdapter mAdapter;

    ViewPager mPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final Activity self = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_search);

        mAdapter = new MyAdapter(getSupportFragmentManager());

        mPager = (ViewPager)findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);

        // Watch for button clicks.
        final Button buttonUnverified = findViewById(R.id.goto_first);
        final Button buttonVerified = findViewById(R.id.goto_last);

        buttonUnverified.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPager.setCurrentItem(0);
                buttonUnverified.setBackgroundTintList(
                        getApplicationContext().getResources()
                        .getColorStateList(R.color.button_color_selected));
                buttonVerified.setBackgroundTintList(
                        getApplicationContext().getResources()
                                .getColorStateList(R.color.button_color));
            }
        });
        buttonVerified.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPager.setCurrentItem(ITEM_NO-1);
                buttonUnverified.setBackgroundTintList(
                        getApplicationContext().getResources()
                                .getColorStateList(R.color.button_color));
                buttonVerified.setBackgroundTintList(
                        getApplicationContext().getResources()
                                .getColorStateList(R.color.button_color_selected));
            }
        });

        final Button buttonBack = findViewById(R.id.backDelSelBtn);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent menuIntent = new Intent(self, MainMenuActivity.class);
                setResult(RESULT_CANCELED, menuIntent);
                startActivity(menuIntent);
                finish();
            }
        });
    }


    public static class MyAdapter extends FragmentPagerAdapter {
        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return ITEM_NO;
        }

        @Override
        public Fragment getItem(int position) {
            return ArrayListFragment.newInstance(position);
        }

        public static class ArrayListFragment extends Fragment {
            int mNum;

            /**
             * Create a new instance of CountingFragment, providing "num"
             * as an argument.
             */
            static ArrayListFragment newInstance(int num) {
                ArrayListFragment f = new ArrayListFragment();

                // Supply num input as an argument.
                Bundle args = new Bundle();
                args.putInt("num", num);
                f.setArguments(args);

                return f;
            }

            /**
             * When creating, retrieve this instance's number from its arguments.
             */
            @Override
            public void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                mNum = getArguments() != null ? getArguments().getInt("num") : 1;
            }

            /**
             * The Fragment's UI is just a simple text view showing its
             * instance number.
             */
            @Override
            public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                     Bundle savedInstanceState) {
                View v = inflater.inflate(R.layout.activity_list_search, container, false);
                Button back = v.findViewById(R.id.backSearch);
                back.setVisibility(View.GONE);
                EditText listFilter = v.findViewById(R.id.searchText);
                listFilter.setHint(R.string.search_delivery);

                List<Delivery> delList;
                if(mNum != 1)
                    delList = Arrays.asList(DeliveryHelper.getUnverifiedDeliveries(getContext().getApplicationContext()));
                else
                   delList = Arrays.asList(DeliveryHelper.getVerifiedDeliveries(getContext().getApplicationContext()));

                ArrayAdapter<Delivery> arrayAdapter = new ArrayAdapter<>(
                        this.getActivity(), android.R.layout.simple_list_item_1, delList
                );
                ListView searchListView = v.findViewById(R.id.searchList);
                searchListView.setAdapter(arrayAdapter);
                final ListView fSearch = searchListView;
                searchListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long id) {
                        Delivery del = (Delivery) fSearch.getItemAtPosition(pos);
                        Log.d(Constants.TAG, del.getDeliveryNo() + " selected");
                        startOrder(del);
                    }
                });

                final ArrayAdapter<Delivery> adapter = arrayAdapter;
                listFilter.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int start, int before, int count) { }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        adapter.getFilter().filter(charSequence);
                    }

                    @Override
                    public void afterTextChanged(Editable editable) { }
                });
                return v;
            }


            private void startOrder(Delivery d){
                Intent intent = new Intent(getActivity(), DeliveryDetailsActivity.class);
                intent.putExtra("type", Constants.DELIVERY);
                intent.putExtra("delivery", d.getDeliveryId());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                getActivity().finish();
            }
        }
    }
}
