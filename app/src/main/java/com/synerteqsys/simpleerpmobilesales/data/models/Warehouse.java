package com.synerteqsys.simpleerpmobilesales.data.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by dev06 on 11/01/2018.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Warehouse {
    private String warehouseId;
    private String warehouseName;
    private String status;

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Warehouse{" +
                "warehouseId='" + warehouseId + '\'' +
                ", warehouseName='" + warehouseName + '\'' +
                '}';
    }
}
