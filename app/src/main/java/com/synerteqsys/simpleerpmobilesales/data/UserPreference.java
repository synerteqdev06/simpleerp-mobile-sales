package com.synerteqsys.simpleerpmobilesales.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by dev06 on 04/01/2018.
 */

public class UserPreference {

    private static final String PREFERENCE_ID = "com.simpleerpmobilesales.preferences";

    public static android.content.SharedPreferences getPreferences(Context context){
        return context.getSharedPreferences(PREFERENCE_ID, Context.MODE_PRIVATE);
    }

    public static void clearPreferences(Context context){
        SharedPreferences pref = getPreferences(context);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.apply();
    }

    public static void setStringPreference(Context context, String key, String value){
        SharedPreferences pref = getPreferences(context);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static void setBoolPreference(Context context, String key, boolean value){
        SharedPreferences pref = getPreferences(context);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static String getStringPreference(Context context, String key){
        return getPreferences(context).getString(key,"");
    }

    public static boolean getBoolPreference(Context context, String key){
        return getPreferences(context).getBoolean(key, false);
    }

    public static String getWarehouse(Context context){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getString("warehouse_pref", "");
    }
}