package com.synerteqsys.simpleerpmobilesales.data;

/**
 * Created by ZhenKun on 1/2/2018.
 */

import android.util.Log;

import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.Tracker;

public class ContentTracker<T> extends Tracker<T> {
    private T mContent;

    public ContentTracker(T content){
        mContent=content;
    }

    public void onNewItem(int id, T item) {
        Log.d("content-tracker","creating " + item);
    }

    public void onUpdate(Detector.Detections<T> detectResults, T item) {
        Log.d("content-tracker","updating " + item);
    }



}
