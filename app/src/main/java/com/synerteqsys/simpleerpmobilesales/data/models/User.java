package com.synerteqsys.simpleerpmobilesales.data.models;

import android.org.apache.commons.codec.digest.DigestUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by dev06 on 02/01/2018.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class User {
    private String userId;
    private String userName;
    private String email;
    private String password;
    private String passwordSalt;
    private String serverUrl;
    private String lastLicenseDt;
    private String token;
    private String functions;

    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getPasswordSalt() {
        return passwordSalt;
    }
    public void setPasswordSalt(String passwordSalt) {
        this.passwordSalt = passwordSalt;
    }
    public String getServerUrl() {
        return serverUrl;
    }
    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }
    public String getLastLicenseDt() {
        return lastLicenseDt;
    }
    public void setLastLicenseDt(String lastLicenseDt) {
        this.lastLicenseDt = lastLicenseDt;
    }
    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }
    public String getFunctions() {
        return functions;
    }
    public void setFunctions(String functions) {
        this.functions = functions;
    }

    // check if the user has logged in the main licensing server in the past month
    public boolean isLisenced(){
        Date lastLisence = new Date(Long.parseLong(lastLicenseDt));

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -1);
        Date currentLisence = cal.getTime();

        return lastLisence.after(currentLisence);
    }

    public boolean matchesPassword(String pword){
        String saltedPass = pword + ":" + getPasswordSalt();
        String hash = DigestUtils.sha256Hex(saltedPass);
        return hash.equals(getPassword());
    }
}
