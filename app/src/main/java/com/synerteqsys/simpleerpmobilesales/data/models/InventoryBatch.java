package com.synerteqsys.simpleerpmobilesales.data.models;

/**
 * Serial Numbers of inventory items stored
 * Created by dev06 on 07/05/2018.
 */

public class InventoryBatch {
    private String inventoryBatchNo;
    private String prodId;
    private String status;
    private Integer qtyRemaining;

    public String getInventoryBatchNo() {
        return inventoryBatchNo;
    }

    public void setInventoryBatchNo(String invBatchNo) {
        this.inventoryBatchNo = invBatchNo;
    }

    public String getProdId() {
        return prodId;
    }

    public void setProdId(String prodId) {
        this.prodId = prodId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getQtyRemaining() {
        return qtyRemaining;
    }

    public void setQtyRemaining(Integer qtyRemaining) {
        this.qtyRemaining = qtyRemaining;
    }

    @Override
    public String toString() {
        return "Serial{" +
                "serialNo='" + inventoryBatchNo + '\'' +
                ", prodId='" + prodId + '\'' +
                '}';
    }
}
