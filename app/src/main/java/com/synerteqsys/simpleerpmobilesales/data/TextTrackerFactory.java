package com.synerteqsys.simpleerpmobilesales.data;

import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.text.TextBlock;
import com.synerteqsys.simpleerpmobilesales.utils.ui.DetectProcessor;

/**
 * Created by ZhenKun on 1/2/2018.
 */

public class TextTrackerFactory  implements MultiProcessor.Factory<TextBlock> {
    private DetectProcessor mProcessor;

    public TextTrackerFactory (DetectProcessor processor){
        mProcessor=processor;
    }

    @Override
    public Tracker<TextBlock> create(TextBlock text) {
        mProcessor.invoke(text.getValue(),"ocr");
        return new ContentTracker<>(text);
    }
}