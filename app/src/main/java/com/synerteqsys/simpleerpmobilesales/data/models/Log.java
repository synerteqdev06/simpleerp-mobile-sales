package com.synerteqsys.simpleerpmobilesales.data.models;

public class Log {
    private Integer logId;
    private String logInfo;
    private String logTime;
    private String userId;

    public Log() { }

    public Log(Integer logId, String logInfo, String logTime){
        this.logId = logId;
        this.logInfo = logInfo;
        this.logTime = logTime;
    }

    public Integer getLogId() {
        return logId;
    }

    public void setLogId(Integer logId) {
        this.logId = logId;
    }

    public String getLogInfo() {
        return logInfo;
    }

    public void setLogInfo(String logInfo) {
        this.logInfo = logInfo;
    }

    public String getLogTime() {
        return logTime;
    }

    public void setLogTime(String logTime) {
        this.logTime = logTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
