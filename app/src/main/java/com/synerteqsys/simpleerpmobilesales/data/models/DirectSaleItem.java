package com.synerteqsys.simpleerpmobilesales.data.models;

/**
 * Created by dev06 on 03/01/2018.
 */

public class DirectSaleItem {
    private String directSaleItemId;
    private String directSaleId;
    private String prodId;
    private long qty;
    private String packagingId;
    private double unitPrice;
    private double subTotal;

    public DirectSaleItem(String directSaleItemId, String directSaleId, String prodId, long qty, String packagingId, double unitPrice, double subTotal) {
        this.directSaleItemId = directSaleItemId;
        this.directSaleId = directSaleId;
        this.prodId = prodId;
        this.qty = qty;
        this.packagingId = packagingId;
        this.unitPrice = unitPrice;
        this.subTotal = subTotal;
    }

    public void setDirectSaleItemId(String value){
        this.directSaleItemId= value;
    }
    public String getDirectSaleItemId(){
        return this.directSaleItemId;
    }
    public void setDirectSaleId(String value){
        this.directSaleId= value;
    }
    public String getDirectSaleId(){
        return this.directSaleId;
    }
    public void setProdId(String value){
        this.prodId= value;
    }
    public String getProdId(){
        return this.prodId;
    }
    public void setQty(long value){
        this.qty= value;
    }
    public long getQty(){
        return this.qty;
    }
    public String getPackagingId() {
        return packagingId;
    }
    public void setPackagingId(String packagingId) {
        this.packagingId = packagingId;
    }
    public void setUnitPrice(double value){
        this.unitPrice= value;
    }
    public double getUnitPrice(){
        return this.unitPrice;
    }
    public void setSubTotal(double value){
        this.subTotal= value;
    }
    public double getSubTotal(){
        return this.subTotal;
    }
}
