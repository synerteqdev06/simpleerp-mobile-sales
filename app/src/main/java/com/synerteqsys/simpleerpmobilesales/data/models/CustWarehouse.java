package com.synerteqsys.simpleerpmobilesales.data.models;

/**
 * Created by dev06 on 17/01/2018.
 */

public class CustWarehouse {
    private String custWarehouseId;
    private String warehouseName;
    private String customerId;
    private String status;
    private String userId;

    public CustWarehouse() { }

    public CustWarehouse(String custWarehouseId, String warehouseName, String customerId) {
        this.custWarehouseId = custWarehouseId;
        this.warehouseName = warehouseName;
        this.customerId = customerId;
    }

    public String getCustWarehouseId() {
        return custWarehouseId;
    }

    public void setCustWarehouseId(String custWarehouseId) {
        this.custWarehouseId = custWarehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return warehouseName;
    }
}
