package com.synerteqsys.simpleerpmobilesales.data.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *
 * Created by dev06 on 03/01/2018.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Product {
    private String prodId;
    private String packagingId;
    private String sku;
    private String barcode;
    private String prodName;
    private String baseUnit;
    private double srp;
    private String status;
    private String foundSerial;
    private String[] serial;
    private String[] batch;

    public void setProdId(String value) {
        this.prodId = value;
    }

    public String getProdId() {
        return this.prodId;
    }

    public String getPackagingId() {
        return packagingId;
    }

    public void setPackagingId(String packagingId) {
        this.packagingId = packagingId;
    }

    public void setSku(String value) {
        this.sku = value;
    }

    public String getSku() {
        return this.sku;
    }

    public void setProdName(String value) {
        this.prodName = value;
    }

    public String getProdName() {
        return this.prodName;
    }

    public void setBarcode(String value) {
        this.barcode = value;
    }

    public String getBarcode() {
        return this.barcode;
    }

    public void setBaseUnit(String value) {
        this.baseUnit = value;
    }

    public String getBaseUnit() {
        return this.baseUnit;
    }

    public void setSrp(double value) {
        this.srp = value;
    }

    public double getSrp() {
        return this.srp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString(){
        return prodName;
    }

    public String getFoundSerial() {
        return foundSerial;
    }

    public String[] getSerial() {
        return serial;
    }

    public void setSerial(String[] serial) {
        this.serial = serial;
    }

    public String[] getBatch() {
        return batch;
    }

    public void setBatch(String[] batch) {
        this.batch = batch;
    }

    public String searchSerial(String key){
        for(String s : serial){
            if(s.toLowerCase().contains(key.toLowerCase())){
                foundSerial = s;
                break;
            }
        }
        return foundSerial;
    }
}
