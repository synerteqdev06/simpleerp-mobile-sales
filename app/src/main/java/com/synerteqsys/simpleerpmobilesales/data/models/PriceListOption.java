package com.synerteqsys.simpleerpmobilesales.data.models;

/**
 * Created by dev06 on 12/01/2018.
 */

public class PriceListOption {
    private String priceListId;
    private String priceListName;
    private String userId;

    public PriceListOption() { }

    public PriceListOption(String priceListId, String priceListName) {
        this.priceListId = priceListId;
        this.priceListName = priceListName;
    }

    public String getPriceListId() {
        return priceListId;
    }

    public void setPriceListId(String priceListId) {
        this.priceListId = priceListId;
    }

    public String getPriceListName() {
        return priceListName;
    }

    public void setPriceListName(String priceListName) {
        this.priceListName = priceListName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return priceListName;
    }
}
