package com.synerteqsys.simpleerpmobilesales.data;

import com.synerteqsys.simpleerpmobilesales.data.models.DeliveryDet;
import com.synerteqsys.simpleerpmobilesales.data.models.Product;

import java.io.Serializable;

/**
 * Created by dev06
 * List Item for the Delivery Verify Activity
 */

public class ScannedDelItem implements Serializable{
    private int qtyApproved;
    private int qtyDone;
    private String unit;
    private String desc;
    private String id;
    private String packagingId;
    private String searchId;
    private String displayId;

    public ScannedDelItem () { }

    public ScannedDelItem(ScannedItem item, int qtyApproved){
        this.qtyApproved = qtyApproved;
        qtyDone = item.getQty();
        unit = item.getUnit();
        desc = item.getDesc();
        id = item.getId();
        displayId = item.getDisplayId();
    }

    public ScannedDelItem(DeliveryDet det, Product p){
        qtyApproved = (int) det.getQtyApprovedTotal();
        qtyDone = (int) det.getQtyDoneTotal();
        unit = p.getBaseUnit();
        id = det.getProdId();
        displayId = p.getSku();
        desc = p.getProdName();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getQtyApproved() {
        return qtyApproved;
    }

    public void setQtyApproved(int qtyApproved) {
        this.qtyApproved = qtyApproved;
    }

    public int getQtyDone() {
        return qtyDone;
    }

    public void setQtyDone(int qtyDone) {
        this.qtyDone = qtyDone;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getPackagingId() {
        return packagingId;
    }

    public void setPackagingId(String packagingId) {
        this.packagingId = packagingId;
    }

    public String getSearchId() {
        return searchId;
    }

    public void setSearchId(String searchId) {
        this.searchId = searchId;
    }

    public String getDisplayId() {
        return displayId;
    }

    public void setDisplayId(String displayId) {
        this.displayId = displayId;
    }
}
