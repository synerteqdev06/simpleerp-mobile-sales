package com.synerteqsys.simpleerpmobilesales.data.models;

/**
 * Created by dev06 on 03/01/2018.
 */

public class InventoryIn {
    private String inventoryInId;
    private String status;
    private String remarks;
    private String creationDt;
    private String userId;
    private String warehouseId;
    private InventoryInDet[] items;

    public InventoryIn(String inventoryInId, String status, String remarks, String creationDt,
                       String userId, String warehouseId, InventoryInDet[] items) {
        this.inventoryInId = inventoryInId;
        this.status = status;
        this.remarks = remarks;
        this.creationDt = creationDt;
        this.userId = userId;
        this.warehouseId = warehouseId;
        this.items = items;
    }

    public String getUserId() {
        return userId;
    }
    public void setUserId(String value){
        this.userId = value;
    }
    public void setInventoryInId(String value) {
        this.inventoryInId = value;
    }
    public String getInventoryInId() {
        return this.inventoryInId;
    }
    public void setStatus(String value) {
        this.status = value;
    }
    public String getStatus() {
        return this.status;
    }
    public void setRemarks(String value) {
        this.remarks = value;
    }
    public String getRemarks() {
        return this.remarks;
    }
    public void setCreationDt(String value) {
        this.creationDt = value;
    }
    public String getCreationDt() {
        return this.creationDt;
    }
    public String getWarehouseId() {
        return warehouseId;
    }
    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }
    public InventoryInDet[] getItems() {
        return items;
    }
    public void setItems(InventoryInDet[] items) {
        this.items = items;
    }
}
