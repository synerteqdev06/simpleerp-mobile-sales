package com.synerteqsys.simpleerpmobilesales.data.models;

/**
 * Created by dev06 on 03/01/2018.
 */

public class DirectSale {
    private String directSaleId;
    private String directSaleSessionId;
    private double totalAmountDue;
    private double amountPaid;
    private String status;
    private String creationDt;
    private DirectSaleItem[] items;

    public DirectSale(){
        this.totalAmountDue = 0;
        this.amountPaid = 0;
        this.status = "P";
    }

    public DirectSale(String sessionId){
        this.directSaleSessionId = sessionId;
        this.totalAmountDue = 0;
        this.amountPaid = 0;
        this.status = "P";
    }

    public DirectSale(double totalAmountDue, double amountPaid) {
        this.totalAmountDue = totalAmountDue;
        this.amountPaid = amountPaid;
    }

    public DirectSale(String directSaleId, String directSaleSessionId, double totalAmountDue, double amountPaid,
                      String status, String creationDt, DirectSaleItem[] items) {
        this.directSaleId = directSaleId;
        this.directSaleSessionId = directSaleSessionId;
        this.totalAmountDue = totalAmountDue;
        this.amountPaid = amountPaid;
        this.status = status;
        this.creationDt = creationDt;
        this.items = items;
    }

    public void setDirectSaleId(String value) {
        this.directSaleId = value;
    }
    public String getDirectSaleId() {
        return this.directSaleId;
    }
    public void setDirectSaleSessionId(String value){
        this.directSaleSessionId= value;
    }
    public String getDirectSaleSessionId(){
        return this.directSaleSessionId;
    }
    public void setTotalAmountDue(double value) {
        this.totalAmountDue = value;
    }
    public double getTotalAmountDue() {
        return this.totalAmountDue;
    }
    public void setAmountPaid(double value) {
        this.amountPaid = value;
    }
    public double getAmountPaid() {
        return this.amountPaid;
    }
    public void setStatus(String value) {
        this.status = value;
    }
    public String getStatus() {
        return this.status;
    }
    public void setCreationDt(String value) {
        this.creationDt = value;
    }
    public String getCreationDt() {
        return this.creationDt;
    }
    public DirectSaleItem[] getItems() {
        return items;
    }
    public void setItems(DirectSaleItem[] items) {
        this.items = items;
    }
}
