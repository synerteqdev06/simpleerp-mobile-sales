package com.synerteqsys.simpleerpmobilesales.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.synerteqsys.simpleerpmobilesales.data.models.CustWarehouse;
import com.synerteqsys.simpleerpmobilesales.data.models.Customer;
import com.synerteqsys.simpleerpmobilesales.data.models.Delivery;
import com.synerteqsys.simpleerpmobilesales.data.models.DeliveryDet;
import com.synerteqsys.simpleerpmobilesales.data.models.InventoryBatch;
import com.synerteqsys.simpleerpmobilesales.data.models.PriceList;
import com.synerteqsys.simpleerpmobilesales.data.models.PriceListDet;
import com.synerteqsys.simpleerpmobilesales.data.models.PriceListOption;
import com.synerteqsys.simpleerpmobilesales.data.models.Product;
import com.synerteqsys.simpleerpmobilesales.data.models.Serial;
import com.synerteqsys.simpleerpmobilesales.data.models.Warehouse;

/**
 * Used for holding data downloaded from the server
 * Created by dev06 on 04/01/2018.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserData {
    private Product[] product;
    private Warehouse[] warehouses;
    private Customer[] customer;
    private PriceListOption[] pricelistopt;
    private PriceList[] pricelist;
    private PriceListDet[] pricelistdet;
    private CustWarehouse[] custWarehouse;
    private Delivery[] delivery;
    private DeliveryDet[] deliverydet;
    private Serial[] serial;
    private InventoryBatch[] inventoryBatch;
    private String userId;

    public String getUserId() {
        return userId;
    }
    public void setUserId(String value){
        this.userId = value;
    }

    public Product[] getProduct() {
        return product;
    }

    public void setProduct(Product[] product) {
        this.product = product;
    }

    public Warehouse[] getWarehouses() {
        return warehouses;
    }

    public void setWarehouses(Warehouse[] warehouses) {
        this.warehouses = warehouses;
    }

    public Customer[] getCustomer() {
        return customer;
    }

    public void setCustomer(Customer[] customer) {
        this.customer = customer;
    }

    public PriceList[] getPricelist() {
        return pricelist;
    }

    public void setPricelist(PriceList[] pricelist) {
        this.pricelist = pricelist;
    }

    public PriceListDet[] getPricelistdet() {
        return pricelistdet;
    }

    public void setPricelistdet(PriceListDet[] pricelistdet) {
        this.pricelistdet = pricelistdet;
    }

    public PriceListOption[] getPricelistopt() {
        return pricelistopt;
    }

    public void setPricelistopt(PriceListOption[] pricelistopt) {
        this.pricelistopt = pricelistopt;
    }

    public CustWarehouse[] getCustWarehouse() {
        return custWarehouse;
    }

    public void setCustWarehouse(CustWarehouse[] custWarehouse) {
        this.custWarehouse = custWarehouse;
    }

    public Delivery[] getDelivery() {
        return delivery;
    }

    public void setDelivery(Delivery[] delivery) {
        this.delivery = delivery;
    }

    public DeliveryDet[] getDeliverydet() {
        return deliverydet;
    }

    public void setDeliverydet(DeliveryDet[] deliverydet) {
        this.deliverydet = deliverydet;
    }

    public Serial[] getSerial() {
        return serial;
    }

    public void setSerial(Serial[] serial) {
        this.serial = serial;
    }

    public InventoryBatch[] getInventoryBatch() {
        return inventoryBatch;
    }

    public void setInventoryBatch(InventoryBatch[] inventoryBatch) {
        this.inventoryBatch = inventoryBatch;
    }
}
