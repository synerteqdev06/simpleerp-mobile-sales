package com.synerteqsys.simpleerpmobilesales.data.models;

/**
 * Created by dev06 on 12/01/2018.
 */

public class DeliveryDet {
    private String deliveryDetId;
    private String deliveryId;
    private String prodId;
    private long qtyApprovedTotal;
    private long qtyDoneTotal;
    private String userId;

    public DeliveryDet() { }

    public DeliveryDet(String deliveryDetId, String deliveryId, String prodId,
                       long qtyApprovedTotal, long qtyDoneTotal) {
        this.deliveryDetId = deliveryDetId;
        this.deliveryId = deliveryId;
        this.prodId = prodId;
        this.qtyApprovedTotal = qtyApprovedTotal;
        this.qtyDoneTotal = qtyDoneTotal;
    }

    public String getDeliveryDetId() {
        return deliveryDetId;
    }

    public void setDeliveryDetId(String deliveryDetId) {
        this.deliveryDetId = deliveryDetId;
    }

    public String getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(String deliveryId) {
        this.deliveryId = deliveryId;
    }

    public String getProdId() {
        return prodId;
    }

    public void setProdId(String prodId) {
        this.prodId = prodId;
    }

    public long getQtyApprovedTotal() {
        return qtyApprovedTotal;
    }

    public void setQtyApprovedTotal(long qty) {
        this.qtyApprovedTotal = qty;
    }

    public long getQtyDoneTotal() {
        return qtyDoneTotal;
    }

    public void setQtyDoneTotal(long qtyDoneTotal) {
        this.qtyDoneTotal = qtyDoneTotal;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
