package com.synerteqsys.simpleerpmobilesales.data.models;

/**
 * Created by dev06 on 12/01/2018.
 */

public class PriceListDet {
    private String priceListId;
    private String prodId;
    private double effectivePrice;
    private String userId;

    public String getPriceListId() {
        return priceListId;
    }

    public void setPriceListId(String priceListId) {
        this.priceListId = priceListId;
    }

    public String getProdId() {
        return prodId;
    }

    public void setProdId(String prodId) {
        this.prodId = prodId;
    }

    public double getEffectivePrice() {
        return effectivePrice;
    }

    public void setEffectivePrice(double effectivePrice) {
        this.effectivePrice = effectivePrice;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "PriceListDet{" +
                "priceListId='" + priceListId + '\'' +
                ", prodId='" + prodId + '\'' +
                '}';
    }
}
