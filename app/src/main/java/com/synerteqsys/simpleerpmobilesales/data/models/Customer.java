package com.synerteqsys.simpleerpmobilesales.data.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by dev06 on 11/01/2018.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Customer {
    private String partnerId;
    private String partnerName;
    private String priceListId;
    private String status;
    private String userId;

    public Customer(){ }

    public Customer(String partnerId, String partnerName, String priceListId) {
        this.partnerId = partnerId;
        this.partnerName = partnerName;
        this.priceListId = priceListId;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public String getPriceListId() {
        return priceListId;
    }

    public void setPriceListId(String priceListId) {
        this.priceListId = priceListId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return partnerName;
    }
}
