package com.synerteqsys.simpleerpmobilesales.data;

import java.util.Map;

/**
 * Created by dev06 on 12/01/2018.
 */

public class Response {
    private int status;
    private String message;
    private Map<String, Object> attributes;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, Object> data) {
        this.attributes = data;
    }

    @Override
    public String toString() {
        return "Response{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", attributes=" + attributes +
                '}';
    }
}
