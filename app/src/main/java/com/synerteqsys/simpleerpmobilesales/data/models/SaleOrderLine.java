package com.synerteqsys.simpleerpmobilesales.data.models;

/**
 * Created by dev06 on 12/01/2018.
 */

public class SaleOrderLine {
    private String saleOrderLineId;
    private String saleOrderId;
    private String prodId;
    private long qty;
    private double price;
    private double subTotal;

    public SaleOrderLine(String saleOrderLineId, String saleOrderId, String prodId,
                         long qty, double price, double subTotal) {
        this.saleOrderLineId = saleOrderLineId;
        this.saleOrderId = saleOrderId;
        this.prodId = prodId;
        this.qty = qty;
        this.price = price;
        this.subTotal = subTotal;
    }

    public String getSaleOrderLineId() {
        return saleOrderLineId;
    }

    public void setSaleOrderLineId(String saleOrderLineId) {
        this.saleOrderLineId = saleOrderLineId;
    }

    public String getSaleOrderId() {
        return saleOrderId;
    }

    public void setSaleOrderId(String saleOrderId) {
        this.saleOrderId = saleOrderId;
    }

    public String getProdId() {
        return prodId;
    }

    public void setProdId(String prodId) {
        this.prodId = prodId;
    }

    public long getQty() {
        return qty;
    }

    public void setQty(long qty) {
        this.qty = qty;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(double subTotal) {
        this.subTotal = subTotal;
    }
}
