package com.synerteqsys.simpleerpmobilesales.data.models;


/**
 * Created by dev06 on 03/01/2018.
 */

public class DirectSaleSession {
    private String directSaleSessionId;
    private String salePersonId;
    private String startTime;
    private String endTime;
    private String warehouseId;
    private DirectSale[] sales;

    public DirectSaleSession(String directSaleSessionId, String salePersonId, String startTime,
                             String endTime, String warehouseId, DirectSale[] sales) {
        this.directSaleSessionId = directSaleSessionId;
        this.salePersonId = salePersonId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.warehouseId = warehouseId;
        this.sales = sales;
    }

    public void setDirectSaleSessionId(String value){
        this.directSaleSessionId= value;
    }
    public String getDirectSaleSessionId(){
        return this.directSaleSessionId;
    }
    public void setSalePersonId(String value){
        this.salePersonId= value;
    }
    public String getSalePersonId(){
        return this.salePersonId;
    }
    public void setStartTime(String value){
        this.startTime= value;
    }
    public String getStartTime(){
        return this.startTime;
    }
    public void setEndTime(String value){
        this.endTime= value;
    }
    public String getEndTime(){
        return this.endTime;
    }
    public String getWarehouseId() {
        return warehouseId;
    }
    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }
    public DirectSale[] getSales() {
        return sales;
    }
    public void setSales(DirectSale[] sales) {
        this.sales = sales;
    }
}
