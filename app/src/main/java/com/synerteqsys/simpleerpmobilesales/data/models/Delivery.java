package com.synerteqsys.simpleerpmobilesales.data.models;

/**
 * Created by dev06 on 12/01/2018.
 */

public class Delivery {
    private String deliveryId;
    private String deliveryNo;
    private String deliveryType;
    private String isVerified;
    private String isUploaded;
    private DeliveryDet[] dets;
    private String userId;
    private String status;

    public Delivery() { }

    public Delivery(String deliveryId, String deliveryNo) {
        this.deliveryId = deliveryId;
        this.deliveryNo = deliveryNo;
    }

    public Delivery(String saleOrderId, String customerId, String isVerified, DeliveryDet[] lines) {
        this.deliveryId = saleOrderId;
        this.deliveryNo = customerId;
        this.isVerified= isVerified;
        this.dets = lines;
    }

    public Delivery(String saleOrderId, String customerId, String isVerified, String forUpload, DeliveryDet[] lines) {
        this.deliveryId = saleOrderId;
        this.deliveryNo = customerId;
        this.isVerified= isVerified;
        this.isUploaded = forUpload;
        this.dets = lines;
    }

    public String getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(String deliveryId) {
        this.deliveryId = deliveryId;
    }

    public String getDeliveryNo() {
        return deliveryNo;
    }

    public void setDeliveryNo(String deliveryNo) {
        this.deliveryNo = deliveryNo;
    }

    public String getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }

    public String getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(String isVerified) {
        this.isVerified = isVerified;
    }

    public String getIsUploaded() {
        return isUploaded;
    }

    public void setIsUploaded(String isUploaded) {
        this.isUploaded = isUploaded;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DeliveryDet[] getDets() {
        return dets;
    }

    public void setDets(DeliveryDet[] dets) {
        this.dets = dets;
    }

    public String getRecordNo(){
        return "DR" + deliveryNo;
    }

    public boolean isVerified(){
        return this.isVerified.equals("Y");
    }

    public boolean isUploaded() {return this.isUploaded.equals("Y");}

    public DeliveryDet findDelItem(String prodId){
        for(DeliveryDet d : dets){
            if(prodId.equals(d.getProdId()))
                return d;
        }
        return null;
    }

    @Override
    public String toString() {
        return getRecordNo();
    }
}
