package com.synerteqsys.simpleerpmobilesales.data;

import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.barcode.Barcode;
import com.synerteqsys.simpleerpmobilesales.utils.ui.DetectProcessor;

/**
 * Created by ZhenKun on 1/2/2018.
 */

public class BarcodeTrackerFactory implements MultiProcessor.Factory<Barcode> {
    private DetectProcessor mProcessor;

    public BarcodeTrackerFactory (DetectProcessor processor){
        mProcessor=processor;
    }

    @Override
    public Tracker<Barcode> create(Barcode barcode) {
        mProcessor.invoke(barcode.rawValue,"barcode");
        return new com.synerteqsys.simpleerpmobilesales.data.ContentTracker<>(barcode);
    }
}
