package com.synerteqsys.simpleerpmobilesales.data.models;

/**
 * Created by dev06 on 12/01/2018.
 */

public class SaleOrder {
    private String saleOrderId;
    private String customerId;
    private String creationDt;
    private double amountDue;
    private String remarks;
    private String warehouseId;
    private String custWarehouseId;
    private SaleOrderLine[] lines;
    private String userId;

    public SaleOrder(String saleOrderId, String customerId, String creationDt, double amountDue,
                     String remarks, String warehouseId, String custWarehouseId, SaleOrderLine[] lines) {
        this.saleOrderId = saleOrderId;
        this.customerId = customerId;
        this.creationDt = creationDt;
        this.amountDue = amountDue;
        this.remarks = remarks;
        this.warehouseId = warehouseId;
        this.custWarehouseId = custWarehouseId;
        this.lines = lines;
    }

    public String getSaleOrderId() {
        return saleOrderId;
    }

    public void setSaleOrderId(String saleOrderId) {
        this.saleOrderId = saleOrderId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCreationDt() {
        return creationDt;
    }

    public void setCreationDt(String creationDt) {
        this.creationDt = creationDt;
    }

    public double getAmountDue() {
        return amountDue;
    }

    public void setAmountDue(double amountDue) {
        this.amountDue = amountDue;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getCustWarehouseId() {
        return custWarehouseId;
    }

    public void setCustWarehouseId(String custWarehouseId) {
        this.custWarehouseId = custWarehouseId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public SaleOrderLine[] getLines() {
        return lines;
    }

    public void setLines(SaleOrderLine[] lines) {
        this.lines = lines;
    }
}
