package com.synerteqsys.simpleerpmobilesales.data.models;

/**
 * Serial Numbers of inventory items stored
 * Created by dev06 on 07/05/2018.
 */

public class Serial {
    private String serialNo;
    private String prodId;
    private String status;

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getProdId() {
        return prodId;
    }

    public void setProdId(String prodId) {
        this.prodId = prodId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Serial{" +
                "serialNo='" + serialNo + '\'' +
                ", prodId='" + prodId + '\'' +
                '}';
    }
}
