package com.synerteqsys.simpleerpmobilesales.data.models;

/**
 * Created by dev06 on 03/01/2018.
 */

public class InventoryInDet {
    private String inventoryInDetId;
    private String inventoryInId;
    private String prodId;
    private long qty;

    public InventoryInDet(String inventoryInDetId, String inventoryInId, String prodId, long qty) {
        this.inventoryInDetId = inventoryInDetId;
        this.inventoryInId = inventoryInId;
        this.prodId = prodId;
        this.qty = qty;
    }

    public void setInventoryInDetId(String value) {
        this.inventoryInDetId = value;
    }
    public String getInventoryInDetId() {
        return this.inventoryInDetId;
    }
    public void setInventoryInId(String value) {
        this.inventoryInId = value;
    }
    public String getInventoryInId() {
        return this.inventoryInId;
    }
    public void setProdId(String value) {
        this.prodId = value;
    }
    public String getProdId() {
        return this.prodId;
    }
    public void setQty(long value){
        this.qty= value;
    }
    public long getQty(){
        return this.qty;
    }
}
