package com.synerteqsys.simpleerpmobilesales.data.models;

/**
 * Created by dev06 on 12/01/2018.
 */

public class PriceList {
    private String priceListId;
    private String customerId;
    private String status;
    private String userId;

    public String getPriceListId() {
        return priceListId;
    }

    public void setPriceListId(String priceListId) {
        this.priceListId = priceListId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "PriceList{" +
                "priceListId='" + priceListId + '\'' +
                ", customerId='" + customerId + '\'' +
                '}';
    }
}
