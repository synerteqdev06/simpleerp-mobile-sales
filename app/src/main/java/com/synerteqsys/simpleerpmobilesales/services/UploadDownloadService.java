package com.synerteqsys.simpleerpmobilesales.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.synerteqsys.simpleerpmobilesales.R;
import com.synerteqsys.simpleerpmobilesales.data.Response;
import com.synerteqsys.simpleerpmobilesales.data.UserPreference;
import com.synerteqsys.simpleerpmobilesales.data.models.Delivery;
import com.synerteqsys.simpleerpmobilesales.data.models.DirectSaleSession;
import com.synerteqsys.simpleerpmobilesales.data.models.InventoryIn;
import com.synerteqsys.simpleerpmobilesales.data.models.SaleOrder;
import com.synerteqsys.simpleerpmobilesales.utils.ActionDoneListener;
import com.synerteqsys.simpleerpmobilesales.utils.AsyncHttpPostTask;
import com.synerteqsys.simpleerpmobilesales.utils.Constants;
import com.synerteqsys.simpleerpmobilesales.utils.FormatUtils;
import com.synerteqsys.simpleerpmobilesales.utils.LoginTask;
import com.synerteqsys.simpleerpmobilesales.utils.NetworkUtils;
import com.synerteqsys.simpleerpmobilesales.utils.ResponseParser;
import com.synerteqsys.simpleerpmobilesales.utils.database.DeliveryHelper;
import com.synerteqsys.simpleerpmobilesales.utils.database.DirectSaleHelper;
import com.synerteqsys.simpleerpmobilesales.utils.database.PurchaseOrderHelper;
import com.synerteqsys.simpleerpmobilesales.utils.database.SaleOrderHelper;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * Service that uploads data stored in the phone
 * and downloads updated data from the server
 */
public class UploadDownloadService extends Service {
    private Handler handler = new Handler();
    private static LoginTask loginTask;
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            Log.d(Constants.TAG, "Running UploadDownload thread");
            boolean isOnlineMode = UserPreference.getStringPreference(getApplicationContext(), Constants.MODE).equals(Constants.ONLINE);
            if(NetworkUtils.hasNetwork(getApplicationContext()) && isOnlineMode) {
                try {
                    NetworkUtils.downloadData(getApplicationContext());
                    uploadDirectSales();
                    uploadPurchases();
                    uploadSales();
                    uploadDeliveries();
                } catch (Exception e) {
                    Log.e(Constants.TAG, "Error uploading data to the server");
                    e.printStackTrace();
                }
            } else if(!NetworkUtils.hasNetwork(getApplicationContext()) && isOnlineMode) {
                // once the service finds out that there is no internet, set the user's mode to offline mode
                UserPreference.setStringPreference(getApplicationContext(), Constants.MODE, Constants.OFFLINE);
                Toast.makeText(getApplicationContext(), getText(R.string.offline_broadcast), Toast.LENGTH_SHORT).show();
            } else if(NetworkUtils.hasNetwork(getApplicationContext()) && !isOnlineMode) { // offline mode but internet is detected
                Context c = getApplicationContext();
                // we create the login task thread if it hasn't been created yet
                // if the task is still running, we don't run the task
                if(loginTask == null || loginTask.getStatus() == AsyncTask.Status.FINISHED) {
                    loginTask = new LoginTask(c, UserPreference.getStringPreference(c, Constants.USER_NAME),
                            UserPreference.getStringPreference(c, Constants.PASSWORD));
                    loginTask.setPostActionListener(new ActionDoneListener() {
                        @Override
                        public void handleDone(Object o) {
                            Log.d(Constants.TAG, "Login Task Complete");
                        }
                    });
                    loginTask.execute();
                }
            }
            long interval = getInterval() * 60 * 1000; //convert the minutes into millisec
            Log.d(Constants.TAG, "Interval: " + interval);
            handler.postDelayed(runnable, interval);
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // start the thread once the service has been started
        Log.d(Constants.TAG, "Creating runnable for service");
        handler.post(runnable);
    }

    @Override
    public void onDestroy() {
        handler.removeCallbacks(runnable);
        Log.w(Constants.TAG, "Stopping runnable for upload service");
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(Constants.TAG, "UploadDownload Service Started");
        return super.onStartCommand(intent, flags, startId);
    }

    private void uploadDirectSales() throws ExecutionException, InterruptedException, UnsupportedEncodingException, JsonProcessingException {
        DirectSaleSession[] sessionList = DirectSaleHelper.getDirectSaleSessions(getApplicationContext());
        sendData(sessionList, "sessions");
    }

    private void uploadPurchases() throws JsonProcessingException, UnsupportedEncodingException {
        InventoryIn[] orderList = PurchaseOrderHelper.getPurchaseOrder(getApplicationContext());
        sendData(orderList, "purchases");
    }

    private void uploadSales() throws UnsupportedEncodingException, JsonProcessingException {
        SaleOrder[] orderList = SaleOrderHelper.getSaleOrders(getApplicationContext());
        sendData(orderList, "sales");

    }

    private void uploadDeliveries() throws UnsupportedEncodingException, JsonProcessingException {
        Delivery[] delList = DeliveryHelper.getUnuploadedDeliveries(getApplicationContext());
        sendData(delList, "deliveries");

    }

    private void sendData(Object[] dataList, final String type) throws JsonProcessingException, UnsupportedEncodingException {
        if(dataList.length > 0) {
            ObjectMapper mapper = new ObjectMapper();
            String jsonMap = mapper.writeValueAsString(dataList);
            Log.d(Constants.TAG, "Uploading " + dataList.length + " " + type);
            String sessionData;

            Map<String, Object> data = new LinkedHashMap<>();
            switch(type) {
                case "sessions":
                    data.put("ACTION", Constants.ACT_UPLOAD_SESSION);
                    data.put("sessions", jsonMap); break;
                case "purchases":
                    data.put("ACTION", Constants.ACT_UPLOAD_INVENTORY_IN);
                    data.put("orders", jsonMap); break;
                case "sales":
                    data.put("ACTION", Constants.ACT_UPLOAD_SALE_ORDER);
                    data.put("orders", jsonMap); break;
                case "deliveries":
                    data.put("ACTION", Constants.ACT_UPLOAD_DELIVERY);
                    data.put("deliveries", jsonMap); break;
            }
            data.put("CSRF_TOKEN", UserPreference.getStringPreference(getApplicationContext(), Constants.CSRF_TOKEN));
            sessionData = FormatUtils.formatFormData(data);

            AsyncHttpPostTask uploadSession = new AsyncHttpPostTask(getApplicationContext(), new ResponseParser() {
                @Override
                public void parse(Context context, String response) throws IOException {
                    ObjectMapper mapper = new ObjectMapper();
                    Response resp = mapper.readValue(response, Response.class);
                    if(resp.getStatus() == 1){
                        Map data = (Map) resp.getAttributes().get("data");
                        List<String> records = (List<String>) data.get("records");
                        Log.d(Constants.TAG, "Successfully uploaded " + records.size() + " " + type);

                        Context dbContext = getApplicationContext();
                        switch(type){
                            case "sessions":
                                DirectSaleHelper.deleteSaleSessions(dbContext, records.toArray(new String[0]));
                                break;
                            case "purchases":
                                PurchaseOrderHelper.deletePurchaseOrders(dbContext, records.toArray(new String[0]));
                                break;
                            case "sales":
                                SaleOrderHelper.deleteSaleOrders(dbContext, records.toArray(new String[0]));
                                break;
                            case "deliveries":
                                DeliveryHelper.setDeliveriesAsUploaded(dbContext, records.toArray(new String[0]));
                                break;
                        }
                    } else {
                        Log.w(Constants.TAG, "Something went wrong on the server");
                        Log.w(Constants.TAG, resp.getMessage());
                    }

                }
            }, sessionData);
            uploadSession.execute(
                    UserPreference.getStringPreference(getApplicationContext(), Constants.SERVER) + Constants.UPLOAD_URL,
                    Constants.SERVER_REQUEST_TYPE);

        }
    }

    private long getInterval(){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        return Long.parseLong(sharedPref.getString("interval_pref", "5"));
    }
}
