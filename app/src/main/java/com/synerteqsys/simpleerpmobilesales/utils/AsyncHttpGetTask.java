package com.synerteqsys.simpleerpmobilesales.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

import com.synerteqsys.simpleerpmobilesales.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by dev06 on 03/01/2018.
 */

public class AsyncHttpGetTask extends AsyncTask<String, Integer, Boolean> {
    private Context context;
    private Boolean result;

    private ActionDoneListener postActionListener;

    private PowerManager.WakeLock wakelock;
    private ProgressDialog progressDialog;
    private String progressMessage;
    private String completeMessage;
    private String errorMessage;
    private ResponseParser parser;
    public AsyncHttpGetTask(Context context, ResponseParser parser) {
        this.context = context;
        this.parser = parser;
    }

    public Boolean getResult() {
        return result;
    }

    public void setProgressDialog(ProgressDialog progressDialog) {
        this.progressDialog = progressDialog;
    }

    public ProgressDialog getProgressDialog() {
        return progressDialog;
    }

    public ResponseParser getParser() {
        return parser;
    }

    public ActionDoneListener getPostActionListener() {
        return postActionListener;
    }

    public void setPostActionListener(ActionDoneListener postActionListener) {
        this.postActionListener = postActionListener;
    }

    public Context getContext() {
        return context;
    }

    public void setParser(ResponseParser parser) {
        this.parser = parser;
    }

    public String getProgressMessage() {
        return progressMessage;
    }

    public void setProgressMessage(String message) {
        progressMessage = message;
    }

    public String getCompleteMessage() {
        return completeMessage;
    }

    public void setCompleteMessage(String completeMessage) {
        this.completeMessage = completeMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    protected void onPreExecute() {
        Log.d(Constants.TAG, "Async task started: " + this.getClass().getSimpleName());
        // take CPU lock to prevent CPU from going off if the user
        // presses the power button during download
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        wakelock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                getClass().getName());
        wakelock.acquire();
        if(context instanceof Activity) {
            if(progressDialog==null) {
                progressDialog = new ProgressDialog(context);
                progressDialog.setMessage(getProgressMessage());
                progressDialog.setIndeterminate(true);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setCancelable(true);
                final AsyncHttpGetTask task = this;
                progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        task.cancel(true);
                    }
                });
            }
            progressDialog.show();
        }
        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate(progress);
        if(context instanceof Activity) {
            // if we get here, length is known, now set indeterminate to false
            progressDialog.setIndeterminate(false);
            progressDialog.setMax(100);
            progressDialog.setProgress(progress[0]);
        }
    }

    @Override
    protected void onPostExecute(Boolean result) {
        if(context instanceof Activity) {
            wakelock.release();
            if (result) {
                if (postActionListener != null) {
                    postActionListener.handleDone(this);
                }
                if (getCompleteMessage() != null)
                    Toast.makeText(context.getApplicationContext(), getCompleteMessage(), Toast.LENGTH_LONG).show();
            } else if (getErrorMessage() != null)
                Toast.makeText(context.getApplicationContext(), getErrorMessage(), Toast.LENGTH_SHORT).show();

            if(progressDialog.isShowing())
                progressDialog.dismiss();
        }
    }

    @Override
    protected void onCancelled(Boolean result) {
        wakelock.release();
        Toast.makeText(context.getApplicationContext(), context.getString(R.string.operation_cancelled), Toast.LENGTH_SHORT).show();
        super.onCancelled(result);
    }

    @Override
    protected Boolean doInBackground(String... urls) {
        if (NetworkUtils.hasNetwork(context)) {
            try {
                Log.d(Constants.TAG, "url " + urls[0]);
                URL url = new URL(urls[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    urlConnection.setRequestMethod("GET");
                    urlConnection.setConnectTimeout(10000);
                    urlConnection.connect();
                    if (urlConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                        Log.d(Constants.TAG, "Response code: " + urlConnection.getResponseCode());
                        return false;
                    }
                    return downloadData(urlConnection);
                } catch (IOException e) {
                    Log.w(Constants.TAG, "error downloading data from server", e);
                    e.printStackTrace();
                    return false;
                } finally {
                    urlConnection.disconnect();
                }
            } catch (IOException e) {
                Log.w(Constants.TAG, "error creating url to the server");
                e.printStackTrace();
                return false;
            }
        }
        // no internet available
        return false;
    }

    protected Boolean downloadData(HttpURLConnection urlConnection) throws IOException {
        // this will be useful to display download percentage
        // might be -1: server did not report the length
        int fileLength = urlConnection.getContentLength();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            // download the file
            InputStream input = urlConnection.getInputStream();
            try {
                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    baos.write(data, 0, count);
                    // allow canceling with back button
                    if (isCancelled()) {
                        input.close();
                        return false;
                    }
                    total += count;
                    // publishing the progress....
                    if (fileLength > 0) // only if total length is known
                        publishProgress((int) (total * 100 / fileLength));
                }
                parse(new String(baos.toByteArray(), "utf-8"));
                return true;
            } finally {
                input.close();
            }
        } finally {
            baos.close();
        }
    }

    protected void parse(String s) throws IOException {
        this.parser.parse(this.context, s);
    }

    protected boolean serverIsOnline(URL url){
        try {
            HttpURLConnection urlc;
            urlc = (HttpURLConnection) url.openConnection();
            urlc.setConnectTimeout(6 * 1000);          // 6 s.
            urlc.setReadTimeout(6 * 1000);             // 6 s.
            urlc.setRequestMethod("HEAD");
            urlc.connect();
            if (urlc.getResponseCode() == HttpURLConnection.HTTP_OK) {        // 200 = "OK" code (http connection is fine).
                Log.d(Constants.TAG, url.getPath() + ": Local server is available.");
                return true;
            } else {
                Log.d(Constants.TAG, url.getPath() + ": Local server is unavailable.");
                return false;
            }
        } catch (Exception e) {
            Log.d(Constants.TAG, url.getPath() + ": Unable to get to local server.");
            return false;
        }
    }


}