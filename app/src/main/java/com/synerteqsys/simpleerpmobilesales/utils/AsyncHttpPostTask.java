package com.synerteqsys.simpleerpmobilesales.utils;

import android.content.Context;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;


public class AsyncHttpPostTask extends AsyncHttpGetTask{
    private String postData;

    public AsyncHttpPostTask(Context context, ResponseParser parser, String postData) {
        super(context, parser);
        this.postData= postData;
    }

    /*
     * params: the last url is ALWAYS the content-type of the request form;
     */
    @Override
    protected Boolean doInBackground(String... params) {
        if(NetworkUtils.hasNetwork(getContext())) {
            try {
                Log.d(Constants.TAG, "url " + params[0]);
                URL url = new URL(params[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setConnectTimeout(10000);
                    urlConnection.setRequestProperty("Content-Type", params[params.length - 1]);
                    urlConnection.setDoOutput(true);
                    urlConnection.setDoInput(true);
                    urlConnection.setChunkedStreamingMode(0);
                    urlConnection.connect();
                    OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
                    try {
                        Writer writer = new OutputStreamWriter(out, "UTF-8");
                        try {
                            writer.write(this.postData);
                        } finally {
                            writer.close();
                        }
                    } finally {
                        out.close();
                    }
                    if (urlConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                        Log.d(Constants.TAG, "Response code: " + urlConnection.getResponseCode());
                        return false;
                    }
                    return downloadData(urlConnection);
                } finally {
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                Log.w(Constants.TAG, "unable to connect to server\n", e);
                return false;
            }
        }
        // no internet
        Log.w(Constants.TAG, "No internet connection\n");
        return false;
    }
}
