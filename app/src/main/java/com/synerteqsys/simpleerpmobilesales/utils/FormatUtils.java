package com.synerteqsys.simpleerpmobilesales.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;

/**
 * Created by dev06 on 03/01/2018.
 */

public class FormatUtils {

    public final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("MM-dd-yyyy");
    public final static DecimalFormat FLOAT_FORMAT = new DecimalFormat("#,##0.00");
    private final static DecimalFormat INT_FORMAT = new DecimalFormat("#,##0");

    public final static String formatDate(java.util.Date date) {
        return DATE_FORMAT.format(date);
    }

    public final static String formatInt(Number value) {
        return INT_FORMAT.format(value);
    }

    public final static String formatFloat(Number value) {
        return FLOAT_FORMAT.format(value);
    }

    public final static java.util.Date parseDate(String value) throws ParseException {
        return DATE_FORMAT.parse(value);
    }

    public final static Number parseInt(String value) throws ParseException {
        return INT_FORMAT.parse(value);
    }

    public final static Number parseFloat(String value) throws ParseException {
        return FLOAT_FORMAT.parse(value);
    }

    public final static java.util.Date today() {
        Calendar cd = Calendar.getInstance();
        cd.set(Calendar.HOUR_OF_DAY, 0);
        cd.set(Calendar.MINUTE, 0);
        cd.set(Calendar.SECOND, 0);
        cd.set(Calendar.MILLISECOND, 0);
        return cd.getTime();
    }

    public final static String formatFormData(Map<String, Object> data) throws UnsupportedEncodingException {
        String postString;

        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String,Object> param : data.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        postString = postData.toString();

        return postString;
    }
}