package com.synerteqsys.simpleerpmobilesales.utils;

/**
 * Created by dev06 on 03/01/2018.
 */

public interface ActionDoneListener {
    public void handleDone(Object o);
}