package com.synerteqsys.simpleerpmobilesales.utils.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.synerteqsys.simpleerpmobilesales.R;
import com.synerteqsys.simpleerpmobilesales.activities.ItemScanActivity;
import com.synerteqsys.simpleerpmobilesales.data.models.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by dev06 on 22/02/2018.
 */

public class ProductListAdapter extends BaseAdapter {
    Context mContext;
    LayoutInflater inflater;
    private ArrayList<Product> prodArrList;
    private List<Product> prodList;


    public ProductListAdapter(Context mContext, List<Product> prodList) {
        this.mContext = mContext;
        this.inflater = LayoutInflater.from(mContext);
        this.prodList = new ArrayList<>(prodList);
        this.prodArrList = new ArrayList<>();
        this.prodArrList.addAll(prodList);
    }

    class ViewHolder {
        TextView name;
        TextView unit;
        TextView sku;
        TextView barcode;
    }

    @Override
    public int getCount() {
        return prodList.size();
    }

    @Override
    public Object getItem(int i) {
        return prodList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        final ViewHolder holder;
        if(view == null){
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.product_item, null);
            holder.name = view.findViewById(R.id.name);
            holder.barcode = view.findViewById(R.id.barcode);
            holder.sku = view.findViewById(R.id.sku);
            holder.unit = view.findViewById(R.id.unit);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        final Product p = prodList.get(position);
        holder.name.setText(p.getProdName()==null ? "" : p.getProdName());
        if(((ItemScanActivity) mContext).isSearchSerial()){
            holder.sku.setText(p.getFoundSerial()==null ? "" : p.getFoundSerial());
            holder.barcode.setText("");
        }else{
            holder.barcode.setText(p.getBarcode()==null ? "" : p.getBarcode());
            holder.sku.setText(p.getSku()==null ? "" : p.getSku());
        }
        holder.unit.setText(p.getBaseUnit()==null ? "" : p.getBaseUnit());

        final int prodPosition = position;
        view.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                ItemScanActivity scan = (ItemScanActivity) mContext;
                scan.addProductToList(p.getPackagingId(), true);
            }
        });

        return view;
    }

    public void filter(CharSequence charText){

        String value = charText.toString();
        prodList.clear();
        if(value.length() == 0){
            prodList.addAll(prodArrList);
        } else {
            if(((ItemScanActivity) mContext).isSearchSerial()){
                for (Product p : prodArrList) {
                    String serialSearch = p.searchSerial(charText.toString());

                    if (serialSearch != null && !serialSearch.isEmpty()) {
                        prodList.add(p);
                    }
                }
            } else {
                // first check if the user scans the item with the barcode or the SKU or both
                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(mContext.getApplicationContext());
                Set<String> identifiers = sharedPref.getStringSet("scanner_pref", null);
                boolean usesSku = false, usesBarcode = false;
                if (identifiers != null) {
                    for (String ident : identifiers) {
                        if (ident.equals("sku")) {
                            usesSku = true;
                        } else if (ident.equals("barcode")) {
                            usesBarcode = true;
                        }
                    }
                }
                for (Product p : prodArrList) {
                    boolean matchesName = p.getProdName().toLowerCase().contains(value.toLowerCase());
                    boolean matchesSku = usesSku && p.getSku() != null && p.getSku().toLowerCase().contains(value.toLowerCase());
                    boolean matchesBarcode = usesBarcode && p.getBarcode() != null && p.getBarcode().toLowerCase().contains(value.toLowerCase());

                    if (matchesName || matchesSku || matchesBarcode) {
                        prodList.add(p);
                    }
                }
            }
            notifyDataSetChanged();
        }
    }

}
