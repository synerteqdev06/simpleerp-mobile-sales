package com.synerteqsys.simpleerpmobilesales.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.synerteqsys.simpleerpmobilesales.R;
import com.synerteqsys.simpleerpmobilesales.data.UserData;
import com.synerteqsys.simpleerpmobilesales.data.UserPreference;
import com.synerteqsys.simpleerpmobilesales.utils.database.DatabaseHelper;
import com.synerteqsys.simpleerpmobilesales.utils.database.UserHelper;

import java.io.IOException;

/**
 * Created by dev06 on 12/01/2018.
 */

public class NetworkUtils implements ResponseParser{
    public static boolean hasNetwork(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void downloadData(Context c){
        Context appContext = c.getApplicationContext();
        String serverUrl = UserPreference.getStringPreference(appContext, Constants.SERVER);
        String warehouse = UserPreference.getWarehouse(appContext);
        if(!warehouse.isEmpty()) {
            Log.d(Constants.TAG, "Downloading data from server");

            String params = "";
            String dlTime = UserHelper.getLastDlTime(appContext);
            if(!(dlTime == null || dlTime.isEmpty()))
                params += "&lastDownload=" + dlTime;

            AsyncHttpGetTask task = new AsyncHttpGetTask(c, new NetworkUtils());
            task.setProgressMessage(c.getString(R.string.downloading));
            task.execute(serverUrl + Constants.PRODUCT_URL + params);

            params += "&warehouse=" + warehouse;
            task = new AsyncHttpGetTask(c, new NetworkUtils());
            task.setProgressMessage(c.getString(R.string.downloading));
            task.execute(serverUrl + Constants.CUSTOMER_URL + params);

            task = new AsyncHttpGetTask(c, new NetworkUtils());
            task.setProgressMessage(c.getString(R.string.downloading));
            task.execute(serverUrl + Constants.CUSTOMER_WH_URL + params);

            task = new AsyncHttpGetTask(c, new NetworkUtils());
            task.setProgressMessage(c.getString(R.string.downloading));
            task.execute(serverUrl + Constants.PRICELIST_URL + params);

            task = new AsyncHttpGetTask(c, new NetworkUtils());
            task.setProgressMessage(c.getString(R.string.downloading));
            task.execute(serverUrl + Constants.PRICELIST_DET_URL + params);

            task = new AsyncHttpGetTask(c, new NetworkUtils());
            task.setProgressMessage(c.getString(R.string.downloading));
            task.execute(serverUrl + Constants.PRICELIST_OPTION_URL + params);

            task = new AsyncHttpGetTask(c, new NetworkUtils());
            task.setProgressMessage(c.getString(R.string.downloading));
            task.execute(serverUrl + Constants.DELIVERY_URL + params);

            task = new AsyncHttpGetTask(c, new NetworkUtils());
            task.setProgressMessage(c.getString(R.string.downloading));
            task.execute(serverUrl + Constants.SERIAL_URL + params);

            dlTime = Long.toString(System.currentTimeMillis());
            UserHelper.updateLastDlTime(appContext, dlTime);
        }
    }

    @Override
    public void parse(Context context, String response) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        UserData data = mapper.readValue(response, UserData.class);
        DatabaseHelper.processData(context, data);
    }

}
