package com.synerteqsys.simpleerpmobilesales.utils.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.synerteqsys.simpleerpmobilesales.data.UserData;
import com.synerteqsys.simpleerpmobilesales.data.models.Product;
import com.synerteqsys.simpleerpmobilesales.data.models.Serial;
import com.synerteqsys.simpleerpmobilesales.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class SerialHelper {
    public static Product findSerial(Context context, String serialNo){
        Product p = null;
        DatabaseHelper helper = DatabaseHelper.getInstance(context);

        String selectSerial = "select prod_id from serial where serial_no=?";
        Log.d(Constants.TAG, "Serial search: " + selectSerial);

        try(SQLiteDatabase db = helper.getReadableDatabase() ) {
            Cursor c = db.rawQuery(selectSerial, new String[]{serialNo});
            if (c.moveToFirst()) {
                String prodId = c.getString(c.getColumnIndex("prod_id"));
                Log.d(Constants.TAG, "Find product id: " + prodId);
                p = ProductHelper.findBaseProduct(context, prodId);
            }
            c.close();
        }
        return p;
    }

    static String[] getSerial(Context context, Product p){
        List<String> serialList = new ArrayList<>();
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String selectSql = "select serial_no " +
                "from serial where prod_id=?";
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor c = db.rawQuery(selectSql, new String[]{ p.getProdId()});
        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                serialList.add(c.getString(c.getColumnIndex("serial_no")));
                c.moveToNext();
            }
        }
        c.close();

        Log.d(Constants.TAG, "Found " + serialList.size() + " serial numbers");
        return serialList.toArray(new String[0]);
    }

    static void processSerial(Context context, UserData data) {
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.beginTransaction();
        for (Serial serial : data.getSerial()) {
            db.delete("serial", "serial_no=?", new String[]{serial.getSerialNo()});
            if(serial.getStatus().equals("I") || serial.getStatus().equals("R")) {
                ContentValues cv = new ContentValues();
                cv.put("serial_no", serial.getSerialNo());
                cv.put("prod_id", serial.getProdId());
                db.insert("serial", null, cv);
                Log.d(Constants.TAG, serial.toString());
            }
        }
        db.setTransactionSuccessful();
        db.endTransaction();
    }
}
