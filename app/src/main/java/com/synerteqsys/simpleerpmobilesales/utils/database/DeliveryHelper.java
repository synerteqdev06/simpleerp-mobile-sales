package com.synerteqsys.simpleerpmobilesales.utils.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.synerteqsys.simpleerpmobilesales.data.ScannedDelItem;
import com.synerteqsys.simpleerpmobilesales.data.UserData;
import com.synerteqsys.simpleerpmobilesales.data.models.Delivery;
import com.synerteqsys.simpleerpmobilesales.data.models.DeliveryDet;
import com.synerteqsys.simpleerpmobilesales.utils.Constants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DeliveryHelper {
    static void processDelivery(Context context, UserData data) {
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        final List delStat = Arrays.asList(new String[]{"A", "T"});
        String userId = DatabaseHelper.getUserId(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.beginTransaction();
        for (Delivery delivery : data.getDelivery()) {
            db.delete("delivery", "user_id=? and delivery_id=?",
                    new String[]{userId,delivery.getDeliveryId()});
            if(delStat.contains(delivery.getStatus())) {
                ContentValues cv = new ContentValues();
                cv.put("delivery_id", delivery.getDeliveryId());
                cv.put("delivery_no", delivery.getDeliveryNo());
                cv.put("delivery_type", delivery.getDeliveryType());
                cv.put("is_verified", "N");
                cv.put("for_upload", "N");
                cv.put("user_id", userId);
                db.insert("delivery", null, cv);
            } else {
                db.delete("delivery_det", "user_id=? and delivery_id=?",
                        new String[]{userId,delivery.getDeliveryId()});
            }
            Log.d(Constants.TAG, delivery.toString() + ": " + userId);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    static void processDeliveryDet(Context context, UserData data) {
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String userId = DatabaseHelper.getUserId(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.beginTransaction();
        for (DeliveryDet det : data.getDeliverydet()) {
            if(det.getQtyApprovedTotal() > 0) {
                db.delete("delivery_det", "user_id=? and delivery_id=? and prod_id=?",
                        new String[]{userId, det.getDeliveryId(), det.getProdId()});
                ContentValues cv = new ContentValues();
                cv.put("delivery_id", det.getDeliveryId());
                cv.put("delivery_det_id", det.getDeliveryDetId());
                cv.put("prod_id", det.getProdId());
                cv.put("qty_approved_total", det.getQtyApprovedTotal());
                cv.put("qty_done_total", det.getQtyDoneTotal());
                cv.put("user_id", userId);
                db.insert("delivery_det", null, cv);
                Log.d(Constants.TAG, det.toString() + ": " + userId);
            }
        }
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    /**
     * Gets a delivery from the database given the delivery ID
     * @param context context at time of retrieval
     * @param delId the id of the delivery being retrieved
     * @return The Delivery object from the database. Returns null if the delivery isn't found
     */
    public static Delivery getDelivery(Context context, String delId){
        Delivery delivery = null;
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String selectSql = "select delivery_no, is_verified " +
                "from delivery where user_id=? and delivery_id=?";
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor c = db.rawQuery(selectSql, new String[]{DatabaseHelper.getUserId(context), delId});
        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                delivery = new Delivery(
                        delId,
                        c.getString(c.getColumnIndex("delivery_no")),
                        c.getString(c.getColumnIndex("is_verified")),
                        getDeliveryDet(context, delId));
                c.moveToNext();
            }
        }
        c.close();

        Log.d(Constants.TAG, "Found delivery: " + delId);
        return delivery;

    }

    /**
     * Deletes a delivery and all of its delivery details
     * @param context context at the time of deletion
     * @param delId The id of the delivery to be deleted
     */
    public static void deleteDelivery(Context context, String delId){
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.beginTransaction();
        db.delete("delivery", "delivery_id=?", new String[]{delId});
        db.delete("delivery_det", "delivery_id=?", new String[]{delId});
        Log.d(Constants.TAG, "Deleted DR"+delId);
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    /**
     * Gets the list of deliveries that haven't been uploaded to the server yet
     * @param context context at the time of upload
     * @return list of unuploaded deliveries
     */
    public static Delivery[] getUnuploadedDeliveries(Context context) {
        List<Delivery> deliveries = new ArrayList<>();
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String selectSql = "select delivery_id, delivery_no, is_verified " +
                "from delivery where user_id=? and for_upload='Y' order by delivery_no collate nocase";
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor c = db.rawQuery(selectSql, new String[]{DatabaseHelper.getUserId(context)});
        if (c.moveToFirst()) {
            while (!c.isAfterLast()) {
                String delId = c.getString(c.getColumnIndex("delivery_id"));
                deliveries.add(new Delivery(
                        delId,
                        c.getString(c.getColumnIndex("delivery_no")),
                        c.getString(c.getColumnIndex("is_verified")),
                        "Y",
                        getDeliveryDet(context, delId)
                ));
                c.moveToNext();
            }
        }
        c.close();

        return deliveries.toArray(new Delivery[0]);
    }

    /**
     * Gets deliveries that haven't been verified yet
     * @param context the context at the time of retrieval
     * @return the list of unverified deliveries
     */
    public static Delivery[] getUnverifiedDeliveries(Context context){
        return getDeliveries(context, "N");
    }

    /**
     * Gets deliveries that have been verified
     * @param context the context at the time of retrieval
     * @return the list of verified deliveries
     */
    public static Delivery[] getVerifiedDeliveries(Context context){
        return getDeliveries(context, "Y");
    }

    private static Delivery[] getDeliveries(Context context, String isVerified){
        List<Delivery> deliveries = new ArrayList<>();
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String selectSql = "select delivery_id, delivery_no, is_verified " +
                "from delivery where user_id=? and is_verified=? " +
                "order by delivery_no collate nocase";
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor c = db.rawQuery(selectSql, new String[]{DatabaseHelper.getUserId(context), isVerified});
        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                String delId = c.getString(c.getColumnIndex("delivery_id"));
                deliveries.add(new Delivery(
                        delId,
                        c.getString(c.getColumnIndex("delivery_no")),
                        c.getString(c.getColumnIndex("is_verified")),
                        getDeliveryDet(context, delId)
                ));
                c.moveToNext();
            }
        }
        c.close();

        Log.d(Constants.TAG, "Found " + deliveries.size() + " deliveries");
        return deliveries.toArray(new Delivery[0]);

    }

    /**
     * Sets the deliveries passed as uploaded
     * @param context Context at the time of update
     * @param dels The deliveries to be updated
     */
    public static void setDeliveriesAsUploaded(Context context, String[] dels) {
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        SQLiteDatabase db = helper.getWritableDatabase();
            db.beginTransaction();
            for (String delId : dels) {
                ContentValues cv = new ContentValues();
                cv.put("for_upload", "N");
                db.update("delivery", cv, "delivery_id=? and user_id=?", new String[]{delId, DatabaseHelper.getUserId(context)});
            }
            db.setTransactionSuccessful();
            db.endTransaction();
    }

    /**
     * Updates whether a delivery has been uploaded or not
     * @param context Context at the time of update
     * @param delId The delivery ID of the delivery to be updated
     */
    public static void setDeliveryUploadStatus(Context context, String delId, String upload){
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        SQLiteDatabase db = helper.getWritableDatabase();
            db.beginTransaction();
            ContentValues cv = new ContentValues();
            cv.put("for_upload", upload);
            db.update("delivery", cv, "delivery_id=? and user_id=?", new String[]{delId, DatabaseHelper.getUserId(context)});
            db.setTransactionSuccessful();
            db.endTransaction();
    }

    /**
     * Updates whether a delivery has been verified or not
     * @param context Context at the time of update
     * @param delId The delivery ID of the delivery to be updated
     */
    public static void setDeliveryVerifyStatus(Context context, String delId, String verified){
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.beginTransaction();
        ContentValues cv = new ContentValues();
        cv.put("is_verified", verified);
        db.update("delivery", cv, "delivery_id=? and user_id=?", new String[]{delId, DatabaseHelper.getUserId(context)});
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    /**
     * Set the done quantities of the delivery items.
     * @param context context at the time of update
     * @param delId delivery the items belong to
     * @param deItems the delivery items holding the qty done
     */
    public static void setDoneQtyOfDelivery(Context context, String delId, ArrayList<ScannedDelItem> deItems){
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.beginTransaction();
        for(ScannedDelItem item : deItems) {
            ContentValues cv = new ContentValues();
            cv.put("qty_done_total", item.getQtyDone());
            db.update("delivery_det", cv, "delivery_id=? and user_id=? and prod_id=?",
                           new String[]{delId, DatabaseHelper.getUserId(context), item.getId()});
            Log.d(Constants.TAG, "Setting " + item.getQtyDone() + " as qty_done for " + item.getId());
        }
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    private static DeliveryDet[] getDeliveryDet(Context context, String delId){
        List<DeliveryDet> deliveryDets = new ArrayList<>();
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String selectSql = "select delivery_det_id, delivery_id, prod_id, qty_approved_total, qty_done_total " +
                "from delivery_det where user_id=? and delivery_id =?";
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor c = db.rawQuery(selectSql, new String[]{DatabaseHelper.getUserId(context), delId});
        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                deliveryDets.add(new DeliveryDet(
                        c.getString(c.getColumnIndex("delivery_det_id")),
                        c.getString(c.getColumnIndex("delivery_id")),
                        c.getString(c.getColumnIndex("prod_id")),
                        c.getLong(c.getColumnIndex("qty_approved_total")),
                        c.getLong(c.getColumnIndex("qty_done_total"))
                ));
                c.moveToNext();
            }
        }
        c.close();

        Log.d(Constants.TAG, "Found " + deliveryDets.size() + " details for " + delId);
        return deliveryDets.toArray(new DeliveryDet[0]);
    }

}
