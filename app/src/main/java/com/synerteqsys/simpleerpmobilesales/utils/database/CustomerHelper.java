package com.synerteqsys.simpleerpmobilesales.utils.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.synerteqsys.simpleerpmobilesales.data.UserData;
import com.synerteqsys.simpleerpmobilesales.data.models.CustWarehouse;
import com.synerteqsys.simpleerpmobilesales.data.models.Customer;
import com.synerteqsys.simpleerpmobilesales.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class CustomerHelper {

    /**
     * Insert customers into the database. Will also update the customer record if there are any changes done to it.
     * @param context context at the time of insertion
     * @param data the data containing the list of customers to that will be added into the database
     */
    static void processCustomers(Context context, UserData data) {
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String userId = DatabaseHelper.getUserId(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.beginTransaction();
        StringBuilder logBuilder = new StringBuilder();
        for (Customer customer : data.getCustomer()) {
            int numDeleted = db.delete("customer", "user_id=? and partner_id=?", new String[]{userId, customer.getPartnerId()});
            if(customer.getStatus().equals(Constants.ACTIVE)) {
                ContentValues cv = new ContentValues();
                cv.put("partner_id", customer.getPartnerId());
                cv.put("partner_name", customer.getPartnerName());
                cv.put("price_list_id", customer.getPriceListId());
                cv.put("user_id", userId);
                db.insert("customer", null, cv);
                Log.d(Constants.TAG, customer.toString() + ": " + userId);
                if(numDeleted > 0){
                    // record has been updated
                    logBuilder.append("Customer " + customer.getPartnerName() + " updated. ");
                }
            } else if(numDeleted > 0){
                // record is just deleted
                logBuilder.append("Customer " + customer.getPartnerName() + " deleted. ");
            }
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        // add logs if any row has been updated/deleted
        if(logBuilder.length() != 0)
            LogHelper.addLog(context, logBuilder.toString());
    }

    /**
     *
     * @param context
     * @param data
     */
    static void processCustWarehouse(Context context, UserData data) {
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String userId = DatabaseHelper.getUserId(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.beginTransaction();
        StringBuilder logBuilder = new StringBuilder();
        for (CustWarehouse custWh : data.getCustWarehouse()) {
            int numDeleted = db.delete("cust_warehouse", "user_id=? and cust_warehouse_id=?", new String[]{userId, custWh.getCustWarehouseId()});
            if(custWh.getStatus().equals(Constants.ACTIVE)) {
                ContentValues cv = new ContentValues();
                cv.put("cust_warehouse_id", custWh.getCustWarehouseId());
                cv.put("warehouse_name", custWh.getWarehouseName());
                cv.put("customer_id", custWh.getCustomerId());
                cv.put("user_id", userId);
                db.insert("cust_warehouse", null, cv);
                Log.d(Constants.TAG, custWh.toString() + ": " + userId);
            }
        }
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    /**
     * Gets the list of customers of the user from the database
     * @param context context at time of retrieval
     * @return Array of Customer retrieved from the database
     */
    public static Customer[] getCustomers(Context context){
        List<Customer> customerList = new ArrayList<>();
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String selectSql = "select partner_id, partner_name, price_list_id " +
                "from customer where user_id=? order by partner_name collate nocase";
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor c = db.rawQuery(selectSql, new String[]{DatabaseHelper.getUserId(context)});
        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                customerList.add(new Customer(
                        c.getString(c.getColumnIndex("partner_id")),
                        c.getString(c.getColumnIndex("partner_name")),
                        c.getString(c.getColumnIndex("price_list_id"))
                ));
                c.moveToNext();
            }
        }
        c.close();

        Log.d(Constants.TAG, "Found " + customerList.size() + " customers");
        return customerList.toArray(new Customer[0]);

    }

    /**
     * Gets the list of Customer Warehouses for a given customer
     * @param context context at time of retrieval
     * @param custId the id of the customer that the warehouses belong to
     * @return An array of CustWarehouse of the customer
     */
    public static CustWarehouse[] getCustWh(Context context, String custId){
        List<CustWarehouse> custWhList = new ArrayList<>();
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String selectSql = "select cust_warehouse_id, warehouse_name, customer_id " +
                "from cust_warehouse where user_id=? and customer_id=? order by warehouse_name collate nocase";
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor c = db.rawQuery(selectSql, new String[]{DatabaseHelper.getUserId(context), custId});
        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                custWhList.add(new CustWarehouse(
                        c.getString(c.getColumnIndex("cust_warehouse_id")),
                        c.getString(c.getColumnIndex("warehouse_name")),
                        c.getString(c.getColumnIndex("customer_id"))
                ));
                c.moveToNext();
            }
        }
        c.close();

        Log.d(Constants.TAG, "Found " + custWhList.size() + " customer warehouses");
        return custWhList.toArray(new CustWarehouse[0]);

    }
}
