package com.synerteqsys.simpleerpmobilesales.utils;

/**
 * Created by dev06 on 03/01/2018.
 */

public class Constants {
    public static final String TAG = "simpleerp";

    public static final String SERVER_REQUEST_TYPE= "application/x-www-form-urlencoded; charset=UTF-8";
//    public static final String AUTH_SERVER= "http://192.168.2.85:14697/Auth_App/login";
    public static final String AUTH_SERVER= "http://demo.synerteqsys.com/Auth/login";
//    public static final String AUTH_SERVER= "www.auth.synerteqsys.com";
    public static final String LOGIN_URL = "/rs/action/main";
    public static final String LOGIN_PAGE = "/#!/login";
    public static final String ACT_LOGIN = "ACT_LOGIN";

    public static final String PRODUCT_URL = "/rs/action/mobile?ACTION=ACT_RETRIEVE_MOBILE_PRODUCT";
    public static final String WAREHOUSE_URL = "/rs/action/mobile?ACTION=ACT_AVAILABLE_WAREHOUSE";
    public static final String CUSTOMER_URL = "/rs/action/mobile?ACTION=ACT_AVAILABLE_CUSTOMER";
    public static final String CUSTOMER_WH_URL = "/rs/action/mobile?ACTION=ACT_RETRIEVE_MOBILE_CUST_WAREHOUSE";
    public static final String PRICELIST_URL = "/rs/action/mobile?ACTION=ACT_RETRIEVE_MOBILE_PRICELIST";
    public static final String PRICELIST_DET_URL = "/rs/action/mobile?ACTION=ACT_RETRIEVE_MOBILE_PRICELIST_DET";
    public static final String PRICELIST_OPTION_URL = "/rs/action/mobile?ACTION=ACT_RETRIEVE_MOBILE_PRICELIST_OPTION";
    public static final String DELIVERY_URL = "/rs/action/mobile?ACTION=ACT_RETRIEVE_MOBILE_DELIVERY";
    public static final String SERIAL_URL = "/rs/action/mobile?ACTION=ACT_RETRIEVE_MOBILE_SERIAL";

    public static final String UPLOAD_URL = "/rs/action/mobile";
    public static final String ACT_UPLOAD_SESSION = "ACT_UPLOAD_SESSION";
    public static final String ACT_UPLOAD_INVENTORY_IN = "ACT_UPLOAD_INVENTORY_IN";
    public static final String ACT_UPLOAD_SALE_ORDER = "ACT_UPLOAD_SALE_ORDER";
    public static final String ACT_UPLOAD_DELIVERY = "ACT_UPLOAD_DELIVERY";

    public static final String DIRECT_SALE_ORDER = "DIRECT_SALE";
    public static final String PURCHASE_ORDER = "PURCHASE";
    public static final String SALE_ORDER = "SALE";
    public static final String DELIVERY = "DELIVERY";
    public static final String PRICE_SEARCH = "PRICE_SEARCH";

    // PREFERENCE KEYS
    public static final String MODE = "MODE";
    public static final String LOGGED_IN = "LOGGED_IN";
    public static final String USER_ID = "USER_ID";
    public static final String USER_NAME = "USER_NAME";
    public static final String SERVER = "SERVER";
    public static final String PASSWORD = "PASSWORD";
    public static final String CSRF_TOKEN = "CSRF_TOKEN";
    public static final String DS_SESSION = "DS_SESSION"; // Direct Sale Session

    // PREFERENCE PRE-DEFINED VALUES
    public static final String ONLINE = "ONLINE";
    public static final String OFFLINE = "OFFLINE";

    public static final String ACTIVE = "A";
}
