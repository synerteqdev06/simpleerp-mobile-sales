package com.synerteqsys.simpleerpmobilesales.utils.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.synerteqsys.simpleerpmobilesales.data.models.DirectSale;
import com.synerteqsys.simpleerpmobilesales.data.models.DirectSaleItem;
import com.synerteqsys.simpleerpmobilesales.data.models.DirectSaleSession;
import com.synerteqsys.simpleerpmobilesales.utils.Constants;
import com.synerteqsys.simpleerpmobilesales.utils.FormatUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DirectSaleHelper {
    /**
     * Creates a direct sale session with the current time, current user,
     * and warehouse of the device
     *
     * @param context context at the time of creation
     * @return the id of the newly created direct sale session
     */
    public static String createSaleSession(Context context){
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String code = DatabaseHelper.getGUID();
        String userId = DatabaseHelper.getUserId(context);
        String time = Long.toString(new Date().getTime());
        try(SQLiteDatabase db = helper.getWritableDatabase() ){
            db.beginTransaction();
            ContentValues cv = new ContentValues();
            cv.put("direct_sale_session_id", code);
            cv.put("sale_person_id", userId);
            cv.put("start_time", time);
            cv.put("warehouse_id", DatabaseHelper.getWarehouse(context));
            db.insert("direct_sale_session", null, cv);
            db.setTransactionSuccessful();
            db.endTransaction();
            Log.d(Constants.TAG, "Session started at " + time);
        }
        return code;
    }

    /**
     * Closes the given direct sale session at the current time.
     *
     * @param context context at the time of closing
     * @param sessionId Direct Sale Session that is to be closed
     */
    public static void closeSaleSession(Context context, String sessionId){
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String time = Long.toString(new Date().getTime());
        try(SQLiteDatabase db = helper.getWritableDatabase() ) {
            db.beginTransaction();
            ContentValues cv = new ContentValues();
            cv.put("end_time", time);
            db.update("direct_sale_session", cv, "direct_sale_session_id=?", new String[]{sessionId});
            db.setTransactionSuccessful();
            db.endTransaction();
        }
    }

    /**
     * Deletes direct sale sessions that have been passed to it
     * @param context context at the time of deletion
     * @param sessions the direct sale sessions to be deleted
     */
    public static void deleteSaleSessions(Context context, String[] sessions){
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        try(SQLiteDatabase db = helper.getWritableDatabase() ) {
            db.beginTransaction();
            for(String sessionId : sessions) {
                String deleteSaleItem = "delete from direct_sale_item where direct_sale_id in (select direct_sale_id from direct_sale where direct_sale_session_id=?)";
                db.execSQL(deleteSaleItem, new String[]{sessionId});
                db.delete("direct_sale", "direct_sale_session_id=?", new String[]{sessionId});
                db.delete("direct_sale_session", "direct_sale_session_id=?", new String[]{sessionId});
            }
            Log.d(Constants.TAG, "Deleted " + sessions.length + " direct sale sessions");
            db.setTransactionSuccessful();
            db.endTransaction();
        }
    }

    /**
     * Inserts a direct sale order that has been completed
     * @param context context at the creation of the Sale order
     * @param sale the direct sale order to be inserted into the database
     * @return the generated UID of the direct sale order that has been inserted
     */
    public static String createDirectSaleOrder(Context context, DirectSale sale){
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String code = DatabaseHelper.getGUID();
        try(SQLiteDatabase db = helper.getWritableDatabase() ) {
            db.beginTransaction();
            ContentValues cv = new ContentValues();
            cv.put("direct_sale_id", code);
            cv.put("direct_sale_session_id", sale.getDirectSaleSessionId());
            cv.put("total_amount_due", sale.getTotalAmountDue());
            cv.put("amount_paid", sale.getAmountPaid());
            cv.put("status", sale.getStatus());
            cv.put("creation_dt", FormatUtils.today().getTime());
            db.insert("direct_sale", null, cv);
            db.setTransactionSuccessful();
            db.endTransaction();
            Log.d(Constants.TAG, "Order ID " + code + " created");
        }
        return code;
    }

    /**
     * Gets the list of all CLOSED direct sale sessions of the current user
     * @param context context at the moment the direct sale sessions are being retrieved
     * @return list of the direct sale sessions found in the database that fulfill the stated conditions
     */
    public static DirectSaleSession[] getDirectSaleSessions(Context context){
        List<DirectSaleSession> sessionList = new ArrayList<>();
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        // get the sale sessions of the user that are already closed
        String selectSql = "select direct_sale_session_id, sale_person_id, start_time, end_time, warehouse_id " +
                           "from direct_sale_session where sale_person_id=? and end_time is not null or end_time != ''";
        try(SQLiteDatabase db = helper.getReadableDatabase() ) {
            Cursor c = db.rawQuery(selectSql, new String[]{DatabaseHelper.getUserId(context)});
            if (c.moveToFirst()) {
                while ( !c.isAfterLast() ) {
                    String sessionId = c.getString(c.getColumnIndex("direct_sale_session_id"));
                    sessionList.add(new DirectSaleSession(
                            sessionId,
                            c.getString(c.getColumnIndex("sale_person_id")),
                            c.getString(c.getColumnIndex("start_time")),
                            c.getString(c.getColumnIndex("end_time")),
                            c.getString(c.getColumnIndex("warehouse_id")),
                            getDirectSales(db, sessionId)
                    ));
                    c.moveToNext();
                }
            }
            c.close();
        }

        Log.d(Constants.TAG, "Found " + sessionList.size() + " sessions");
        return sessionList.toArray(new DirectSaleSession[0]);
    }

    /**
     * Gets all of the direct sales made in the provided direct sale session
     * @param db the database where the direct sales will be from
     * @param sessionId the session id that the direct sales are part of
     * @return list of direct sales in the direct sale session
     */
    private static DirectSale[] getDirectSales(SQLiteDatabase db, String sessionId){
        List<DirectSale> salesList = new ArrayList<>();

        String selectSql = "select direct_sale_id, total_amount_due, amount_paid, status, creation_dt from direct_sale where direct_sale_session_id=?";
        Cursor c = db.rawQuery(selectSql, new String[]{sessionId});
        if (c.moveToFirst()) {
            while (!c.isAfterLast()) {
                String saleId = c.getString(c.getColumnIndex("direct_sale_id"));
                salesList.add(new DirectSale(
                        saleId,
                        sessionId,
                        c.getDouble(c.getColumnIndex("total_amount_due")),
                        c.getDouble(c.getColumnIndex("amount_paid")),
                        c.getString(c.getColumnIndex("status")),
                        c.getString(c.getColumnIndex("creation_dt")),
                        getDirectSaleOrderItems(db, saleId)
                ));
                c.moveToNext();
            }
        }
        c.close();
        return salesList.toArray(new DirectSale[0]);
    }

    /**
     * Gets all of the direct sale items for a given direct sale
     * @param db the database where the direct sale items will be from
     * @param saleId the direct sale where the items are from
     * @return the list of items of the passed direct sale
     */
    private static DirectSaleItem[] getDirectSaleOrderItems(SQLiteDatabase db, String saleId){
        List<DirectSaleItem> itemList = new ArrayList<>();

        String selectSql = "select direct_sale_item_id, direct_sale_id, prod_id, qty, " +
                "packaging_id, unit_price, sub_total " +
                "from direct_sale_item where direct_sale_id=?";

        Cursor c = db.rawQuery(selectSql, new String[]{saleId});
        if (c.moveToFirst()) {
            while (!c.isAfterLast()) {
                itemList.add(new DirectSaleItem(
                        c.getString(c.getColumnIndex("direct_sale_item_id")),
                        c.getString(c.getColumnIndex("direct_sale_id")),
                        c.getString(c.getColumnIndex("prod_id")),
                        c.getLong(c.getColumnIndex("qty")),
                        c.getString(c.getColumnIndex("packaging_id")),
                        c.getDouble(c.getColumnIndex("unit_price")),
                        c.getDouble(c.getColumnIndex("sub_total"))
                ));
                c.moveToNext();
            }
        }
        c.close();
        return itemList.toArray(new DirectSaleItem[0]);
    }

    /**
     * Creates a direct sale item for the direct sale
     * @param context the context at the moment of item addition
     * @param saleCode the id of the direct sale that will contains the item
     * @param productId the product of the item line
     * @param qty the number requested
     * @param price the price of the item
     * @param pck_id the packaging id of the item (for unit measurement)
     * @return the generated id of the inserted direct sale item
     */
    public static String addDirectSaleOrderItem(Context context, String saleCode, String productId, int qty, double price, String pck_id) {
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String sId= DatabaseHelper.getGUID();
        try(SQLiteDatabase db = helper.getWritableDatabase() ) {
            db.beginTransaction();
            double total = qty*price;
            ContentValues cv = new ContentValues();
            cv.put("direct_sale_item_id", sId);
            cv.put("direct_sale_id", saleCode);
            cv.put("prod_id", productId);
            cv.put("qty", qty);
            cv.put("unit_price", price);
            cv.put("sub_total", total);
            cv.put("packaging_id", pck_id);
            db.insert("direct_sale_item", null, cv);
            db.setTransactionSuccessful();
            db.endTransaction();
            Log.d(Constants.TAG, saleCode + ": " + productId + " - " + total);
        }
        return sId;
    }
}
