package com.synerteqsys.simpleerpmobilesales.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;

import java.util.Locale;

/**
 * Created by dev06 on 19/01/2018.
 */

public class LocaleUtils {
    public static void resetLocale(Context context){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        Locale locale = new Locale(sharedPref.getString("lang_pref", "US_en"));
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config, null);
    }
}
