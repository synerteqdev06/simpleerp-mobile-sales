package com.synerteqsys.simpleerpmobilesales.utils.ui;

/**
 * Created by ZhenKun on 12/29/2017.
 */

public interface DetectProcessor {
    public void invoke(String value, String source);
}
