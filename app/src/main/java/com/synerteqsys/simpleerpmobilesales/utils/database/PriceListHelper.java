package com.synerteqsys.simpleerpmobilesales.utils.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.synerteqsys.simpleerpmobilesales.data.UserData;
import com.synerteqsys.simpleerpmobilesales.data.models.PriceList;
import com.synerteqsys.simpleerpmobilesales.data.models.PriceListDet;
import com.synerteqsys.simpleerpmobilesales.data.models.PriceListOption;
import com.synerteqsys.simpleerpmobilesales.data.models.Product;
import com.synerteqsys.simpleerpmobilesales.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class PriceListHelper {
    public static Product findCustProdPrice(SQLiteDatabase db, Product p, String custId){
        // get the public price list of the customer
        String publicPriceList = "";
        String publicQ = "select price_list_id from customer where partner_id = ?";
        Cursor c = db.rawQuery(publicQ, new String[]{custId});
        if(c.moveToFirst())
            publicPriceList = c.getString(c.getColumnIndex("price_list_id"));

        // get the private price list
        String privatePriceList = "";
        String privateQ = "select price_list_id from price_list where customer_id = ?";
        c = db.rawQuery(privateQ, new String[]{custId});
        if(c.moveToFirst())
            privatePriceList = c.getString(c.getColumnIndex("price_list_id"));

        String selectPrice =
                "select pr.prod_name, coalesce(private.effective_price, public.effective_price, pr.srp) as price from product pr \n" +
                        "left join (select effective_price, prod_id from price_list_det pld1 where price_list_id = ?) private on private.prod_id = pr.prod_id\n" +
                        "left join (select effective_price, prod_id from price_list_det pld1 where price_list_id = ?) public on public.prod_id = pr.prod_id\n" +
                        "where pr.prod_id = ?";
        c = db.rawQuery(selectPrice, new String[]{privatePriceList, publicPriceList, p.getProdId()});
        if(c.moveToFirst())
            p.setSrp(c.getDouble(c.getColumnIndex("price")));

        c.close();

        return p;
    }

    public static Double findProductPrice(Context context, String prodId, String pricelistId){
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String selectSql = "select effective_price from price_list_det where prod_id=? and price_list_id=? and user_id=?";
        Double price = null;
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor c = db.rawQuery(selectSql, new String[]{prodId, pricelistId, DatabaseHelper.getUserId(context)});
        if (c.moveToFirst()) {
            price = c.getDouble(c.getColumnIndex("effective_price"));
            Log.d(Constants.TAG, prodId + " price found: " + price);
        }
        c.close();

        return price;
    }

    public static String findPriceListId(Context context, String priceList){
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String selectSql = "select price_list_id from price_list_option where price_list_name=? and user_id=?";
        String list = null;
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor c = db.rawQuery(selectSql, new String[]{priceList, DatabaseHelper.getUserId(context)});
        if (c.moveToFirst()) {
            list = c.getString(c.getColumnIndex("price_list_id"));
            Log.d(Constants.TAG, " pricelist found: " + list);
        }
        c.close();

        return list;
    }

    public static PriceListOption[] getPricelistOptions(Context context){
        List<PriceListOption> plistOptions = new ArrayList<>();
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String selectSql = "select price_list_id, price_list_name " +
                "from price_list_option where user_id=? order by price_list_name collate nocase";
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor c = db.rawQuery(selectSql, new String[]{DatabaseHelper.getUserId(context)});
        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                plistOptions.add(new PriceListOption(
                        c.getString(c.getColumnIndex("price_list_id")),
                        c.getString(c.getColumnIndex("price_list_name"))
                ));
                c.moveToNext();
            }
        }
        c.close();

        Log.d(Constants.TAG, "Found " + plistOptions.size() + " pricelists");
        return plistOptions.toArray(new PriceListOption[0]);

    }

    static void processPriceList(Context context, UserData data) {
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String userId = DatabaseHelper.getUserId(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.beginTransaction();
        for (PriceList priceList : data.getPricelist()) {
            db.delete("price_list","user_id=? and price_list_id=?", new String[]{userId, priceList.getPriceListId()});
            if(priceList.getStatus().equals(Constants.ACTIVE)) {
                ContentValues cv = new ContentValues();
                cv.put("price_list_id", priceList.getPriceListId());
                cv.put("customer_id", priceList.getCustomerId());
                cv.put("user_id", userId);
                db.insert("price_list", null, cv);
                Log.d(Constants.TAG, priceList.toString() + ": " + userId);
            } else {
                db.delete("price_list_det","user_id=? and price_list_id=?", new String[]{userId, priceList.getPriceListId()});
            }
        }
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    static void processPriceListOption(Context context, UserData data) {
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String userId = DatabaseHelper.getUserId(context);
        SQLiteDatabase db = helper.getWritableDatabase();
            db.beginTransaction();
            db.delete("price_list_option","user_id=?", new String[]{userId});
            for (PriceListOption priceList : data.getPricelistopt()) {
                ContentValues cv = new ContentValues();
                cv.put("price_list_id", priceList.getPriceListId());
                cv.put("price_list_name", priceList.getPriceListName());
                cv.put("user_id", userId);
                db.insert("price_list_option", null, cv);
                Log.d(Constants.TAG, priceList.toString() + ": " + userId);
            }
            db.setTransactionSuccessful();
            db.endTransaction();
    }

    static void processPriceListDet(Context context, UserData data) {
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String userId = DatabaseHelper.getUserId(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.beginTransaction();
        for (PriceListDet priceListDet : data.getPricelistdet()) {
            db.delete("price_list_det", "user_id=? and price_list_id=? and prod_id=?",
                    new String[]{userId,priceListDet.getPriceListId(),priceListDet.getProdId()});
            ContentValues cv = new ContentValues();
            cv.put("price_list_id", priceListDet.getPriceListId());
            cv.put("prod_id", priceListDet.getProdId());
            cv.put("effective_price", priceListDet.getEffectivePrice());
            cv.put("user_id", userId);
            db.insert("price_list_det", null, cv);
            Log.d(Constants.TAG, priceListDet.toString() + ": " + userId);
        }
        String deleteQ = "delete FROM price_list_det where price_list_id not in " +
            "(select price_list_id from price_list union all select price_list_id from price_list_option)";
        db.execSQL(deleteQ);

        db.setTransactionSuccessful();
        db.endTransaction();
    }
}
