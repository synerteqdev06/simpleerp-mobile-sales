package com.synerteqsys.simpleerpmobilesales.utils.database;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.util.Log;

import com.synerteqsys.simpleerpmobilesales.data.UserData;
import com.synerteqsys.simpleerpmobilesales.data.models.Product;
import com.synerteqsys.simpleerpmobilesales.utils.Constants;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ProductHelper {
    public static Product[] getProductsWithSerial(Context context){
        return getProducts(context, false, true);
    }

    public static Product[] getProductsWithPackaging(Context context){
        return getProducts(context, true, false);
    }

    public static Product[] getProductsNoPackaging(Context context){
        return getProducts(context, false, false);
    }

    private static Product[] getProducts(Context context, boolean usesPackaging, boolean getSerial){
        List<Product> productList = new ArrayList<>();
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String selectSql = "select prod_id, sku, barcode, base_unit, prod_name, packaging_id from product " +
                           "where packaging_id in (select min(packaging_id) from product group by prod_id) " +
                           "order by prod_name collate nocase";
        if(usesPackaging){
            selectSql = "select prod_id, sku, barcode, base_unit, prod_name, packaging_id from product "
                      + "order by prod_name collate nocase";
        }
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor c;
        if(usesPackaging)
            c = db.rawQuery(selectSql, new String[]{});
        else
            c = db.rawQuery(selectSql, new String[]{DatabaseHelper.getUserId(context)});

        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                Product p = new Product();
                p.setProdId(c.getString(c.getColumnIndex("prod_id")));
                p.setSku(c.getString(c.getColumnIndex("sku")));
                p.setBarcode(c.getString(c.getColumnIndex("barcode")));
                p.setProdName(c.getString(c.getColumnIndex("prod_name")));
                p.setBaseUnit(c.getString(c.getColumnIndex("base_unit")));
                p.setPackagingId(c.getString(c.getColumnIndex("packaging_id")));
                if(getSerial)
                    p.setSerial(SerialHelper.getSerial(context, p));
                productList.add(p);
                c.moveToNext();
            }
        }
        c.close();


        Log.d(Constants.TAG, "Found " + productList.size() + " products");
        return productList.toArray(new Product[0]);

    }

    public static Product findBaseProduct(Context context, String value) {
        DatabaseHelper helper = DatabaseHelper.getInstance(context);

        String selectSql = "select prod_id, sku, barcode, prod_name, base_unit, srp, packaging_id from product where " +
                "prod_id=? order by packaging_id asc";
        Log.d(Constants.TAG, "Product search: " + selectSql);

        Product p = null;
        try(SQLiteDatabase db = helper.getReadableDatabase() ) {
            Cursor c = db.rawQuery(selectSql, new String[]{value});
            if (c.moveToFirst()) {
                p = new Product();
                p.setProdId(c.getString(c.getColumnIndex("prod_id")));
                p.setSku(c.getString(c.getColumnIndex("sku")));
                p.setBarcode(c.getString(c.getColumnIndex("barcode")));
                p.setProdName(c.getString(c.getColumnIndex("prod_name")));
                p.setBaseUnit(c.getString(c.getColumnIndex("base_unit")));
                p.setSrp(c.getDouble(c.getColumnIndex("srp")));
                p.setPackagingId(c.getString(c.getColumnIndex("packaging_id")));
                Log.d(Constants.TAG, "Find Product: " + value);
            }
            c.close();
        }
        return p;
    }

    public static Product findProduct(Context context, String value) {
        return findProduct(context, value, null, false);
    }

    public static Product findProduct(Context context, String value, boolean isId) {
        return findProduct(context, value, null, isId);
    }

    public static Product findProduct(Context context, String value, String custId, boolean isId) {
        // first search the product normally
        Product p = findProductDetails(context, value, custId, isId);

        // Now, there is a special case wherein the reader won't be
        // able to read the first 0 in a barcode if the barcode starts
        // with a zero. We will perform a search with the 0 in the
        // start of our key if no product has been returned yet and
        // if the barcode is being used in the search.
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        HashSet<String> identifiers = new HashSet(sharedPref.getStringSet("scanner_pref", null));
        if(p == null && value.length()==12 && identifiers.contains("barcode")){
            p = findProductDetails(context, "0" + value, custId, isId);
        }

        return p;
    }

    private static Product findProductDetails(Context context, String value, String custId, boolean isId){
        DatabaseHelper helper = DatabaseHelper.getInstance(context);

        // first check if the user scans the item with the barcode or the SKU or both
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        Set<String> identifiers =  sharedPref.getStringSet("scanner_pref", null);
        List<String> values = new ArrayList<>();
        StringBuilder builder = new StringBuilder("");
        // search via ID if the boolean is passed
        if(isId){
            builder.append("packaging_id=? ");
            values.add(value);
        } else if(identifiers != null) {
            builder.append("(");
            for (String ident : identifiers) {
                if (builder.length() > 1)
                    builder.append(" or ");
                builder.append(ident);
                builder.append("=?");
                values.add(value);
            }
            builder.append(") ");
        }
        // now, select the product from the database
        String selectSql = "select prod_id, sku, barcode, prod_name, base_unit, srp, packaging_id from product where " +
                            builder.toString();
        Log.d(Constants.TAG, "Product search: " + selectSql);
        Product p = null;
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor c = db.rawQuery(selectSql, values.toArray(new String[0]));
        if (c.moveToFirst()) {
            p = new Product();
            p.setProdId(c.getString(c.getColumnIndex("prod_id")));
            p.setSku(c.getString(c.getColumnIndex("sku")));
            p.setBarcode(c.getString(c.getColumnIndex("barcode")));
            p.setProdName(c.getString(c.getColumnIndex("prod_name")));
            p.setBaseUnit(c.getString(c.getColumnIndex("base_unit")));
            p.setSrp(c.getDouble(c.getColumnIndex("srp")));
            p.setPackagingId(c.getString(c.getColumnIndex("packaging_id")));
            Log.d(Constants.TAG, "Find Product: " + value);
        }
        c.close();

        //the user passes a customer id, so we get the price from the price list of the customer
        if(p!=null && custId != null){
            p = PriceListHelper.findCustProdPrice(db, p, custId);
        }


        return p;
    }

    static void processProducts(Context context, UserData data) {
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.beginTransaction();
        for (Product product : data.getProduct()) {
            db.delete("product", " packaging_id=?", new String[]{product.getPackagingId()});
            if(product.getStatus().equals(Constants.ACTIVE)) {
                ContentValues cv = new ContentValues();
                cv.put("prod_id", product.getProdId());
                cv.put("sku", product.getSku());
                cv.put("barcode", product.getBarcode());
                cv.put("prod_name", product.getProdName());
                cv.put("base_unit", product.getBaseUnit());
                cv.put("packaging_id", product.getPackagingId());
                cv.put("srp", product.getSrp());
                db.insert("product", null, cv);
                Log.d(Constants.TAG, product.toString() );
            }
        }
        db.setTransactionSuccessful();
        db.endTransaction();
    }
}
