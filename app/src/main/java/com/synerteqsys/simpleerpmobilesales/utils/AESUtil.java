package com.synerteqsys.simpleerpmobilesales.utils;

import android.util.Base64;

import java.security.MessageDigest;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by dev06 on 02/01/2018.
 */

public class AESUtil {

    public static final String encryptAsBase64(String key, String value) throws Exception {
        byte[] raw = key.getBytes("iso-8859-1");
        MessageDigest digest = MessageDigest.getInstance("MD5");
        SecretKeySpec kspec = new SecretKeySpec(digest.digest(raw), "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, kspec, new IvParameterSpec(new byte[16]));
        byte[] encrypted = cipher.doFinal(value.getBytes("iso-8859-1"));
        return Base64.encodeToString(encrypted,Base64.NO_WRAP);
    }

    public static final String decryptFromBase64(String key, String value) throws Exception {
        byte[] raw = key.getBytes("iso-8859-1");
        MessageDigest digest = MessageDigest.getInstance("MD5");
        SecretKeySpec kspec = new SecretKeySpec(digest.digest(raw), "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, kspec, new IvParameterSpec(new byte[16]));
        byte[] data = Base64.decode(value,Base64.NO_WRAP);
        byte[] encrypted = cipher.doFinal(data);
        return new String(encrypted, "iso-8859-1");
    }
}
