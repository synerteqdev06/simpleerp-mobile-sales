package com.synerteqsys.simpleerpmobilesales.utils.database;

import android.content.Context;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.synerteqsys.simpleerpmobilesales.data.UserData;
import com.synerteqsys.simpleerpmobilesales.data.UserPreference;
import com.synerteqsys.simpleerpmobilesales.utils.Constants;

import java.util.UUID;

/**
 * Helper class for all database-related operations.
 *
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 11;
    private static final String DATABASE_NAME = "SIMPLEERP_DB";
    private static DatabaseHelper instance;


    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized DatabaseHelper getInstance(Context context){
        if(instance == null)
            instance = new DatabaseHelper(context);

        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_USERS_TABLE = "CREATE TABLE users(user_id TEXT PRIMARY KEY, username TEXT, email_address TEXT, password TEXT, salt TEXT, url TEXT, last_license_dt TEXT, token TEXT, last_download TEXT, functions TEXT)";
        db.execSQL(CREATE_USERS_TABLE);

        String CREATE_PRODUCT_TABLE = "CREATE TABLE product ( prod_id TEXT, packaging_id TEXT, sku TEXT, barcode TEXT, prod_name TEXT, base_unit TEXT, srp REAL, user_id TEXT)";
        db.execSQL(CREATE_PRODUCT_TABLE);

        String CREATE_DIRECT_SALE_SESS_TABLE = "CREATE TABLE direct_sale_session ( direct_sale_session_id TEXT PRIMARY KEY, sale_person_id TEXT NOT NULL, start_time TEXT, end_time TEXT, warehouse_id TEXT)";
        db.execSQL(CREATE_DIRECT_SALE_SESS_TABLE);

        String CREATE_DIRECT_SALE_TABLE = "CREATE TABLE direct_sale (direct_sale_id TEXT PRIMARY KEY, direct_sale_session_id TEXT NOT NULL, total_amount_due REAL, amount_paid REAL, status TEXT, creation_dt TEXT)";
        db.execSQL(CREATE_DIRECT_SALE_TABLE);

        String CREATE_DIRECT_SALE_ITEM = "CREATE TABLE direct_sale_item ( direct_sale_item_id TEXT PRIMARY KEY, direct_sale_id TEXT NOT NULL, prod_id TEXT NOT NULL, qty INTEGER, packaging_id TEXT, unit_price REAL NOT NULL, sub_total REAL NOT NULL)";
        db.execSQL(CREATE_DIRECT_SALE_ITEM);

        String CREATE_inventory_in= "create table inventory_in ( inventory_in_id TEXT PRIMARY KEY, status TEXT, remarks TEXT, creation_dt TEXT, warehouse_id TEXT, user_id TEXT)";
        db.execSQL(CREATE_inventory_in);

        String CREATE_inventory_in_det = "create table inventory_in_det ( inventory_in_det_id TEXT PRIMARY KEY, inventory_in_id TEXT NOT NULL, prod_id TEXT NOT NULL, qty INTEGER NOT NULL)";
        db.execSQL(CREATE_inventory_in_det);

        String CREATE_PRICELIST = "create table price_list ( price_list_id TEXT, customer_id TEXT, user_id TEXT)";
        db.execSQL(CREATE_PRICELIST);

        String CREATE_PRICELIST_OPTION = "create table price_list_option ( price_list_id TEXT, price_list_name TEXT, user_id TEXT)";
        db.execSQL(CREATE_PRICELIST_OPTION);

        String CREATE_PRICELIST_DET = "create table price_list_det ( price_list_id TEXT, prod_id TEXT, effective_price REAL, user_id TEXT)";
        db.execSQL(CREATE_PRICELIST_DET);

        String CREATE_CUSTOMER = "create table customer ( partner_id TEXT, partner_name TEXT, price_list_id TEXT, user_id TEXT)";
        db.execSQL(CREATE_CUSTOMER);

        String CREATE_CUSTOMER_WAREHOUSE = "create table cust_warehouse (cust_warehouse_id TEXT, warehouse_name TEXT, customer_id TEXT, user_id TEXT)";
        db.execSQL(CREATE_CUSTOMER_WAREHOUSE);

        String CREATE_SALE_ORDER = "create table sale_order( sale_order_id TEXT PRIMARY KEY, customer_id TEXT NOT NULL, creation_dt TEXT, amount_due REAL, remarks TEXT, warehouse_id TEXT, cust_warehouse_id TEXT, user_id TEXT)";
        db.execSQL(CREATE_SALE_ORDER);

        String CREATE_SALE_ORDER_LINE = "create table sale_order_line( sale_order_line_id TEXT PRIMARY KEY, sale_order_id TEXT NOT NULL, prod_id TEXT, qty INTEGER, price REAL, sub_total REAL)";
        db.execSQL(CREATE_SALE_ORDER_LINE);

        String CREATE_DELIVERY = "create table delivery( delivery_id TEXT NOT NULL, delivery_no TEXT NOT NULL, delivery_type TEXT, is_verified TEXT, for_upload TEXT, user_id TEXT)";
        db.execSQL(CREATE_DELIVERY);

        String CREATE_DELIVERY_DET = "create table delivery_det( delivery_det_id TEXT NOT NULL, delivery_id TEXT NOT NULL, prod_id TEXT, qty_approved_total INTEGER, qty_done_total INTEGER, user_id TEXT)";
        db.execSQL(CREATE_DELIVERY_DET);

        String CREATE_SERIAL = "create table serial ( serial_no TEXT, prod_id TEXT)";
        db.execSQL(CREATE_SERIAL);

        String CREATE_BATCH = "create table inventory_batch ( inventory_batch_no TEXT, prod_id TEXT)";
        db.execSQL(CREATE_BATCH);

        String CREATE_LOG = "create table log ( log_id INTEGER PRIMARY KEY, log_info TEXT, log_time TEXT, user_id TEXT)";
        db.execSQL(CREATE_LOG);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String DROP_USERS_TABLE = "DROP TABLE IF EXISTS users";
        db.execSQL(DROP_USERS_TABLE);

        String DROP_PRODUCT_TABLE = "DROP TABLE IF EXISTS product";
        db.execSQL(DROP_PRODUCT_TABLE);

        String DROP_DIRECT_SALE_SESSION = "DROP TABLE IF EXISTS direct_sale_session";
        db.execSQL(DROP_DIRECT_SALE_SESSION);

        String DROP_DIRECT_SALE_TABLE = "DROP TABLE IF EXISTS direct_sale";
        db.execSQL(DROP_DIRECT_SALE_TABLE);

        String DROP_DIRECT_SALE_ITEM_TABLE = "DROP TABLE IF EXISTS direct_sale_item";
        db.execSQL(DROP_DIRECT_SALE_ITEM_TABLE);

        String DROP_PRICE_LIST = "DROP TABLE IF EXISTS price_list";
        db.execSQL(DROP_PRICE_LIST);

        String DROP_PRICE_LIST_OPT = "DROP TABLE IF EXISTS price_list_option";
        db.execSQL(DROP_PRICE_LIST_OPT);

        String DROP_PRICE_LIST_DET = "DROP TABLE IF EXISTS price_list_det";
        db.execSQL(DROP_PRICE_LIST_DET);

        String DROP_CUSTOMER = "DROP TABLE IF EXISTS customer";
        db.execSQL(DROP_CUSTOMER);

        String DROP_CUSTOMER_WH = "DROP TABLE IF EXISTS cust_warehouse";
        db.execSQL(DROP_CUSTOMER_WH);

        String DROP_SALE_ORDER = "DROP TABLE IF EXISTS sale_order";
        db.execSQL(DROP_SALE_ORDER);

        String DROP_SALE_ORDER_LINE = "DROP TABLE IF EXISTS sale_order_line";
        db.execSQL(DROP_SALE_ORDER_LINE);

        String DROP_INV_IN = "DROP TABLE IF EXISTS inventory_in";
        db.execSQL(DROP_INV_IN);

        String DROP_INV_IN_DET= "DROP TABLE IF EXISTS inventory_in_det";
        db.execSQL(DROP_INV_IN_DET);

        String DROP_DELIVERY = "DROP TABLE IF EXISTS delivery";
        db.execSQL(DROP_DELIVERY);

        String DROP_DELIVERY_DET = "DROP TABLE IF EXISTS delivery_det";
        db.execSQL(DROP_DELIVERY_DET);

        String DROP_SERIAL = "DROP TABLE IF EXISTS serial";
        db.execSQL(DROP_SERIAL);

        String DROP_BATCH = "DROP TABLE IF EXISTS inventory_batch";
        db.execSQL(DROP_BATCH);

        String DROP_LOG = "DROP TABLE IF EXISTS log";
        db.execSQL(DROP_LOG);

        onCreate(db);
    }

    public static void processData(Context context, UserData data){
        if(data.getProduct() != null)
            ProductHelper.processProducts(context, data);
        if(data.getSerial() != null)
            SerialHelper.processSerial(context, data);
        if(data.getCustomer() != null)
            CustomerHelper.processCustomers(context, data);
        if(data.getCustWarehouse() != null)
            CustomerHelper.processCustWarehouse(context, data);
        if(data.getPricelist() != null)
            PriceListHelper.processPriceList(context, data);
        if(data.getPricelistopt() != null)
            PriceListHelper.processPriceListOption(context, data);
        if(data.getPricelistdet() != null)
            PriceListHelper.processPriceListDet(context, data);
        if(data.getDelivery() != null)
            DeliveryHelper.processDelivery(context, data);
        if(data.getDeliverydet() != null)
            DeliveryHelper.processDeliveryDet(context, data);
        if(data.getInventoryBatch() != null)
            InvBatchHelper.processInvBatch(context, data);
    }

    public static void clearDatabase(Context context){
        DatabaseHelper helper = getInstance(context);
        SQLiteDatabase db = helper.getReadableDatabase();
        db.beginTransaction();
        db.delete("product", null, null);
        db.delete("serial", null, null);
        db.delete("inventory_batch", null, null);
        db.delete("customer", null, null);
        db.delete("cust_warehouse", null, null);
        db.delete("price_list", null, null);
        db.delete("price_list_option", null, null);
        db.delete("price_list_det", null, null);
        db.delete("delivery", null, null);
        db.delete("delivery_det", null, null);
        Log.d(Constants.TAG, "Cleared out database");
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public static void clearWholeDatabase(Context context){
        DatabaseHelper helper = getInstance(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.beginTransaction();
        db.delete("product", null, null);
        db.delete("serial", null, null);
        db.delete("inventory_batch", null, null);
        db.delete("customer", null, null);
        db.delete("cust_warehouse", null, null);
        db.delete("price_list", null, null);
        db.delete("price_list_option", null, null);
        db.delete("price_list_det", null, null);
        db.delete("delivery", null, null);
        db.delete("delivery_det", null, null);
        db.delete("direct_sale_session", null, null);
        db.delete("direct_sale", null, null);
        db.delete("direct_sale_item", null, null);
        db.delete("inventory_in", null, null);
        db.delete("inventory_in_det", null, null);
        db.delete("sale_order", null, null);
        db.delete("sale_order_line", null, null);
        Log.d(Constants.TAG, "Cleared out database");
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public static long numOfRows(Context context, String table){
        return numOfRows(context, table, "user_id=?", new String[] {getUserId(context)});
    }

    public static long numOfRows(Context context, String table, String condition, String[] params){
        long rowCnt;

        DatabaseHelper helper = getInstance(context);
        SQLiteDatabase db = helper.getReadableDatabase();
        rowCnt = DatabaseUtils.queryNumEntries(db, table, condition, params);

        return rowCnt;
    }

    static String getUserId(Context context){
        return UserPreference.getStringPreference(context, Constants.USER_ID);
    }

    static String getWarehouse(Context context){
        return UserPreference.getWarehouse(context);
    }

    static String getGUID(){
        return UUID.randomUUID().toString();
    }

}
