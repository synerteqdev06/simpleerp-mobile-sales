package com.synerteqsys.simpleerpmobilesales.utils.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.synerteqsys.simpleerpmobilesales.data.UserData;
import com.synerteqsys.simpleerpmobilesales.data.models.InventoryBatch;
import com.synerteqsys.simpleerpmobilesales.data.models.Product;
import com.synerteqsys.simpleerpmobilesales.data.models.Serial;
import com.synerteqsys.simpleerpmobilesales.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class InvBatchHelper {
    static void processInvBatch(Context context, UserData data) {
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.beginTransaction();
        for (InventoryBatch batch : data.getInventoryBatch()) {
            db.delete("inventory_batch", "inventory_batch_no=?", new String[]{batch.getInventoryBatchNo()});
            if(batch.getQtyRemaining() > 0) {
                ContentValues cv = new ContentValues();
                cv.put("inventory_batch_no", batch.getInventoryBatchNo());
                cv.put("prod_id", batch.getProdId());
                db.insert("inventory_batch", null, cv);
                Log.d(Constants.TAG, batch.toString());
            }
        }
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public static String[] getBatch(Context context, Product p){
        List<String> batchList = new ArrayList<>();
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String selectSql = "select inventory_batch_no " +
                "from inventory_batch where prod_id=?";
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor c = db.rawQuery(selectSql, new String[]{ p.getProdId()});
        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                batchList.add(c.getString(c.getColumnIndex("inventory_batch_no")));
                c.moveToNext();
            }
        }
        c.close();

        Log.d(Constants.TAG, "Found " + batchList.size() + " batches");
        return batchList.toArray(new String[0]);
    }
}
