package com.synerteqsys.simpleerpmobilesales.utils.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.synerteqsys.simpleerpmobilesales.data.models.User;
import com.synerteqsys.simpleerpmobilesales.utils.Constants;
import com.synerteqsys.simpleerpmobilesales.utils.FormatUtils;

public class UserHelper {
    /**
     * Creates a new user given the passed values
     * @param context context at time of creation
     * @param user The new user to be created
     */
    public static void createUser(Context context, User user){
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.beginTransaction();
        ContentValues cv = new ContentValues();
        cv.put("user_id", user.getUserId());
        cv.put("username", user.getUserName());
        cv.put("email_address", user.getEmail());
        cv.put("password", user.getPassword());
        cv.put("salt", user.getPasswordSalt());
        cv.put("url", user.getServerUrl());
        cv.put("last_license_dt", FormatUtils.today().getTime());
        cv.put("functions", user.getFunctions());
        db.insert("users", null, cv);
        db.setTransactionSuccessful();
        db.endTransaction();
        Log.d(Constants.TAG, "create: " + user.getUserName());
    }

    public static User searchUser(Context context, String value){
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String selectSql = "select user_id, username, email_address, password, salt, url, token, last_license_dt, functions from users where username=? COLLATE NOCASE or email_address=? COLLATE NOCASE";
        User u = null;
        SQLiteDatabase db = helper.getWritableDatabase();
        Cursor c = db.rawQuery(selectSql, new String[]{value, value});
        if (c.moveToFirst()) {
            u = new User();
            u.setUserId(c.getString(c.getColumnIndex("user_id")));
            u.setUserName(c.getString(c.getColumnIndex("username")));
            u.setEmail(c.getString(c.getColumnIndex("email_address")));
            u.setPassword(c.getString(c.getColumnIndex("password")));
            u.setPasswordSalt(c.getString(c.getColumnIndex("salt")));
            u.setServerUrl(c.getString(c.getColumnIndex("url")));
            u.setToken(c.getString(c.getColumnIndex("token")));
            u.setLastLicenseDt(c.getString(c.getColumnIndex("last_license_dt")));
            u.setFunctions(c.getString(c.getColumnIndex("functions")));
            Log.d(Constants.TAG, "search user: " + value);
        }
        c.close();
        return u;
    }

    public static void updateUserToken(Context context, String username, String token){
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        SQLiteDatabase db = helper.getWritableDatabase();
            db.beginTransaction();
            ContentValues cv = new ContentValues();
            cv.put("token", token);
            db.update("users", cv, "username=?", new String[]{username});
            db.setTransactionSuccessful();
            db.endTransaction();
    }

    public static void updateLastDlTime(Context context, String dlTime){
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.beginTransaction();
        ContentValues cv = new ContentValues();
        cv.put("last_download", dlTime);
        db.update("users", cv, "user_id=?", new String[]{DatabaseHelper.getUserId(context)});
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public static String getLastDlTime(Context context){
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String selectSql = "select last_download from users where user_id=?";
        String lastDl = null;
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor c = db.rawQuery(selectSql, new String[]{DatabaseHelper.getUserId(context)});
        if (c.moveToFirst()) {
            lastDl = c.getString(c.getColumnIndex("last_download"));
        }
        c.close();
        return lastDl;
    }

    public static String getUserFunctions(Context context){
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String selectSql = "select functions from users where user_id=?";
        String functions = null;
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor c = db.rawQuery(selectSql, new String[]{DatabaseHelper.getUserId(context)});
        if (c.moveToFirst()) {
            functions = c.getString(c.getColumnIndex("functions"));
        }
        c.close();
        return functions;
    }

    public static void updateUserLicense(Context context, String username){
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.beginTransaction();
        ContentValues cv = new ContentValues();
        cv.put("last_license_dt", FormatUtils.today().getTime());
        db.update("users", cv, "username=?", new String[]{username});
        db.setTransactionSuccessful();
        db.endTransaction();
        Log.d(Constants.TAG, "Renewed license of user " + username);
    }
}
