package com.synerteqsys.simpleerpmobilesales.utils.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.synerteqsys.simpleerpmobilesales.data.models.SaleOrder;
import com.synerteqsys.simpleerpmobilesales.data.models.SaleOrderLine;
import com.synerteqsys.simpleerpmobilesales.utils.Constants;
import com.synerteqsys.simpleerpmobilesales.utils.FormatUtils;

import java.util.ArrayList;
import java.util.List;

public class SaleOrderHelper {
    public static String createSaleOrder(Context context, String custId, String custWh){
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String code = DatabaseHelper.getGUID();
        try(SQLiteDatabase db = helper.getWritableDatabase() ) {
            db.beginTransaction();
            ContentValues cv = new ContentValues();
            cv.put("sale_order_id", code);
            cv.put("customer_id", custId);
            cv.put("amount_due", 0);
            cv.put("creation_dt", FormatUtils.today().getTime());
            cv.put("user_id", DatabaseHelper.getUserId(context));
            cv.put("warehouse_id", DatabaseHelper.getWarehouse(context));
            cv.put("cust_warehouse_id", custWh);
            db.insert("sale_order", null, cv);
            db.setTransactionSuccessful();
            db.endTransaction();
            Log.d(Constants.TAG, "Order ID " + code + " created");
        }
        return code;

    }

    public static String addSaleOrderLine(Context context, String saleCode, String productId, int qty, double price) {
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String lineId = DatabaseHelper.getGUID();
        try(SQLiteDatabase db = helper.getWritableDatabase() ) {
            db.beginTransaction();
            double total = qty*price;
            ContentValues cv = new ContentValues();
            cv.put("sale_order_line_id", lineId);
            cv.put("sale_order_id", saleCode);
            cv.put("prod_id", productId);
            cv.put("qty", qty);
            cv.put("price", price);
            cv.put("sub_total", total);
            db.insert("sale_order_line", null, cv);

            String UPDATE_TOTAL_COST = "update sale_order set amount_due=amount_due+? where sale_order_id=?";
            db.execSQL(UPDATE_TOTAL_COST,new Object[]{total, saleCode});
            db.setTransactionSuccessful();
            db.endTransaction();
            Log.d(Constants.TAG, saleCode + ": " + productId + " - " + total);
        }
        return lineId;
    }

    public static void addRemarksToSale(Context context, String saleId, String remarks){
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String code = DatabaseHelper.getGUID();
        try(SQLiteDatabase db = helper.getWritableDatabase() ) {
            db.beginTransaction();
            ContentValues cv = new ContentValues();
            cv.put("remarks", remarks);
            db.update("sale_order", cv, "sale_order_id=?", new String[]{saleId});
            db.setTransactionSuccessful();
            db.endTransaction();
            Log.d(Constants.TAG, "Added remarks to sale order " + code);
        }
    }

    public static SaleOrder[] getSaleOrders(Context context){
        List<SaleOrder> salesList = new ArrayList<>();
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        // get the purchases of the user that hasn't been synced yet
        String selectSql = "select sale_order_id, customer_id, creation_dt, amount_due, remarks, warehouse_id, cust_warehouse_id  " +
                "from sale_order where user_id=?";
        try(SQLiteDatabase db = helper.getReadableDatabase() ) {
            Cursor c = db.rawQuery(selectSql, new String[]{DatabaseHelper.getUserId(context)});
            if (c.moveToFirst()) {
                while ( !c.isAfterLast() ) {
                    String orderId = c.getString(c.getColumnIndex("sale_order_id"));
                    salesList.add(new SaleOrder(
                            orderId,
                            c.getString(c.getColumnIndex("customer_id")),
                            c.getString(c.getColumnIndex("creation_dt")),
                            c.getDouble(c.getColumnIndex("amount_due")),
                            c.getString(c.getColumnIndex("remarks")),
                            c.getString(c.getColumnIndex("warehouse_id")),
                            c.getString(c.getColumnIndex("cust_warehouse_id")),
                            getSaleOrderItems(db, orderId)
                    ));
                    c.moveToNext();
                }
            }
            c.close();
        }

        Log.d(Constants.TAG, "Found " + salesList.size() + " sale orders");
        return salesList.toArray(new SaleOrder[0]);
    }

    private static SaleOrderLine[] getSaleOrderItems(SQLiteDatabase db, String orderId){
        List<SaleOrderLine> itemList = new ArrayList<>();

        String selectSql = "select sale_order_line_id, sale_order_id, prod_id, qty, price, sub_total " +
                "from sale_order_line where sale_order_id=?";

        Cursor c = db.rawQuery(selectSql, new String[]{orderId});
        if (c.moveToFirst()) {
            while (!c.isAfterLast()) {
                itemList.add(new SaleOrderLine(
                        c.getString(c.getColumnIndex("sale_order_line_id")),
                        c.getString(c.getColumnIndex("sale_order_id")),
                        c.getString(c.getColumnIndex("prod_id")),
                        c.getLong(c.getColumnIndex("qty")),
                        c.getDouble(c.getColumnIndex("price")),
                        c.getDouble(c.getColumnIndex("sub_total"))
                ));
                c.moveToNext();
            }
        }
        c.close();

        return itemList.toArray(new SaleOrderLine[0]);
    }

    public static void deleteSaleOrders(Context context, String[] orders){
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        try(SQLiteDatabase db = helper.getWritableDatabase() ) {
            db.beginTransaction();
            for(String orderId : orders) {
                db.delete("sale_order_line", "sale_order_id=?", new String[]{orderId});
                db.delete("sale_order", "sale_order_id=?", new String[]{orderId});
            }
            Log.d(Constants.TAG, "Deleted " + orders.length + " sale orders");
            db.setTransactionSuccessful();
            db.endTransaction();
        }
    }
}
