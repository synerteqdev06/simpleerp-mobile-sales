package com.synerteqsys.simpleerpmobilesales.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.synerteqsys.simpleerpmobilesales.R;
import com.synerteqsys.simpleerpmobilesales.data.Response;
import com.synerteqsys.simpleerpmobilesales.data.UserPreference;
import com.synerteqsys.simpleerpmobilesales.data.models.User;
import com.synerteqsys.simpleerpmobilesales.utils.database.UserHelper;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by dev06 on 05/01/2018.
 * First submits the user's data to the auth server (if the server is up)
 * and then logs in the user to their designated server.
 */

public class LoginTask extends AsyncHttpGetTask {
    private String csrfToken;
    private String username;
    private String password;

    private User user;

    public void setCsrfToken(String csrfToken) {
        this.csrfToken = csrfToken;
    }

    public String getCsrfToken() {
        return csrfToken;
    }

    public LoginTask(Context context, String username, String password) {
        super(context, null);
        this.username = username;
        this.password = password;
    }

    @Override
    protected Boolean doInBackground(String... params) {
        final Context appContext = getContext().getApplicationContext();
        user = UserHelper.searchUser(appContext, username);
        if (user!=null) {
            Log.d("user obtained without auth",user.getServerUrl());
        }
        if (!NetworkUtils.hasNetwork(getContext())){
            if(user!=null){
                Log.d(Constants.TAG, "Logging in user " + user.getUserName() + " daaocally");

                UserPreference.setStringPreference(appContext, Constants.MODE, Constants.OFFLINE);
                return true;
            } else {
                setErrorMessage(appContext.getString(R.string.user_not_found));
                return false;
            }
        } else {
            Map<String, Object> data = new LinkedHashMap<>();
            ObjectMapper mapper = new ObjectMapper();
            Log.d(Constants.TAG, "Authenticating user: " + username);
            String deviceId = getDeviceId(appContext);
            data.put("userName", username);
            data.put("password", password);
            data.put("deviceId", deviceId);
            try {
                // false returned when authenticating
                if (!sendDataToAuth(Constants.AUTH_SERVER, mapper.writeValueAsString(data), user))
                    return false;
            } catch (Exception e) {
                Log.e(Constants.TAG, "error", e);
                e.printStackTrace();
                return false;
            }
            user = UserHelper.searchUser(appContext, username);

            if (user == null) // auth server didn't validate user
                return false;
            else if (UserPreference.getStringPreference(appContext, Constants.MODE).equals(Constants.OFFLINE)) { // auth server is unreachable
                Log.d(Constants.TAG, "Server is unreachable");
                return true;
            } else {
                Log.d("url", user.getServerUrl());
                return logInMain(user.getServerUrl());
            }
        }
    }

    private boolean logInLicense(String postData, String... params){
        if(NetworkUtils.hasNetwork(getContext())) {
            try {
                Log.d(Constants.TAG, "url " + params[0]);
                URL url = new URL(params[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setConnectTimeout(10000);
                    urlConnection.setRequestProperty("Content-Type", params[params.length - 1]);
                    urlConnection.setDoOutput(true);
                    urlConnection.setDoInput(true);
                    urlConnection.setChunkedStreamingMode(0);
                    urlConnection.connect();
                    OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
                    try {
                        Writer writer = new OutputStreamWriter(out, "UTF-8");
                        try {
                            writer.write(postData);
                        } finally {
                            writer.close();
                        }
                    } finally {
                        out.close();
                    }
                    if (urlConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                        int code = urlConnection.getResponseCode();
                        Log.d(Constants.TAG, "Response code: " + code);
                        switch(code) {
                            case 401: setErrorMessage(getContext().getString(R.string.invalid_username_password)); break;
                            case 412: setErrorMessage(getContext().getString(R.string.error_unlicensed)); break;
                            case 500: setErrorMessage(getContext().getString(R.string.error_login)); break;
                        }
                        return false;
                    }
                    UserPreference.setStringPreference(getContext().getApplicationContext(), Constants.MODE, Constants.ONLINE);
                    return downloadData(urlConnection);
                } finally {
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                Log.w(Constants.TAG, "unable to connect to server\n", e);
                return false;
            }
        }
        // no internet
        Log.w(Constants.TAG, "No internet connection\n");
        return false;
    }

    private Boolean logInMain(String... params){
        Context c = getContext();
        if(NetworkUtils.hasNetwork(c)) try {
            Log.d(Constants.TAG, "url: " + params[0]);
            URL url = new URL(params[0] + Constants.LOGIN_PAGE);
            if(serverIsOnline(url)) {
                //create the cookie for the user
                CookieHandler.setDefault(new CookieManager());
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    urlConnection.setRequestMethod("GET");
                    urlConnection.setConnectTimeout(10000);
                    urlConnection.connect();
                    if (urlConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                        Log.w(Constants.TAG, "Response code: " + urlConnection.getResponseCode());
                        return false;
                    }
                    getCsrfToken(urlConnection);
                } finally {
                    urlConnection.disconnect();
                }

                UserPreference.setStringPreference(c.getApplicationContext(), Constants.MODE, Constants.ONLINE);
                return accessServer(params);
            } else { // log in locally instead
                Log.d(Constants.TAG, "Server not available. Logging in locally...");
                UserPreference.setStringPreference(c.getApplicationContext(), Constants.MODE, Constants.OFFLINE);
                return true;
            }
        } catch (Exception e) {
            Log.w(Constants.TAG, "unable to connect to server\n");
            e.printStackTrace();
            return false;
        }

        Log.w(Constants.TAG, "unable to connect to server\n");
        return false;

    }

    private boolean accessServer(String... params) throws Exception{
        this.setParser( new ResponseParser(){
            @Override
            public void parse(Context context, String response) throws IOException {
                ObjectMapper mapper = new ObjectMapper();
                Response resp = mapper.readValue(response, Response.class);
                if(resp.getStatus() != 1){
                    UserPreference.setBoolPreference(context, Constants.LOGGED_IN, false);
                } else {
                    UserPreference.setBoolPreference(context, Constants.LOGGED_IN, true);
                    Map attributes = resp.getAttributes();
                    Map data = (Map) attributes.get("data");
                    String rmToken = (String) data.get("rmtoken");
                    UserHelper.updateUserToken(getContext().getApplicationContext(), user.getUserName(), rmToken);
                }
            }
        });

        URL loginUrl = new URL(params[0] + Constants.LOGIN_URL);
        Log.d(Constants.TAG, "url: " + loginUrl);
        HttpURLConnection loginConnection = (HttpURLConnection) loginUrl.openConnection();
        try{
            String postData = setUpPostData(username, password);
            loginConnection.setRequestMethod("POST");
            loginConnection.setConnectTimeout(10000);
            loginConnection.setRequestProperty("Content-Type", Constants.SERVER_REQUEST_TYPE);
            loginConnection.setDoOutput(true);
            loginConnection.setDoInput(true);
            loginConnection.setChunkedStreamingMode(0);
            loginConnection.connect();
            OutputStream out = new BufferedOutputStream(loginConnection.getOutputStream());
            try{
                Writer writer = new OutputStreamWriter(out, "UTF-8");
                try{
                    writer.write(postData);
                } finally {
                    writer.close();
                }
            } finally {
                out.close();
            }
            if(loginConnection.getResponseCode() != HttpURLConnection.HTTP_OK){
                Log.w(Constants.TAG, "Response code: " + loginConnection.getResponseCode());
            }
            return downloadData(loginConnection);
        } finally{
            loginConnection.disconnect();
        }
    }

    private void getCsrfToken(HttpURLConnection urlConnection) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            // download the file
            InputStream input = urlConnection.getInputStream();
            try {
                byte data[] = new byte[4096];
                int count;
                while ((count = input.read(data)) != -1) {
                    baos.write(data, 0, count);
                    // allow canceling with back button
                    if (isCancelled()) {
                        input.close();
                    }
                }
                String response = new String(baos.toByteArray(), "utf-8");
                Document doc = Jsoup.parse(response);
                Element csrfInput = doc.getElementsByTag("input").first();
                setCsrfToken(csrfInput.val());

                Context c = getContext().getApplicationContext();
                UserPreference.setStringPreference(c, Constants.CSRF_TOKEN, csrfInput.val());

                Log.d(Constants.TAG, "csrf: " + csrfInput.val());
            } finally {
                input.close();
            }
        } finally {
            baos.close();
        }
    }

    private String setUpPostData(String username, String password){
        Map<String, Object> data = new LinkedHashMap<String, Object>();
        data.put("ACTION", Constants.ACT_LOGIN);
        data.put("rememberMe", "1");
        data.put("userName", username);
        data.put("password", password);
        data.put("CSRF_TOKEN", csrfToken);

        String loginData = null;
        try {
            loginData = FormatUtils.formatFormData(data);
        } catch (UnsupportedEncodingException e) {
            Log.e(Constants.TAG, "Error formatting data " + e);
            e.printStackTrace();
        }
        return loginData;
    }

    /** send the log-in credentials of the user to the license server to get the server
        url and to check if the device is still valid **/
    private boolean sendDataToAuth(String url, String s, final User user){
        final Context appContext = getContext().getApplicationContext();
        try {
            URL authUrl = new URL(Constants.AUTH_SERVER);
            if(serverIsOnline(authUrl)) {
                setParser(new ResponseParser() {
                    @Override
                    public void parse(Context context, String response) throws IOException {
                        ObjectMapper mapper = new ObjectMapper();
                        User serverUser = mapper.readValue(response, User.class);
                        if (user != null)
                            UserHelper.updateUserLicense(appContext, user.getUserName());
                        else
                            UserHelper.createUser(appContext, serverUser);
                    }
                });

                return logInLicense(s, url, "application/json");

            } else {
                Log.d(Constants.TAG, "No Auth Server. Logging in locally.");
                if(user == null || !user.isLisenced()){
                    setErrorMessage(appContext.getString(R.string.user_not_found));
                    return false;
                }
                else {
                    if (user.getServerUrl()!=null){
                        return logInMain(user.getServerUrl());
                    } else {
                        UserPreference.setStringPreference(getContext().getApplicationContext(), Constants.MODE, Constants.OFFLINE);
                        return true;
                    }
                }
            }
        } catch(Exception e){
            Log.w(Constants.TAG, "Error parsing Auth URL");
            e.printStackTrace();
            return false;
        }
    }

    public synchronized static String getDeviceId(Context context) {
        final String PREF_UNIQUE_ID = "PREF_UNIQUE_ID";
        String uniqueID;
        SharedPreferences sharedPrefs = context.getSharedPreferences(
                PREF_UNIQUE_ID, Context.MODE_PRIVATE);
        uniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null);
        if (uniqueID == null) {
            uniqueID = UUID.randomUUID().toString();
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.putString(PREF_UNIQUE_ID, uniqueID);
            editor.apply();
        }
        return uniqueID;
    }
}
