package com.synerteqsys.simpleerpmobilesales.utils.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.synerteqsys.simpleerpmobilesales.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by ZhenKun on 1/3/2018.
 */

public class ItemAdapter extends ArrayAdapter<com.synerteqsys.simpleerpmobilesales.data.ScannedItem> {

    private ArrayList<com.synerteqsys.simpleerpmobilesales.data.ScannedItem> dataSet;
    Context mContext;
    private int layout;
    public static DecimalFormat moneyFormat=new DecimalFormat("₱#,##0.00");
    public static DecimalFormat intFormat=new DecimalFormat("#,##0");

    private static class ViewHolder{
        TextView txtId;
        TextView txtQty;
        TextView txtUnit;
        TextView txtDesc;
        TextView txtPrice;
        TextView txtSubTotal;
    }

    public ItemAdapter(ArrayList<com.synerteqsys.simpleerpmobilesales.data.ScannedItem> items, int layout, Context context){
        super(context, layout, items);
        this.layout = layout;
        this.dataSet =items;
        this.mContext=context;
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        com.synerteqsys.simpleerpmobilesales.data.ScannedItem item = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(layout, parent, false);
            viewHolder.txtQty = (TextView) convertView.findViewById(R.id.qty);
            viewHolder.txtUnit = (TextView) convertView.findViewById(R.id.unit);
            viewHolder.txtDesc = (TextView) convertView.findViewById(R.id.desc);
            viewHolder.txtPrice= (TextView) convertView.findViewById(R.id.price);
            viewHolder.txtSubTotal= (TextView) convertView.findViewById(R.id.subtotal);
            viewHolder.txtId=(TextView)convertView.findViewById(R.id.id);
            result=convertView;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        lastPosition = position;

        viewHolder.txtQty.setText( intFormat.format(item.getQty()));
        viewHolder.txtUnit.setText(item.getUnit());
        viewHolder.txtDesc.setText(item.getDesc());
        viewHolder.txtPrice.setText(moneyFormat.format(item.getPrice()));
        viewHolder.txtSubTotal.setText(moneyFormat.format(item.getSubtotal()));
        // if the search is the id, we don't print the id
        viewHolder.txtId.setText(item.getDisplayId());

        // Return the completed view to render on screen
        return convertView;
    }
}
