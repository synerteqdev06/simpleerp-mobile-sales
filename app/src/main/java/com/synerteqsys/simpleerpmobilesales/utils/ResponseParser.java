package com.synerteqsys.simpleerpmobilesales.utils;

import android.content.Context;

import java.io.IOException;

/**
 * Created by dev06 on 03/01/2018.
 */
public interface ResponseParser {
    void parse(Context context, String response) throws IOException;
}

