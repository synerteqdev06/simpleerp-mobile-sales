package com.synerteqsys.simpleerpmobilesales.utils.ui;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.util.Log;
import android.util.SparseArray;

import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.Frame;

import java.io.ByteArrayOutputStream;

/**
 * Created by ZhenKun on 1/2/2018.
 */


public class BoxDetector extends Detector {
    private Detector mDelegate;

    private int mBoxWidth, mBoxHeight;


    public BoxDetector(Detector delegate, int boxWidth, int boxHeight) {
        mDelegate = delegate;
        mBoxWidth = boxWidth;
        mBoxHeight = boxHeight;
    }

    @Override
    public SparseArray detect(Frame frame) {
        return mDelegate.detect(frame);
    }

    @Override
    public void receiveFrame(Frame frame) {
        Frame.Metadata meta= frame.getMetadata();
        int width = meta.getWidth();
        int height = meta.getHeight();
        int right = (width / 2) + (mBoxHeight);
        int left = (width / 2) - (mBoxHeight);
        int bottom = (height / 2) + (mBoxWidth);
        int top = (height / 2) - (mBoxWidth);

        Log.d("detector bonds", "w:" + width + ", h:" + height + ", r:" + right + ", l:" + left + ", b:" + bottom + ", t:" + top);
        YuvImage yuvImage = new YuvImage(frame.getGrayscaleImageData().array(), ImageFormat.NV21, width, height, null);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        yuvImage.compressToJpeg(new Rect(left, top, right, bottom), 100, byteArrayOutputStream);
        byte[] jpegArray = byteArrayOutputStream.toByteArray();
        Bitmap bitmap = BitmapFactory.decodeByteArray(jpegArray, 0, jpegArray.length);
        Frame croppedFrame =
                new Frame.Builder()
                        .setBitmap(bitmap)
                        .setRotation(meta.getRotation())
                        .build();

        mDelegate.receiveFrame(croppedFrame);
    }

    public boolean isOperational() {
        return mDelegate.isOperational();
    }

}