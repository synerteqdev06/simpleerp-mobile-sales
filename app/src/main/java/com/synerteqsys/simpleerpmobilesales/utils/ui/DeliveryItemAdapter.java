package com.synerteqsys.simpleerpmobilesales.utils.ui;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.synerteqsys.simpleerpmobilesales.R;
import com.synerteqsys.simpleerpmobilesales.data.ScannedDelItem;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by dev06
 *
 * Delivery Item Adapter for the List View of ScannedDelItem
 * (used in the Delivery Verification activity)
 */

public class DeliveryItemAdapter extends ArrayAdapter<ScannedDelItem> {

    private ArrayList<ScannedDelItem> dataSet;
    Context mContext;
    private int layout;
    public static DecimalFormat intFormat=new DecimalFormat("#,##0");

    private static class ViewHolder{
        TextView txtId;
        TextView txtQtyApproved;
        TextView txtQtyDone;
        TextView txtUnit;
        TextView txtDesc;
    }

    public DeliveryItemAdapter(ArrayList<ScannedDelItem> items, int layout, Context context){
        super(context, layout, items);
        this.layout = layout;
        this.dataSet =items;
        this.mContext=context;
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        ScannedDelItem item = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(layout, parent, false);
            viewHolder.txtQtyApproved = convertView.findViewById(R.id.qtyApproved);
            viewHolder.txtQtyDone = convertView.findViewById(R.id.qtyDone);
            viewHolder.txtUnit = convertView.findViewById(R.id.unit);
            viewHolder.txtDesc = convertView.findViewById(R.id.desc);
            viewHolder.txtId= convertView.findViewById(R.id.id);
            result=convertView;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        lastPosition = position;

        viewHolder.txtQtyApproved.setText( intFormat.format(item.getQtyApproved()));
        viewHolder.txtQtyDone.setText( intFormat.format(item.getQtyDone()));
        viewHolder.txtUnit.setText(item.getUnit());
        viewHolder.txtDesc.setText(item.getDesc());
        // if the search is the id, we don't print the id
        viewHolder.txtId.setText(item.getDisplayId());

        if(item.getQtyApproved() == 0){
            convertView.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.errorBg));
        } else if (item.getQtyDone() != item.getQtyApproved()){
            convertView.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.warningBg));
        } else {
            convertView.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.successBg));
        }


        // Return the completed view to render on screen
        return convertView;
    }
}
