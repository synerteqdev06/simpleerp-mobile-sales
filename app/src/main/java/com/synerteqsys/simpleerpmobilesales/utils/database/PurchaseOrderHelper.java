package com.synerteqsys.simpleerpmobilesales.utils.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.synerteqsys.simpleerpmobilesales.data.models.InventoryIn;
import com.synerteqsys.simpleerpmobilesales.data.models.InventoryInDet;
import com.synerteqsys.simpleerpmobilesales.utils.Constants;
import com.synerteqsys.simpleerpmobilesales.utils.FormatUtils;

import java.util.ArrayList;
import java.util.List;

public class PurchaseOrderHelper {
    /**
     * Creates a new pending purchase order for the user
     * @param context context at the moment of creation
     * @return the generated ID of the inserted purchase order
     */
    public static String createPurchaseOrder(Context context){
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String code = DatabaseHelper.getGUID();
        try(SQLiteDatabase db = helper.getWritableDatabase() ) {
            db.beginTransaction();
            ContentValues cv = new ContentValues();
            cv.put("inventory_in_id", code);
            cv.put("status", "P");
            cv.put("creation_dt", FormatUtils.today().getTime());
            cv.put("warehouse_id", DatabaseHelper.getWarehouse(context));
            cv.put("user_id", DatabaseHelper.getUserId(context));
            db.insert("inventory_in", null, cv);
            db.setTransactionSuccessful();
            db.endTransaction();
            Log.d(Constants.TAG, "creating purchase order " + code);
        }
        return code;
    }

    public static void addRemarksToPurchase(Context context, String purchaseId, String remarks){
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String code = DatabaseHelper.getGUID();
        try(SQLiteDatabase db = helper.getWritableDatabase() ) {
            db.beginTransaction();
            ContentValues cv = new ContentValues();
            cv.put("remarks", remarks);
            db.update("inventory_in", cv, "inventory_in_id=?", new String[]{purchaseId});
            db.setTransactionSuccessful();
            db.endTransaction();
            Log.d(Constants.TAG, "Added remarks to purchase order " + code);
        }
    }

    public static InventoryIn[] getPurchaseOrder(Context context){
        List<InventoryIn> purchaseList = new ArrayList<>();
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        // get the purchases of the user that hasn't been synced yet
        String selectSql = "select inventory_in_id, status, remarks, creation_dt, user_id, warehouse_id " +
                "from inventory_in where user_id=?";
        try(SQLiteDatabase db = helper.getReadableDatabase() ) {
            Cursor c = db.rawQuery(selectSql, new String[]{DatabaseHelper.getUserId(context)});
            if (c.moveToFirst()) {
                while ( !c.isAfterLast() ) {
                    String orderId = c.getString(c.getColumnIndex("inventory_in_id"));
                    purchaseList.add(new InventoryIn(
                            orderId,
                            c.getString(c.getColumnIndex("status")),
                            c.getString(c.getColumnIndex("remarks")),
                            c.getString(c.getColumnIndex("creation_dt")),
                            c.getString(c.getColumnIndex("user_id")),
                            c.getString(c.getColumnIndex("warehouse_id")),
                            getPurchaseOrderItems(db, orderId)
                    ));
                    c.moveToNext();
                }
            }
            c.close();
        }

        Log.d(Constants.TAG, "Found " + purchaseList.size() + " purchase orders");
        return purchaseList.toArray(new InventoryIn[0]);
    }

    private static InventoryInDet[] getPurchaseOrderItems(SQLiteDatabase db, String orderId){
        List<InventoryInDet> itemList = new ArrayList<>();

        String selectSql = "select inventory_in_det_id, inventory_in_id, prod_id, qty " +
                "from inventory_in_det where inventory_in_id=?";

        Cursor c = db.rawQuery(selectSql, new String[]{orderId});
        if (c.moveToFirst()) {
            while (!c.isAfterLast()) {
                itemList.add(new InventoryInDet(
                        c.getString(c.getColumnIndex("inventory_in_det_id")),
                        c.getString(c.getColumnIndex("inventory_in_id")),
                        c.getString(c.getColumnIndex("prod_id")),
                        c.getLong(c.getColumnIndex("qty"))
                ));
                c.moveToNext();
            }
        }
        c.close();

        return itemList.toArray(new InventoryInDet[0]);
    }

    public static String addPurchaseOrderItem(Context context, String purchaseId, String productId, int qty) {
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String pId= DatabaseHelper.getGUID();
        try(SQLiteDatabase db = helper.getWritableDatabase() ) {
            db.beginTransaction();
            ContentValues cv = new ContentValues();
            cv.put("inventory_in_det_id", pId);
            cv.put("inventory_in_id", purchaseId);
            cv.put("prod_id", productId);
            cv.put("qty", qty);
            db.insert("inventory_in_det", null, cv);
            db.setTransactionSuccessful();
            db.endTransaction();
            Log.d(Constants.TAG, purchaseId + ": " + productId );
        }
        return pId;
    }

    public static void deletePurchaseOrders(Context context, String[] orders){
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        try(SQLiteDatabase db = helper.getWritableDatabase() ) {
            db.beginTransaction();
            for(String orderId : orders) {
                db.delete("inventory_in_det", "inventory_in_id=?", new String[]{orderId});
                db.delete("inventory_in", "inventory_in_id=?", new String[]{orderId});
            }
            Log.d(Constants.TAG, "Deleted " + orders.length + " purchase orders");
            db.setTransactionSuccessful();
            db.endTransaction();
        }
    }
}
