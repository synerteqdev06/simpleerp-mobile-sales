package com.synerteqsys.simpleerpmobilesales.utils.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.synerteqsys.simpleerpmobilesales.utils.Constants;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LogHelper {
    /**
     * Adds a new log entry at the current time
     * @param context context at time of insertion
     * @param info The information the log will contain.
     */
    public static void addLog(Context context, String info){
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String time = Long.toString(new Date().getTime());
        try(SQLiteDatabase db = helper.getWritableDatabase() ){
            db.beginTransaction();
            ContentValues cv = new ContentValues();
            cv.put("log_info", info);
            cv.put("log_time", time);
            cv.put("user_id", DatabaseHelper.getUserId(context));
            db.insert("log", null, cv);
            db.setTransactionSuccessful();
            db.endTransaction();
            Log.d(Constants.TAG, "Log created at " + time);
        }
    }

    /**
     * retrieves all logs from the database for a user
     * @param context context at the time of retrieval
     * @return list of all logs stored in the database for the user
     */
    public static com.synerteqsys.simpleerpmobilesales.data.models.Log[] getLogs(Context context){
        List<com.synerteqsys.simpleerpmobilesales.data.models.Log> logList = new ArrayList<>();
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        String selectSql = "select cust_warehouse_id, warehouse_name, customer_id " +
                "from cust_warehouse where user_id=? order by warehouse_name collate nocase";
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor c = db.rawQuery(selectSql, new String[]{DatabaseHelper.getUserId(context)});
        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                logList.add(new com.synerteqsys.simpleerpmobilesales.data.models.Log(
                        c.getInt(c.getColumnIndex("cust_warehouse_id")),
                        c.getString(c.getColumnIndex("warehouse_name")),
                        c.getString(c.getColumnIndex("customer_id"))
                ));
                c.moveToNext();
            }
        }
        c.close();

        Log.d(Constants.TAG, "Found " + logList.size() + " logs");
        return logList.toArray(new com.synerteqsys.simpleerpmobilesales.data.models.Log[0]);
    }
}
