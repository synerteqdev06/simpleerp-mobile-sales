package com.synerteqsys.simpleerpmobilesales;

import android.content.Context;
import android.content.SharedPreferences;
import android.org.apache.commons.codec.digest.DigestUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.synerteqsys.simpleerpmobilesales.activities.LoginActivity;
import com.synerteqsys.simpleerpmobilesales.data.Response;
import com.synerteqsys.simpleerpmobilesales.data.ScannedDelItem;
import com.synerteqsys.simpleerpmobilesales.data.UserData;
import com.synerteqsys.simpleerpmobilesales.data.UserPreference;
import com.synerteqsys.simpleerpmobilesales.data.models.CustWarehouse;
import com.synerteqsys.simpleerpmobilesales.data.models.Customer;
import com.synerteqsys.simpleerpmobilesales.data.models.Delivery;
import com.synerteqsys.simpleerpmobilesales.data.models.DirectSale;
import com.synerteqsys.simpleerpmobilesales.data.models.DirectSaleSession;
import com.synerteqsys.simpleerpmobilesales.data.models.InventoryIn;
import com.synerteqsys.simpleerpmobilesales.data.models.PriceList;
import com.synerteqsys.simpleerpmobilesales.data.models.PriceListDet;
import com.synerteqsys.simpleerpmobilesales.data.models.PriceListOption;
import com.synerteqsys.simpleerpmobilesales.data.models.Product;
import com.synerteqsys.simpleerpmobilesales.data.models.SaleOrder;
import com.synerteqsys.simpleerpmobilesales.data.models.Serial;
import com.synerteqsys.simpleerpmobilesales.data.models.User;
import com.synerteqsys.simpleerpmobilesales.data.models.Warehouse;
import com.synerteqsys.simpleerpmobilesales.utils.AsyncHttpGetTask;
import com.synerteqsys.simpleerpmobilesales.utils.AsyncHttpPostTask;
import com.synerteqsys.simpleerpmobilesales.utils.Constants;
import com.synerteqsys.simpleerpmobilesales.utils.database.DatabaseHelper;
import com.synerteqsys.simpleerpmobilesales.utils.FormatUtils;
import com.synerteqsys.simpleerpmobilesales.utils.LoginTask;
import com.synerteqsys.simpleerpmobilesales.utils.ResponseParser;
import com.synerteqsys.simpleerpmobilesales.utils.database.DeliveryHelper;
import com.synerteqsys.simpleerpmobilesales.utils.database.DirectSaleHelper;
import com.synerteqsys.simpleerpmobilesales.utils.database.PurchaseOrderHelper;
import com.synerteqsys.simpleerpmobilesales.utils.database.SaleOrderHelper;
import com.synerteqsys.simpleerpmobilesales.utils.database.UserHelper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.shadows.ShadowToast;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertNotNull;

/**
 * Created by dev06 on 04/01/2018.
 */

@RunWith(RobolectricTestRunner.class)
public class ServerRequestTest {
    Context context;

    //for logging in
    User user;
    LoginTask task;


    @Before
    public void setUp() throws Exception{
        context = RuntimeEnvironment.application;
        LoginActivity login = Robolectric.buildActivity(LoginActivity.class).create().resume().get();
        String uname = "qwe";
        String pword = "1";
        login.logIn(uname, pword);

        user = UserHelper.searchUser(RuntimeEnvironment.application, uname);
        task = new LoginTask(login, uname, pword);
        try {
            task.execute(new String[]{user.getServerUrl(), uname, pword}).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


        UserPreference.setStringPreference(context, Constants.USER_ID, "1");
        System.out.println("\n------------------------------------------------------------");
    }

    @Test
    public void sendLogin() throws JsonProcessingException, UnsupportedEncodingException {
        String url = "http://localhost:14697/Auth_App/login";
        final String uname = "japs";
        final String pword = "Japs123@";
        Map<String, Object> data = new LinkedHashMap<String, Object>();
        data.put("userName", uname);
        data.put("password", pword);

        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(data);
        AsyncHttpPostTask asyncPost = new AsyncHttpPostTask(context, new ResponseParser() {
            @Override
            public void parse(Context context, String response) throws IOException {
                System.out.println("Response: ");
                System.out.println(response);
                ObjectMapper userMapper = new ObjectMapper();
                User user = userMapper.readValue(response, User.class);
                String salt = user.getPasswordSalt();

                // compute password hash
                String saltedPass = pword + ":" + salt;
                String hash = DigestUtils.sha256Hex(saltedPass);

                String userPass = user.getPassword();
                if (!hash.equals(userPass)) {
                    System.out.println("Not equal");
                } else {
                    System.out.println("Equal");
                }

            }
        }, json);

        asyncPost.execute(new String[] {url, "application/json"});
    }

    @Test
    public void downloadProducts(){
        String productMsg = "Completed Product download";
        AsyncHttpGetTask getProd = new AsyncHttpGetTask(context, new ResponseParser() {
            @Override
            public void parse(Context context, String response) throws IOException {
                ObjectMapper mapper = new ObjectMapper();
                try {
                    UserData data = mapper.readValue(response, UserData.class);
                    for(Product wh : data.getProduct()){
                        System.out.println(wh.getProdName());
                    }
                    assertNotNull(data.getProduct());
                } catch (Exception e){
                    e.printStackTrace();
                }

            }
        });
        getProd.setCompleteMessage(productMsg);

        getProd.execute(user.getServerUrl() + Constants.PRODUCT_URL);
    }

    @Test
    public void downloadWarehouses(){
        String whMsg = "Warehouse Download Successful";
        AsyncHttpGetTask getProd = new AsyncHttpGetTask(context, new ResponseParser() {
            @Override
            public void parse(Context context, String response) throws IOException {
                ObjectMapper mapper = new ObjectMapper();
                try {
                    //replace "data" key with "warehouse" so that the parsing goes correctly"
                    if(response.contains("{\"data\":[")){
                        response = response.replaceFirst("data", "warehouses");
                    }
                    UserData data = mapper.readValue(response, UserData.class);
                    for(Warehouse wh : data.getWarehouses()){
                        System.out.println(wh.getWarehouseName());
                    }

                    assertNotNull(data.getWarehouses());
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        getProd.setCompleteMessage(whMsg);

        getProd.execute(user.getServerUrl() + Constants.WAREHOUSE_URL);

    }

    @Test
    public void downloadCustomers(){
        String whMsg = "Customer Download Successful";
        AsyncHttpGetTask getProd = new AsyncHttpGetTask(context, new ResponseParser() {
            @Override
            public void parse(Context context, String response) throws IOException {
                ObjectMapper mapper = new ObjectMapper();
                try {
                    UserData data = mapper.readValue(response, UserData.class);
                    for(Customer wh : data.getCustomer()){
                        System.out.println(wh.getPartnerName());
                    }
                    assertNotNull(data.getCustomer());
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        getProd.setCompleteMessage(whMsg);

        getProd.execute(user.getServerUrl() + Constants.CUSTOMER_URL + "&warehouse=774");

    }

    @Test
    public void downloadCustWarehouse(){
        String whMsg = "Customer Warehouse Download Successful";
        AsyncHttpGetTask getProd = new AsyncHttpGetTask(context, new ResponseParser() {
            @Override
            public void parse(Context context, String response) throws IOException {
                ObjectMapper mapper = new ObjectMapper();
                try {
                    UserData data = mapper.readValue(response, UserData.class);
                    for(CustWarehouse wh : data.getCustWarehouse()){
                        System.out.println(wh.getWarehouseName());
                    }
                    assertNotNull(data.getCustWarehouse());
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        getProd.setCompleteMessage(whMsg);

        getProd.execute(user.getServerUrl() + Constants.CUSTOMER_WH_URL + "&warehouse=774");

    }

    @Test
    public void downloadPriceList(){
        String whMsg = "Pricelist Download Successful";
        AsyncHttpGetTask getProd = new AsyncHttpGetTask(context, new ResponseParser() {
            @Override
            public void parse(Context context, String response) throws IOException {
                ObjectMapper mapper = new ObjectMapper();
                try {
                    UserData data = mapper.readValue(response, UserData.class);
                    for(PriceList wh : data.getPricelist()){
                        System.out.println(wh.getPriceListId());
                    }
                    assertNotNull(data.getPricelist());
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        getProd.setCompleteMessage(whMsg);
        getProd.execute(user.getServerUrl() + Constants.PRICELIST_URL + "&warehouse=774");

    }

    @Test
    public void downloadPriceListDet(){
        String whMsg = "Pricelist Details Download Successful";
        AsyncHttpGetTask getProd = new AsyncHttpGetTask(context, new ResponseParser() {
            @Override
            public void parse(Context context, String response) throws IOException {
                ObjectMapper mapper = new ObjectMapper();
                try {
                    UserData data = mapper.readValue(response, UserData.class);
                    for(PriceListDet wh : data.getPricelistdet()){
                        System.out.println(wh.getPriceListId() + " - " + wh.getProdId());
                    }
                    assertNotNull(data.getPricelistdet());
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        getProd.setCompleteMessage(whMsg);
        getProd.execute(user.getServerUrl() + Constants.PRICELIST_DET_URL + "&warehouse=774");

    }

    @Test
    public void downloadPriceListOption(){
        String whMsg = "Pricelist Options Download Successful";
        AsyncHttpGetTask getProd = new AsyncHttpGetTask(context, new ResponseParser() {
            @Override
            public void parse(Context context, String response) throws IOException {
                ObjectMapper mapper = new ObjectMapper();
                System.out.println(response);
                try {
                    UserData data = mapper.readValue(response, UserData.class);
                    assertNotNull(data.getPricelistdet());
                    assertNotNull(data.getPricelistopt());
                    for(PriceListOption plistOpt : data.getPricelistopt()){
                        System.out.println(plistOpt);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        getProd.setCompleteMessage(whMsg);
        getProd.execute(user.getServerUrl() + Constants.PRICELIST_OPTION_URL + "&warehouse=984");

    }

    @Test
    public void downloadDelivery(){
        String whMsg = "Delivery Download Successful";
        AsyncHttpGetTask getProd = new AsyncHttpGetTask(context, new ResponseParser() {
            @Override
            public void parse(Context context, String response) throws IOException {
                ObjectMapper mapper = new ObjectMapper();
                System.out.println(response);
                try {
                    UserData data = mapper.readValue(response, UserData.class);
                    assertNotNull(data.getDelivery());
                    assertNotNull(data.getDeliverydet());
                    for(Delivery del : data.getDelivery()){
                        System.out.println(del);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        getProd.setCompleteMessage(whMsg);
        getProd.execute(user.getServerUrl() + Constants.DELIVERY_URL+ "&warehouse=775");

    }

    @Test
    public void downloadSerial(){
        String whMsg = "Serial Download Successful";
        AsyncHttpGetTask getProd = new AsyncHttpGetTask(context, new ResponseParser() {
            @Override
            public void parse(Context context, String response) throws IOException {
                ObjectMapper mapper = new ObjectMapper();
                System.out.println(response);
                try {
                    UserData data = mapper.readValue(response, UserData.class);
                    assertNotNull(data.getSerial());
                    for(Serial del : data.getSerial()){
                        System.out.println(del);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        getProd.setCompleteMessage(whMsg);
        getProd.execute(user.getServerUrl() + Constants.SERIAL_URL+ "&warehouse=775");

    }


    @Test
    public void sendSession() throws InterruptedException, UnsupportedEncodingException, JsonProcessingException {
        String sessionId = DirectSaleHelper.createSaleSession(context);
        String saleId = DirectSaleHelper.createDirectSaleOrder(context, new DirectSale(sessionId));
        DirectSaleHelper.addDirectSaleOrderItem(context, saleId, "675", 1, 70000.00, "1021");
        DirectSaleHelper.addDirectSaleOrderItem(context, saleId, "681", 5, 10.00, "1026");
        saleId = DirectSaleHelper.createDirectSaleOrder(context, new DirectSale(sessionId));
        DirectSaleHelper.addDirectSaleOrderItem(context, saleId, "681", 10, 10.00, "1026");
        DirectSaleHelper.closeSaleSession(context, sessionId);

        sessionId = DirectSaleHelper.createSaleSession(context);
        saleId = DirectSaleHelper.createDirectSaleOrder(context, new DirectSale(sessionId));
        DirectSaleHelper.addDirectSaleOrderItem(context, saleId, "681", 12, 10.00, "1026");
        DirectSaleHelper.closeSaleSession(context, sessionId);

        DirectSaleSession[] sessionList = DirectSaleHelper.getDirectSaleSessions(context);
        ObjectMapper mapper = new ObjectMapper();
        String jsonMap = mapper.writeValueAsString(sessionList);

        String sessionData;
        Map<String, Object> data = new LinkedHashMap<String, Object>();
        data.put("ACTION", Constants.ACT_UPLOAD_SESSION);
        data.put("CSRF_TOKEN", task.getCsrfToken());
        data.put("sessions", jsonMap);
        sessionData = FormatUtils.formatFormData(data);

        System.out.println(sessionData);

        AsyncHttpPostTask uploadSession = new AsyncHttpPostTask(RuntimeEnvironment.application, new ResponseParser() {
            @Override
            public void parse(Context context, String response) throws IOException {
                System.out.println(response);
                ObjectMapper mapper = new ObjectMapper();
                Response resp = mapper.readValue(response, Response.class);
                if(resp.getStatus() == 1){
                    System.out.println("Successful");
                    Map data = (Map) resp.getAttributes().get("data");
                    List<String> sessions = (List<String>) data.get("records");
                    for (String id : sessions)
                        System.out.println(id);
                } else {
                    System.out.println("Something went wrong on the server");
                }
            }
        }, sessionData);
        uploadSession.setCompleteMessage("Session Upload Complete");
        uploadSession.execute(user.getServerUrl() + Constants.UPLOAD_URL, "application/x-www-form-urlencoded; charset=UTF-8");
        System.out.println(ShadowToast.getTextOfLatestToast());
    }

    @Test
    public void sendPurchase() throws InterruptedException, UnsupportedEncodingException, JsonProcessingException {
        String purchaseId =  PurchaseOrderHelper.createPurchaseOrder(context);
        PurchaseOrderHelper.addPurchaseOrderItem(context, purchaseId, "675", 7);
        PurchaseOrderHelper.addPurchaseOrderItem(context, purchaseId, "681", 15);

        InventoryIn[] orderList = PurchaseOrderHelper.getPurchaseOrder(context);
        ObjectMapper mapper = new ObjectMapper();
        String jsonMap = mapper.writeValueAsString(orderList);

        Map<String, Object> data = new LinkedHashMap<>();
        data.put("ACTION", Constants.ACT_UPLOAD_INVENTORY_IN);
        data.put("CSRF_TOKEN", task.getCsrfToken());
        data.put("orders", jsonMap);

        String orderData = FormatUtils.formatFormData(data);
        System.out.println(orderData);

        AsyncHttpPostTask uploadOrders = new AsyncHttpPostTask(RuntimeEnvironment.application, new ResponseParser() {
            @Override
            public void parse(Context context, String response) throws IOException {
                System.out.println(response);
                ObjectMapper mapper = new ObjectMapper();
                Response resp = mapper.readValue(response, Response.class);
                if(resp.getStatus() == 1){
                    System.out.println("Successful");
                    Map data = (Map) resp.getAttributes().get("data");
                    List<String> orders = (List<String>) data.get("records");
                    for (String id : orders)
                        System.out.println(id);
                } else {
                    System.out.println("Something went wrong on the server");
                }
            }
        }, orderData);
        uploadOrders.setCompleteMessage("Purchase Upload Complete");
        uploadOrders.execute(user.getServerUrl() + Constants.UPLOAD_URL, Constants.SERVER_REQUEST_TYPE);
        System.out.println(ShadowToast.getTextOfLatestToast());
    }

    @Test
    public void sendSales() throws InterruptedException, UnsupportedEncodingException, JsonProcessingException {
        SharedPreferences sharedPreferences = RuntimeEnvironment.application
                .getSharedPreferences("com.simpleerpmobilesales.preferences", Context.MODE_PRIVATE);
        sharedPreferences.edit().putString("warehouse", "774").commit();

        String saleId =  SaleOrderHelper.createSaleOrder(context, "630", "611");
        SaleOrderHelper.addSaleOrderLine(context,saleId, "733", 5, 54.0);
        SaleOrderHelper.addSaleOrderLine(context,saleId, "722", 20, 4.0);

        SaleOrder[] orderList = SaleOrderHelper.getSaleOrders(context);
        ObjectMapper mapper = new ObjectMapper();
        String jsonMap = mapper.writeValueAsString(orderList);
        Map<String, Object> data = new LinkedHashMap<>();
        data.put("ACTION", Constants.ACT_UPLOAD_SALE_ORDER);
        data.put("CSRF_TOKEN", task.getCsrfToken());
        data.put("orders", jsonMap);

        String orderData = FormatUtils.formatFormData(data);
        System.out.println(orderData);

        AsyncHttpPostTask uploadOrders = new AsyncHttpPostTask(RuntimeEnvironment.application, new ResponseParser() {
            @Override
            public void parse(Context context, String response) throws IOException {
                System.out.println(response);
                ObjectMapper mapper = new ObjectMapper();
                Response resp = mapper.readValue(response, Response.class);
                if(resp.getStatus() == 1){
                    System.out.println("Successful");
                    Map data = (Map) resp.getAttributes().get("data");
                    List<String> orders = (List<String>) data.get("records");
                    for (String id : orders)
                        System.out.println(id);
                } else {
                    System.out.println("Something went wrong on the server");
                }
            }
        }, orderData);
        uploadOrders.setCompleteMessage("Sale Upload Complete");
        uploadOrders.execute(user.getServerUrl() + Constants.UPLOAD_URL, Constants.SERVER_REQUEST_TYPE);
        System.out.println(ShadowToast.getTextOfLatestToast());
    }

    @Test
    public void sendDeliveries() throws InterruptedException, IOException {
        setupDelivery();

        Delivery[] orderList = DeliveryHelper.getVerifiedDeliveries(context);
        ObjectMapper mapper = new ObjectMapper();
        String jsonMap = mapper.writeValueAsString(orderList);
        System.out.println(jsonMap);
        Map<String, Object> data = new LinkedHashMap<>();
        data.put("ACTION", Constants.ACT_UPLOAD_DELIVERY);
        data.put("CSRF_TOKEN", task.getCsrfToken());
        data.put("deliveries", jsonMap);

        String delData = FormatUtils.formatFormData(data);
        System.out.println(delData);

        AsyncHttpPostTask uploadDel = new AsyncHttpPostTask(RuntimeEnvironment.application, new ResponseParser() {
            @Override
            public void parse(Context context, String response) throws IOException {
                System.out.println(response);
                ObjectMapper mapper = new ObjectMapper();
                Response resp = mapper.readValue(response, Response.class);
                if(resp.getStatus() == 1){
                    System.out.println("Successful");
                    Map data = (Map) resp.getAttributes().get("data");
                    List<String> orders = (List<String>) data.get("records");
                    for (String id : orders)
                        System.out.println(id);
                } else {
                    System.out.println("Something went wrong on the server");
                }
            }
        }, delData);
        uploadDel.setCompleteMessage("Delivery Upload Complete");
        uploadDel.execute(user.getServerUrl() + Constants.UPLOAD_URL, Constants.SERVER_REQUEST_TYPE);
        System.out.println(ShadowToast.getTextOfLatestToast());
    }

    void setupDelivery() throws IOException {
        String delStr = "{\"delivery\":[{\"deliveryId\":7702,\"deliveryNo\":\"2787\"},{\"deliveryId\":7759,\"deliveryNo\":\"2844\"}],\"deliverydet\":[{\"qtyApprovedTotal\":0,\"qtyDoneTotal\":0,\"deliveryId\":7702,\"deliveryDetId\":36492,\"prodId\":1029},{\"qtyApprovedTotal\":1,\"qtyDoneTotal\":1,\"deliveryId\":7759,\"deliveryDetId\":36713,\"prodId\":913},{\"qtyApprovedTotal\":3,\"qtyDoneTotal\":3,\"deliveryId\":7759,\"deliveryDetId\":36714,\"prodId\":1029},{\"qtyApprovedTotal\":2,\"qtyDoneTotal\":2,\"deliveryId\":7759,\"deliveryDetId\":36715,\"prodId\":1053}]}";
        ObjectMapper mapper = new ObjectMapper();
        UserData data = mapper.readValue(delStr, UserData.class);
        DatabaseHelper.processData(context, data);

        String delId = "7759";
        ArrayList<ScannedDelItem> list = new ArrayList<>();
        ScannedDelItem item = new ScannedDelItem();
        item.setId("1053");
        item.setQtyDone(2);
        list.add(item);
        item = new ScannedDelItem();
        item.setId("913");
        item.setQtyDone(2);
        list.add(item);
        DeliveryHelper.setDoneQtyOfDelivery(context, delId, list);
        DeliveryHelper.setDeliveryVerifyStatus(context, delId, "Y");
    }

}

