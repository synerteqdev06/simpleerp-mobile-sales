package com.synerteqsys.simpleerpmobilesales;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.synerteqsys.simpleerpmobilesales.data.ScannedDelItem;
import com.synerteqsys.simpleerpmobilesales.data.UserData;
import com.synerteqsys.simpleerpmobilesales.data.UserPreference;
import com.synerteqsys.simpleerpmobilesales.data.models.CustWarehouse;
import com.synerteqsys.simpleerpmobilesales.data.models.Customer;
import com.synerteqsys.simpleerpmobilesales.data.models.Delivery;
import com.synerteqsys.simpleerpmobilesales.data.models.DirectSale;
import com.synerteqsys.simpleerpmobilesales.data.models.DirectSaleSession;
import com.synerteqsys.simpleerpmobilesales.data.models.InventoryBatch;
import com.synerteqsys.simpleerpmobilesales.data.models.InventoryIn;
import com.synerteqsys.simpleerpmobilesales.data.models.PriceListOption;
import com.synerteqsys.simpleerpmobilesales.data.models.Product;
import com.synerteqsys.simpleerpmobilesales.data.models.SaleOrder;
import com.synerteqsys.simpleerpmobilesales.data.models.Serial;
import com.synerteqsys.simpleerpmobilesales.data.models.User;
import com.synerteqsys.simpleerpmobilesales.utils.Constants;
import com.synerteqsys.simpleerpmobilesales.utils.database.CustomerHelper;
import com.synerteqsys.simpleerpmobilesales.utils.database.DatabaseHelper;
import com.synerteqsys.simpleerpmobilesales.utils.FormatUtils;
import com.synerteqsys.simpleerpmobilesales.utils.database.DeliveryHelper;
import com.synerteqsys.simpleerpmobilesales.utils.database.DirectSaleHelper;
import com.synerteqsys.simpleerpmobilesales.utils.database.InvBatchHelper;
import com.synerteqsys.simpleerpmobilesales.utils.database.LogHelper;
import com.synerteqsys.simpleerpmobilesales.utils.database.PriceListHelper;
import com.synerteqsys.simpleerpmobilesales.utils.database.ProductHelper;
import com.synerteqsys.simpleerpmobilesales.utils.database.PurchaseOrderHelper;
import com.synerteqsys.simpleerpmobilesales.utils.database.SaleOrderHelper;
import com.synerteqsys.simpleerpmobilesales.utils.database.SerialHelper;
import com.synerteqsys.simpleerpmobilesales.utils.database.UserHelper;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;

/**
 * Tests the methods in the DatabaseHelper class
 *
 */

@RunWith(RobolectricTestRunner.class)
public class DbMethodsTest {
    private DatabaseHelper dbHelper;
    private Context context;

    @Before
    public void setUp() throws Exception {
        context = RuntimeEnvironment.application;
        dbHelper = DatabaseHelper.getInstance(context);

        UserPreference.setStringPreference(context, Constants.USER_ID, "1");
        DatabaseHelper.clearWholeDatabase(context);
    }

    @After
    public void tearDown() {
        dbHelper.close();
        dbHelper = null;
    }

    @Test
    public void testDBCreated(){
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // Verify if the DB is opening correctly
        assertTrue("DB didn't open", db.isOpen());
        db.close();
    }

    @Test
    public void testTablesCreated(){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] tableList =  new String[] {"users", "product", "direct_sale_session", "direct_sale",
                "direct_sale_item", "inventory_in", "inventory_in_det", "cust_warehouse",
                "price_list", "customer", "price_list_det", "sale_order", "sale_order_line", "price_list_option",
                "delivery", "delivery_det", "serial"};
        Cursor c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name!='android_metadata'", null);
        int tableCnt = 0;
        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                String col = c.getString( c.getColumnIndex("name"));
                boolean isFound = Arrays.asList(tableList).contains(col);
                assertTrue(col + " isn't supposed to be in the database!", isFound);
                tableCnt++;
                c.moveToNext();
            }
        }
        assertEquals(tableList.length, tableCnt);
    }

    @Test
    public void testOrderCreation(){
        String orderId = DirectSaleHelper.createDirectSaleOrder(context, new DirectSale(UUID.randomUUID().toString().replace("-","")));
        System.out.println("Order created with the ID: " + orderId);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        long storedRows = DatabaseUtils.queryNumEntries(db, "direct_sale", "direct_sale_id=?",new String[]{orderId});
        assertEquals(1, storedRows);
    }

    @Test
    public void testOrderItemCreation(){
        String orderId = DirectSaleHelper.createDirectSaleOrder(context, new DirectSale(0,0));
        System.out.println("Order created with the ID: " + orderId);
        int qty = 3;
        double price = 43.50;
        String itemId = DirectSaleHelper.addDirectSaleOrderItem(context,orderId, "123", qty, price, "123");
        System.out.println("Added an item to the order with the ID: " + itemId);
        double expectedTotal = (double) qty * price;

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String total_select = "SELECT total_amount_due from direct_sale where direct_sale_id=?";
        Cursor c = db.rawQuery(total_select, new String[]{orderId});
        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                Double storedTotal = c.getDouble( c.getColumnIndex("total_amount_due"));
                assertEquals(expectedTotal, storedTotal);
                c.moveToNext();
            }
        }

        long storedRows = DatabaseUtils.queryNumEntries(db, "direct_sale_item", "direct_sale_id=?",new String[]{orderId});
        assertEquals(1, storedRows);
        db.close();
    }

    @Test
    public void testCreateProduct() throws IOException {
        String response = "{\"product\":[{\"srp\":12,\"bala\":null,\"baseUnit\":\"12\",\"status\":\"A\",\"prodName\":\"Barad123213\",\"prodId\":687,\"sku\":\"sdfsdfsdf123123123\",\"barcode\":\"4800001000100\",\"packagingId\":1030,\"status\":\"A\"},{\"srp\":105,\"baseUnit\":\"12\",\"prodName\":\"1233\",\"prodId\":684,\"sku\":\"1223\",\"barcode\":\"4802122001431\",\"packagingId\":1051,\"status\":\"A\"}]}";
        parseProducts(context, response);

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long storedRows = DatabaseUtils.queryNumEntries(db, "product", null, null);
        assertEquals(2, storedRows);
    }

    @Ignore
    @Test
    public void testSearchProduct() throws IOException {
        String response = "{\"product\":[{\"prodId\":645,\"sku\":\"12123123\",\"prodName\":\"new product123\",\"barcode\":\"4800010075069\",\"baseUnit\":\"pcs\",\"srp\":50.0},{\"prodId\":644,\"sku\":\"Prod0014\",\"prodName\":\"Product 14\",\"barcode\":null,\"baseUnit\":\"Piece\",\"srp\":150.0}]}";
        parseProducts(context, response);

        // search SKU
        Product p = ProductHelper.findProduct(context, "12123123");
        assertNotNull(p);

        // search Barcode
        p = ProductHelper.findProduct(context, "4800010075069");
        assertNotNull(p);

        // search empty
        p = ProductHelper.findProduct(context, "4800110075069");
        assertNull(p);
    }

    @Test
    public void testGetProductWithPackaging() throws IOException {
        String response = "{\"product\":[{\"prodId\":645,\"sku\":\"12123123\",\"prodName\":\"new product123\",\"barcode\":\"4800010075069\",\"baseUnit\":\"pcs\",\"srp\":50.0,\"status\":\"A\"},{\"prodId\":644,\"sku\":\"Prod0014\",\"prodName\":\"Product 14\",\"barcode\":null,\"baseUnit\":\"Piece\",\"srp\":150.0,\"status\":\"A\"}]}";
        parseProducts(context, response);

        Product[] pList = ProductHelper.getProductsWithPackaging(context);

        for(Product c : pList)
            System.out.println(c.getProdName());
        assertNotNull(pList);

        assertEquals(2, pList.length);
    }

    @Test
    public void testGetProductWithNoPackaging() throws IOException {
        String response = "{\"product\":[{\"prodId\":644,\"sku\":\"12123123\",\"prodName\":\"new product123\",\"barcode\":\"4800010075069\",\"baseUnit\":\"pcs\",\"srp\":50.0,\"status\":\"A\",\"packagingId\":\"124\"},{\"prodId\":644,\"sku\":\"Prod0014\",\"prodName\":\"Product 14\",\"barcode\":null,\"baseUnit\":\"Piece\",\"srp\":150.0,\"status\":\"A\",\"packagingId\":\"124\"}]}";
        parseProducts(context, response);

        Product[] pList = ProductHelper.getProductsNoPackaging(context);

        for(Product c : pList)
            System.out.println(c.getProdName());
        assertNotNull(pList);

        assertEquals(1, pList.length);
    }

    @Test
    public void testGetBaseProduct() throws IOException {
        String response = "{\"product\":[{\"prodId\":645,\"sku\":\"12123123\",\"prodName\":\"new product123\",\"barcode\":\"4800010075069\",\"baseUnit\":\"pcs\",\"srp\":50.0,\"packagingId\":\"123\",\"status\":\"A\"},{\"prodId\":645,\"packagingId\":\"130\",\"sku\":\"Prod0014\",\"prodName\":\"Product 14\",\"barcode\":null,\"baseUnit\":\"Piece\",\"srp\":150.0,\"status\":\"A\"}]}";
        parseProducts(context, response);

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long storedRows = DatabaseUtils.queryNumEntries(db, "product", "prod_id='645'", null);
        assertEquals(2, storedRows);

        Product pList = ProductHelper.findBaseProduct(context, "645");
        assertEquals("123", pList.getPackagingId());
        db.close();
    }

    public void parseProducts(Context context, String response) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        UserData data = mapper.readValue(response, UserData.class);
        DatabaseHelper.processData(context, data);
    }

    @Test
    public void testCreateSaleSession(){
        String saleSessionId = DirectSaleHelper.createSaleSession(context);
        System.out.println("Sale Session with ID " + saleSessionId + " created");

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long storedRows = DatabaseUtils.queryNumEntries(db, "direct_sale_session", "direct_sale_session_id=?",new String[]{saleSessionId});
        assertEquals(1, storedRows);
        db.close();

    }

    @Test
    public void testCloseSaleSession() throws InterruptedException {
        String saleSessionId = DirectSaleHelper.createSaleSession(context);
        System.out.println("Sale Session with ID " + saleSessionId + " created");

        Thread.sleep(2000);
        DirectSaleHelper.closeSaleSession(context, saleSessionId);

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT end_time FROM direct_sale_session WHERE direct_sale_session_id='"+saleSessionId+"'", null);
        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                String col = c.getString( c.getColumnIndex("end_time"));
                System.out.println("Session ended at " + col);
                assertNotNull(col);
                c.moveToNext();
            }
        }
        db.close();
    }

    @Test
    public void testGetSession() throws JsonProcessingException, UnsupportedEncodingException {
        //create the session, add some sales and close it afterwards
        String sessionId = DirectSaleHelper.createSaleSession(context);
        String saleId = DirectSaleHelper.createDirectSaleOrder(context, new DirectSale(sessionId));
        DirectSaleHelper.addDirectSaleOrderItem(context, saleId, "675", 1, 70000.00, "1021");
        DirectSaleHelper.addDirectSaleOrderItem(context, saleId, "681", 5, 10.00, "1026");
        saleId = DirectSaleHelper.createDirectSaleOrder(context, new DirectSale(sessionId));
        DirectSaleHelper.addDirectSaleOrderItem(context, saleId, "681", 10, 10.00, "1026");
        DirectSaleHelper.closeSaleSession(context, sessionId);

        int expectedSize = 1;
        DirectSaleSession[] sessionList = DirectSaleHelper.getDirectSaleSessions(context);
        assertEquals(expectedSize, sessionList.length);

        for(DirectSaleSession session : sessionList) {
            ObjectMapper mapper = new ObjectMapper();
            String jsonMap = mapper.writeValueAsString(session.getSales());

            Map<String, Object> data = new LinkedHashMap<>();
            data.put("ACTION", Constants.ACT_UPLOAD_SESSION);
            data.put("CSRF_TOKEN", "34AQ2G3aw9q");
            data.put("directSaleSessionId", session.getDirectSaleSessionId());
            data.put("salePersonId", session.getSalePersonId());
            data.put("startTime", session.getStartTime());
            data.put("endTime", session.getEndTime());
            data.put("sales", jsonMap);

            String sessionData = FormatUtils.formatFormData(data);
            System.out.println(sessionData);
        }


    }

    @Test
    public void testDeleteSessions() throws UnsupportedEncodingException, JsonProcessingException {
        testGetSession();
        DirectSaleSession[] sessions = DirectSaleHelper.getDirectSaleSessions(context);
        System.out.println("There are " + sessions.length + " sessions");
        String[] sessionId = new String[sessions.length];
        for(int i = 0; i < sessions.length; i++)
           sessionId[i] = sessions[i].getDirectSaleSessionId();

        DirectSaleHelper.deleteSaleSessions(context, sessionId);

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long storedRows = DatabaseUtils.queryNumEntries(db, "direct_sale_session", null,null);
        assertEquals(0, storedRows);
        storedRows = DatabaseUtils.queryNumEntries(db, "direct_sale", null,null);
        assertEquals(0, storedRows);
        storedRows = DatabaseUtils.queryNumEntries(db, "direct_sale_item", null,null);
        assertEquals(0, storedRows);

        db.close();
    }

    @Test
    public void testCreatePurchaseOrder(){
        String purchaseId =  PurchaseOrderHelper.createPurchaseOrder(context);

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long storedRows = DatabaseUtils.queryNumEntries(db, "inventory_in", "inventory_in_id=?", new String[]{purchaseId});
        assertEquals(1, storedRows);
        db.close();
    }

    @Test
    public void testAddRemarksToPurchase(){
        String purchaseId =  PurchaseOrderHelper.createPurchaseOrder(context);
        String remark = "This is a remark";
        PurchaseOrderHelper.addRemarksToPurchase(context, purchaseId, remark);

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String purchase_remarks = "SELECT remarks from inventory_in where inventory_in_id=?";
        Cursor c = db.rawQuery(purchase_remarks, new String[]{purchaseId});
        if (c.moveToFirst()) {
            String sync = c.getString( c.getColumnIndex("remarks"));
            assertEquals(remark, sync);
        }
        db.close();
    }

    @Test
    public void testAddPurchaseItem(){
        String purchaseId =  PurchaseOrderHelper.createPurchaseOrder(context);

        String itemId = PurchaseOrderHelper.addPurchaseOrderItem(context, purchaseId, "635", 7);
        System.out.println("Item added: " + itemId);
        itemId = PurchaseOrderHelper.addPurchaseOrderItem(context, purchaseId, "478", 15);
        System.out.println("Item added: " + itemId);

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long storedRows = DatabaseUtils.queryNumEntries(db, "inventory_in_det", "inventory_in_id=?", new String[]{purchaseId});
        assertEquals(2, storedRows);

    }

    @Test
    public void testGetPurchases() throws UnsupportedEncodingException, JsonProcessingException {
        String purchaseId =  PurchaseOrderHelper.createPurchaseOrder(context);

        PurchaseOrderHelper.addPurchaseOrderItem(context, purchaseId, "635", 7);
        PurchaseOrderHelper.addPurchaseOrderItem(context, purchaseId, "478", 15);

        InventoryIn[] orderList = PurchaseOrderHelper.getPurchaseOrder(context);
        assertEquals(1, orderList.length);

        ObjectMapper mapper = new ObjectMapper();
        String jsonMap = mapper.writeValueAsString(orderList);

        Map<String, Object> data = new LinkedHashMap<>();
        data.put("ACTION", Constants.ACT_UPLOAD_INVENTORY_IN);
        data.put("CSRF_TOKEN", "34AQ2G3aw9q");
        data.put("orders", jsonMap);

        String sessionData = FormatUtils.formatFormData(data);
        System.out.println(sessionData);
    }

    @Test
    public void testDeletePurchases() throws UnsupportedEncodingException, JsonProcessingException {
        testGetPurchases();
        InventoryIn[] orders = PurchaseOrderHelper.getPurchaseOrder(context);
        String[] orderIds = new String[orders.length];
        System.out.println("There are " + orders.length + " purchases");
        for(int i = 0; i < orders.length; i++)
            orderIds[i] = orders[i].getInventoryInId();

        PurchaseOrderHelper.deletePurchaseOrders(context, orderIds);

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long storedRows = DatabaseUtils.queryNumEntries(db, "inventory_in", null,null);
        assertEquals(0, storedRows);
        storedRows = DatabaseUtils.queryNumEntries(db, "inventory_in_det", null,null);
        assertEquals(0, storedRows);
        System.out.println("Deleted " + orders.length + " purchases");
        db.close();
    }

    @Test
    public void testProcessCustomer() throws IOException {
        String custString = "{\"customer\":[{\"partnerName\":\"*()&*(#$@\",\"priceListId\":530,\"partnerId\":630,\"status\":\"A\"},{\"partnerName\":\"Snack Vision\",\"priceListId\":530,\"partnerId\":400,\"status\":\"A\"}]}";
        ObjectMapper mapper = new ObjectMapper();
        UserData data = mapper.readValue(custString, UserData.class);

        DatabaseHelper.processData(context, data);
        System.out.println("Processing customers...");

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long storedRows = DatabaseUtils.queryNumEntries(db, "customer", null, null);
        assertEquals(data.getCustomer().length, storedRows);
        db.close();
    }

    @Test
    public void testProcessPriceList() throws IOException {
        String priceList = "{\"pricelist\":[{\"customerId\":630,\"priceListId\":490,\"status\":\"A\"},{\"customerId\":635,\"priceListId\":532,\"status\":\"A\"}]}";
        ObjectMapper mapper = new ObjectMapper();
        UserData data = mapper.readValue(priceList, UserData.class);

        DatabaseHelper.processData(context, data);
        System.out.println("Processing price list...");

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long storedRows = DatabaseUtils.queryNumEntries(db, "price_list", null, null);
        assertEquals(data.getPricelist().length, storedRows);
        db.close();
    }

    @Test
    public void testProcessPriceListDet() throws IOException {
        testProcessPriceList();
        String priceListDet = "{\"pricelistdet\":[{\"effectivePrice\":140.5,\"priceListId\":490,\"prodId\":644},{\"effectivePrice\":40,\"priceListId\":490,\"prodId\":645},{\"effectivePrice\":30,\"priceListId\":530,\"prodId\":645}]}";
        ObjectMapper mapper = new ObjectMapper();
        UserData data = mapper.readValue(priceListDet, UserData.class);

        DatabaseHelper.processData(context, data);
        System.out.println("Processing price list det...");

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long storedRows = DatabaseUtils.queryNumEntries(db, "price_list_det", null, null);
        assertEquals(2, storedRows);
        db.close();
    }

    @Test
    public void testGetCustomers() throws IOException {
        testProcessCustomer();
        Customer[] custList = CustomerHelper.getCustomers(context);
        for(Customer c : custList)
            System.out.println(c.getPartnerName());
        assertNotNull(custList);
    }

    @Test
    public void testCreateOrder() {
        String orderId = SaleOrderHelper.createSaleOrder(context, "360", "356");
        System.out.println("Sale order with ID " + orderId + " created");

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long storedRows = DatabaseUtils.queryNumEntries(db, "sale_order", "sale_order_id=?", new String[]{orderId});
        assertEquals(1, storedRows);
        db.close();

    }

    @Test
    public void testCreateOrderLine(){
        String orderId = SaleOrderHelper.createSaleOrder(context, "564", "122");
        System.out.println("Order created with the ID: " + orderId);

        int qty = 3;
        double price = 43.50;
        String itemId = SaleOrderHelper.addSaleOrderLine(context,orderId, "123", qty, price);
        System.out.println("Added an item to the order with the ID: " + itemId);
        double expectedTotal = (double) qty * price;

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String total_select = "SELECT amount_due from sale_order where sale_order_id=?";
        Cursor c = db.rawQuery(total_select, new String[]{orderId});
        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                Double storedTotal = c.getDouble( c.getColumnIndex("amount_due"));
                assertEquals(expectedTotal, storedTotal);
                c.moveToNext();
            }
        }

        long storedRows = DatabaseUtils.queryNumEntries(db, "sale_order_line", "sale_order_id=?",new String[]{orderId});
        assertEquals(1, storedRows);
        db.close();
    }

    @Test
    public void testGetOrder() throws JsonProcessingException, UnsupportedEncodingException {
        String orderId = SaleOrderHelper.createSaleOrder(context, "630", "611");
        SaleOrderHelper.addSaleOrderLine(context,orderId, "733", 3, 54.0);
        orderId = SaleOrderHelper.createSaleOrder(context, "668", "584");
        SaleOrderHelper.addSaleOrderLine(context,orderId, "733", 5, 54.0);
        SaleOrderHelper.addSaleOrderLine(context,orderId, "722", 20, 4.0);

        SaleOrder[] orderList = SaleOrderHelper.getSaleOrders(context);

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long storedRows = DatabaseUtils.queryNumEntries(db, "sale_order", null, null);
        assertEquals(storedRows, orderList.length);

        ObjectMapper mapper = new ObjectMapper();
        String jsonMap = mapper.writeValueAsString(orderList);
        System.out.println(jsonMap);

        Map<String, Object> data = new LinkedHashMap<>();
        data.put("ACTION", Constants.ACT_UPLOAD_SALE_ORDER);
        data.put("CSRF_TOKEN", "34AQ2G3aw9q");
        data.put("orders", jsonMap);

        String sessionData = FormatUtils.formatFormData(data);
        System.out.println(sessionData);
        db.close();

    }

    @Test
    public void testDeleteOrderLine() throws UnsupportedEncodingException, JsonProcessingException {
        testGetOrder();
        SaleOrder[] orders = SaleOrderHelper.getSaleOrders(context);
        String[] orderIds = new String[orders.length];
        System.out.println("There are " + orders.length + " sales");
        for(int i = 0; i < orders.length; i++)
            orderIds[i] = orders[i].getSaleOrderId();

        SaleOrderHelper.deleteSaleOrders(context, orderIds);

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long storedRows = DatabaseUtils.queryNumEntries(db, "sale_order", null,null);
        assertEquals(0, storedRows);
        storedRows = DatabaseUtils.queryNumEntries(db, "sale_order_line", null,null);
        assertEquals(0, storedRows);
        System.out.println("Deleted " + orders.length + " sales");
        db.close();
    }

    @Test
    public void testGetProdPrice() throws IOException {
        String prods = "{\"product\":[{\"prodId\":645,\"sku\":\"12123123\",\"prodName\":\"new product123\",\"barcode\":\"4800010075069\",\"baseUnit\":\"pcs\",\"srp\":50.0,\"status\":\"A\"},{\"prodId\":644,\"sku\":\"Prod0014\",\"prodName\":\"Product 14\",\"barcode\":null,\"baseUnit\":\"Piece\",\"srp\":150.0,\"status\":\"A\"}]}";
        parseProducts(context, prods);
        testProcessCustomer();
        testProcessPriceList();
        testProcessPriceListDet();

        Product p = ProductHelper.findProduct(context, "4800010075069");
        System.out.println(p.getProdName() + " - " + p.getSrp());
        assertEquals(p.getSrp(), 50.0);

        p = ProductHelper.findProduct(context, "4800010075069", "400", false);
        System.out.println(p.getProdName() + " - " + p.getSrp());
        assertEquals(p.getSrp(), 50.0);

        p = ProductHelper.findProduct(context, "4800010075069", "630", false);
        System.out.println(p.getProdName() + " - " + p.getSrp());
        assertEquals(p.getSrp(), 40.0);
        // search with SKU (no preferences while testing, though)
//        p = DatabaseHelper.findProduct(context, "Prod0014", "400", false);
//        System.out.println(p.getProdName() + " - " + p.getSrp());
//        assertEquals(p.getSrp(), 150.0);
//
//        p = DatabaseHelper.findProduct(context, "Prod0014", "630", false);
//        System.out.println(p.getProdName() + " - " + p.getSrp());
//        assertEquals(p.getSrp(), 140.5);


    }

    @Test
    public void testProcessCustWarehouse() throws IOException {
        String custWarehouse = "{\"custWarehouse\":[{\"customerId\":376,\"custWarehouseId\":362,\"warehouseName\":\"4123\",\"status\":\"A\"},{\"customerId\":630,\"custWarehouseId\":564,\"warehouseName\":\"123\",\"status\":\"A\"}]}";
        ObjectMapper mapper = new ObjectMapper();
        UserData data = mapper.readValue(custWarehouse, UserData.class);

        DatabaseHelper.processData(context, data);
        System.out.println("Processing customer warehouse...");

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long storedRows = DatabaseUtils.queryNumEntries(db, "cust_warehouse", null, null);
        assertEquals(data.getCustWarehouse().length, storedRows);
        db.close();

    }

    @Test
    public void testGetCustWarehouse() throws IOException {
        testProcessCustWarehouse();
        String custId = "376";
        CustWarehouse[] custWhList = CustomerHelper.getCustWh(context, custId);

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long storedRows = DatabaseUtils.queryNumEntries(db, "cust_warehouse", "customer_id=?", new String[]{custId});
        assertEquals(storedRows, custWhList.length);
    }

    @Test
    public void testCreateUser() throws IOException {
        String userStr = "{\"appUserId\":2,\"userId\":286,\"userName\":\"japs\",\"email\":\"t1422309@mvrht.net\",\"password\":\"8a61d71f254a4f7a9e5baf631824d50f3516e687cf76d5dc0519d99225ed9642\",\"passwordSalt\":\"dPzpVm\",\"serverUrl\":\"http://192.168.2.85:14697/simpleerp\"}";
        ObjectMapper mapper = new ObjectMapper();
        User serverUser = mapper.readValue(userStr, User.class);

        UserHelper.createUser(context, serverUser);
        User user = UserHelper.searchUser(context, serverUser.getUserName());
        assertEquals(serverUser.getUserId(), user.getUserId());
    }

    @Test
    public void testProcessPriceListOption() throws IOException{
        String priceListOpt = "{\"pricelistopt\":[{\"priceListId\":630,\"priceListName\":\"New Test Customer Price List\"},{\"priceListId\":243,\"priceListName\":\"Price List\"},{\"priceListId\":244,\"priceListName\":\"Price List 2\"}],\"pricelistdet\":[{\"effectivePrice\":12,\"priceListId\":244,\"prodId\":376},{\"effectivePrice\":12,\"priceListId\":244,\"prodId\":374},{\"effectivePrice\":12,\"priceListId\":244,\"prodId\":375},{\"effectivePrice\":15,\"priceListId\":243,\"prodId\":375}]}";
        ObjectMapper mapper = new ObjectMapper();
        UserData data = mapper.readValue(priceListOpt, UserData.class);

        assertNotNull(data.getPricelistopt());

        DatabaseHelper.processData(context, data);
        System.out.println("Processing pricelist options...");

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long storedRows = DatabaseUtils.queryNumEntries(db, "price_list_option", null, null);
        assertEquals(data.getPricelistopt().length, storedRows);
        storedRows = DatabaseUtils.queryNumEntries(db, "price_list_det", null, null);
        assertEquals(data.getPricelistdet().length, storedRows);
        db.close();
    }

    @Test
    public void testGetPricelistOptions() throws IOException{
        testProcessPriceListOption();
        PriceListOption[] options = PriceListHelper.getPricelistOptions(context);
        for(PriceListOption option : options)
            System.out.println(option);

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long storedRows = DatabaseUtils.queryNumEntries(db, "price_list_option", null, null);
        assertEquals(options.length, storedRows);
    }

    @Test
    public void testFindProdPrice() throws IOException{
        testProcessPriceListOption();

        Double price = PriceListHelper.findProductPrice(context, "376", "244");
        System.out.println("Price for 376 in PL 244: " + price);
        assertEquals(price, 12.0);

        price = PriceListHelper.findProductPrice(context, "375", "243");
        System.out.println("Price for 376 in PL 244: " + price);
        assertEquals(price, 15.0);

        //pricelist doesn't exist
        price = PriceListHelper.findProductPrice(context, "376", "245");
        assertNull(price);

        //product doesn't exist
        price = PriceListHelper.findProductPrice(context, "374", "245");
        assertNull(price);
    }

    @Test
    public void testFindPriceList() throws IOException {
        testProcessPriceListOption();

        String id = PriceListHelper.findPriceListId(context, "New Test Customer Price List");
        assertEquals(id, "630");

        id = PriceListHelper.findPriceListId(context, "Non-existing pricelist");
        assertNull(id);
    }

    @Test
    public void testProcessDelivery() throws IOException{
        String delStr = "{\"delivery\":[{\"deliveryId\":7702,\"status\":\"T\",\"deliveryNo\":\"2787\",\"deliveryType\":\"O\"},{\"deliveryId\":7759,\"status\":\"A\",\"deliveryNo\":\"2844\",\"deliveryType\":\"O\"}],\"deliverydet\":[{\"qtyApprovedTotal\":1,\"qtyDoneTotal\":0,\"deliveryId\":7702,\"deliveryDetId\":36492,\"prodId\":1029},{\"qtyApprovedTotal\":1,\"qtyDoneTotal\":1,\"deliveryId\":7759,\"deliveryDetId\":36713,\"prodId\":913},{\"qtyApprovedTotal\":3,\"qtyDoneTotal\":3,\"deliveryId\":7759,\"deliveryDetId\":36714,\"prodId\":1029},{\"qtyApprovedTotal\":2,\"qtyDoneTotal\":2,\"deliveryId\":7759,\"deliveryDetId\":36715,\"prodId\":1053}]}";
        ObjectMapper mapper = new ObjectMapper();
        UserData data = mapper.readValue(delStr, UserData.class);

        assertNotNull(data.getDelivery());
        for(Delivery d : data.getDelivery()){
            System.out.println(d.getRecordNo());
        }
        assertNotNull(data.getDeliverydet());

        try {
            DatabaseHelper.processData(context, data);
            System.out.println("Processing deliveries...");
        }catch (Exception e){
            e.printStackTrace();
        }

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long storedRows = DatabaseUtils.queryNumEntries(db, "delivery", null, null);
        assertEquals(data.getDelivery().length, storedRows);
        storedRows = DatabaseUtils.queryNumEntries(db, "delivery_det", null, null);
        assertEquals(data.getDeliverydet().length, storedRows);
    }


    @Test
    public void testProcessDeliveryWithUpdate() throws IOException {
        testProcessDelivery();
        String delStr = "{\"delivery\":[{\"deliveryId\":7702,\"status\":\"X\",\"deliveryNo\":\"2787\"}]}";
        ObjectMapper mapper = new ObjectMapper();
        UserData data = mapper.readValue(delStr, UserData.class);

        assertNotNull(data.getDelivery());

        try {
            DatabaseHelper.processData(context, data);
            System.out.println("Re-Processing deliveries...");
        }catch (Exception e){
            e.printStackTrace();
        }

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long storedRows = DatabaseUtils.queryNumEntries(db, "delivery", null, null);
        assertEquals(1, storedRows);
    }

    @Test
    public void testDeleteDelivery() throws IOException{
        testProcessDelivery();

        DeliveryHelper.deleteDelivery(context, "7702");

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long storedRows = DatabaseUtils.queryNumEntries(db, "delivery", "delivery_id=7702", null);
        assertEquals(0, storedRows);
        storedRows = DatabaseUtils.queryNumEntries(db, "delivery_det", "delivery_id=7702", null);
        assertEquals(0, storedRows);
    }


    @Test
    public void testGetUnverifiedDeliveries() throws IOException{
        testProcessDelivery();
        Delivery[] delive = new Delivery[0];
        try {
            delive = DeliveryHelper.getUnverifiedDeliveries(context);
        } catch (Exception e){
            e.printStackTrace();
        }

        for(Delivery del : delive) {
            System.out.println(del + ": " + del.getIsVerified());
            assertNotNull(del.getDets());
        }

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long storedRows = DatabaseUtils.queryNumEntries(db, "delivery", "is_verified='N'", null);
        assertEquals(delive.length, storedRows);
    }

    @Test
    public void testSetDeliveryToVerified() throws IOException {
        testProcessDelivery();
        String delId = "7759";

        DeliveryHelper.setDeliveryVerifyStatus(context, delId, "Y");
        String purchase_remarks = "SELECT is_verified from delivery where delivery_id=?";
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor c = db.rawQuery(purchase_remarks, new String[]{delId});
        if (c.moveToFirst()) {
            String isValidated = c.getString( c.getColumnIndex("is_verified"));
            assertEquals("Y", isValidated);
        }
        db.close();

        DeliveryHelper.setDeliveryVerifyStatus(context, delId, "N");
        db = dbHelper.getReadableDatabase();
        c = db.rawQuery(purchase_remarks, new String[]{delId});
        if (c.moveToFirst()) {
            String isValidated = c.getString( c.getColumnIndex("is_verified"));
            assertEquals("N", isValidated);
        }
        db.close();
    }

    @Test
    public void testSetDeliveriesToUploaded() throws IOException {
        testProcessDelivery();
        String[] delId = new String[]{"7759", "7702"};
        DeliveryHelper.setDeliveriesAsUploaded(context, delId);

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String purchase_remarks = "SELECT for_upload from delivery where delivery_id in (?,?)";
        Cursor c = db.rawQuery(purchase_remarks, new String[]{"7759", "7702"});
        if (c.moveToFirst()) {
            String forUpload = c.getString(c.getColumnIndex("for_upload"));
            assertEquals("N", forUpload);
        }
        db.close();
    }

    @Test
    public void testSetDoneQtyOfDelivery() throws IOException {
        testProcessDelivery();

        String delId = "7759";
        ArrayList<ScannedDelItem> list = new ArrayList<>();
        ScannedDelItem item = new ScannedDelItem();
        item.setId("1053");
        item.setQtyDone(2);
        list.add(item);
        item = new ScannedDelItem();
        item.setId("913");
        item.setQtyDone(2);
        list.add(item);
        DeliveryHelper.setDoneQtyOfDelivery(context, delId, list);

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String purchase_remarks = "SELECT qty_done_total, prod_id from delivery_det where delivery_id=?";
        Cursor c = db.rawQuery(purchase_remarks, new String[]{delId});
        if (c.moveToFirst()) {
            String prodId = c.getString(c.getColumnIndex("prod_id"));
            int isValidated = c.getInt(c.getColumnIndex("qty_done_total"));
            if(prodId.equals("1053")) {
                assertEquals(2, isValidated);
            } else if (prodId.equals("913")){
                assertEquals(2, isValidated);
            }
        }
        db.close();

    }

    @Test
    public void testSetUserDl() {
        User u = new User();
        u.setUserId("1");
        u.setUserName("admin");
        UserHelper.createUser(context, u);

        String dlTime = Long.toString(System.currentTimeMillis());
        System.out.println("Current time: " + dlTime);

        UserHelper.updateLastDlTime(context, dlTime);
        assertEquals(dlTime, UserHelper.getLastDlTime(context));
    }

    @Test
    public void testProcessSerial() throws IOException{
        String serialStr = "{\"serial\":[{\"prodId\":687,\"serialNo\":\"3565sad\",\"status\":\"I\"},{\"prodId\":1031,\"serialNo\":\"21e321\",\"status\":\"I\"},{\"prodId\":1031,\"serialNo\":\"asd\",\"status\":\"I\"},{\"prodId\":1115,\"serialNo\":\"32\",\"status\":\"I\"}]}";
        ObjectMapper mapper = new ObjectMapper();
        UserData data = mapper.readValue(serialStr, UserData.class);

        assertNotNull(data.getSerial());
        for(Serial s : data.getSerial()){
            System.out.println(s);
        }

        try {
            DatabaseHelper.processData(context, data);
            System.out.println("Processing serial numbers...");
        }catch (Exception e){
            e.printStackTrace();
        }

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long storedRows = DatabaseUtils.queryNumEntries(db, "serial", null, null);
        assertEquals(data.getSerial().length, storedRows);
    }

    @Test
    public void testSearchSerial() throws IOException {
        String response = "{\"product\":[{\"srp\":12,\"bala\":null,\"baseUnit\":\"12\",\"status\":\"A\",\"prodName\":\"Barad123213\",\"prodId\":687,\"sku\":\"sdfsdfsdf123123123\",\"barcode\":\"4800001000100\",\"packagingId\":1030,\"status\":\"A\"},{\"srp\":105,\"baseUnit\":\"12\",\"prodName\":\"1233\",\"prodId\":684,\"sku\":\"1223\",\"barcode\":\"4802122001431\",\"packagingId\":1051,\"status\":\"A\"}]}";
        parseProducts(context, response);
        testProcessSerial();

        String serialNo = "3565sad";
        Product p = SerialHelper.findSerial(context, serialNo);
        assertNotNull(p);
        System.out.println(p);

        serialNo = "asd";
        p = SerialHelper.findSerial(context, serialNo);
        assertNull(p);
    }

    @Test
    public void testAddLog() {
        LogHelper.addLog(context, "Log #1");
        int rowsInserted = 1;

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long storedRows = DatabaseUtils.queryNumEntries(db, "log", null, null);
        assertEquals(rowsInserted, storedRows);
    }

    @Test
    public void testAddLogWhenProcessing() throws IOException {
        testProcessCustomer();
        String response = "{\"customer\":[{\"partnerName\":\"*()&*(#$@A@!\",\"priceListId\":530,\"partnerId\":630,\"status\":\"A\"},{\"partnerName\":\"Snack Vision\",\"priceListId\":530,\"partnerId\":400,\"status\":\"I\"}]}";
        ObjectMapper mapper = new ObjectMapper();
        UserData data = mapper.readValue(response, UserData.class);
        DatabaseHelper.processData(context, data);

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long storedRows = DatabaseUtils.queryNumEntries(db, "log", null, null);
        assertEquals(1, storedRows);
    }

    @Test
    public void testProcessInvBatch() throws IOException{
        String serialStr = "{\"inventoryBatch\":[{\"prodId\":687,\"qtyRemaining\":1,\"inventoryBatchNo\":\"GEN2030\"},{\"prodId\":687,\"qtyRemaining\":2,\"inventoryBatchNo\":\"GEN1864\"}]}";
        ObjectMapper mapper = new ObjectMapper();
        UserData data = mapper.readValue(serialStr, UserData.class);

        assertNotNull(data.getInventoryBatch());
        for(InventoryBatch s : data.getInventoryBatch()){
            System.out.println(s);
        }

        try {
            DatabaseHelper.processData(context, data);
            System.out.println("Processing inventory batches...");
        }catch (Exception e){
            e.printStackTrace();
        }

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long storedRows = DatabaseUtils.queryNumEntries(db, "inventory_batch", null, null);
        assertEquals(data.getInventoryBatch().length, storedRows);
    }

    @Test
    public void testGetBatch() throws IOException {
        testProcessInvBatch();

        Product p = new Product();
        p.setProdId("687");
        InvBatchHelper.getBatch(context, p);

        assertNotNull(p.getBatch());


    }


}

