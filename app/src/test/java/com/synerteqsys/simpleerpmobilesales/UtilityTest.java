package com.synerteqsys.simpleerpmobilesales;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by dev06 on 28/03/2018.
 */

@RunWith(RobolectricTestRunner.class)
public class UtilityTest {

    @Test
    public void testGetUser() throws IOException {
        String functions = "{\"functions\":[\"price_search\", \"new_so\", \"new_po\", \"new_ds\", \"delivery_verify\"]}";
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = mapper.readValue(functions, new TypeReference<Map<String,Object>>(){});
        ArrayList<String> vals = (ArrayList)map.get("functions");
        for(String i : vals){
            System.out.println(i);
        }
    }
}
