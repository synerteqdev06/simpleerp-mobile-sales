package com.synerteqsys.simpleerpmobilesales;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Button;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.synerteqsys.simpleerpmobilesales.activities.LoginActivity;
import com.synerteqsys.simpleerpmobilesales.data.UserPreference;
import com.synerteqsys.simpleerpmobilesales.data.models.User;
import com.synerteqsys.simpleerpmobilesales.utils.ActionDoneListener;
import com.synerteqsys.simpleerpmobilesales.utils.Constants;
import com.synerteqsys.simpleerpmobilesales.utils.database.DatabaseHelper;
import com.synerteqsys.simpleerpmobilesales.utils.LoginTask;
import com.synerteqsys.simpleerpmobilesales.utils.database.UserHelper;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.shadows.ShadowToast;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Created by dev06 on 03/01/2018.
 */

@RunWith(RobolectricTestRunner.class)
public class LoginActivityTest {
    private LoginActivity login;
    private Resources resource;
    private Context c;

    @Before
    public void setUp() throws Exception {
        login = Robolectric.buildActivity(LoginActivity.class)
                .create()
                .resume()
                .get();
        resource = login.getResources();
        c = RuntimeEnvironment.application;
    }

    @Test
    public void login_no_value() throws Exception {
        Button button = login.findViewById(R.id.sign_in_button);
        assertNotNull(button);
        button.performClick();
        String expectedStr = resource.getString(R.string.username_password_mandatory);
        assertThat(ShadowToast.getTextOfLatestToast(), equalTo(expectedStr));
    }

    @Test
    public void testLogin() {
        String uname = "japs";
        String pword = "Japs123@";
        login.logIn(uname, pword);
        assertTrue(UserPreference.getBoolPreference(c, Constants.LOGGED_IN));

        User u =  UserHelper.searchUser(RuntimeEnvironment.application, uname);
        Assert.assertNotNull(u);
        System.out.println("User ID: " + u.getUserId());

    }

    @Ignore
    @Test
    public void testNoCapsLogin() {
        String uname = "JapS";
        String pword = "Japs123@";
        login.logIn(uname, pword);
        assertTrue(UserPreference.getBoolPreference(c, Constants.LOGGED_IN));

        User u =  UserHelper.searchUser(RuntimeEnvironment.application, uname);
        Assert.assertNotNull(u);
        System.out.println("User ID: " + u.getUserId());

    }

    @Test
    public void testEmailLogin() {
        String uname = "t1422309@mvrht.net";
        String pword = "Japs123@";
        login.logIn(uname, pword);
        assertTrue(UserPreference.getBoolPreference(c, Constants.LOGGED_IN));

        User u =  UserHelper.searchUser(RuntimeEnvironment.application, uname);
        Assert.assertNotNull(u);
        System.out.println("User ID: " + u.getUserId());

    }

    @Ignore
    @Test
    public void testLoginTaskNoUserNoAuth(){
        String username = "japs";
        String pword = "Japs123@";
        //final User loginUser = user;
        //log in user in their designated server
        LoginTask task = new LoginTask(login, username, pword);
        task.setPostActionListener(new ActionDoneListener() {
            @Override
            public void handleDone(Object o) {
                System.out.println("DONE");
                assertFalse(UserPreference.getBoolPreference(c, Constants.LOGGED_IN));
                assertEquals(Constants.OFFLINE, UserPreference.getStringPreference(c, Constants.MODE));
            }
        });
        task.setProgressMessage(login.getString(R.string.login_progress));
        try {
            task.execute().get();
        } catch (InterruptedException | ExecutionException e) {
            System.out.println(e.getStackTrace());
        }
    }

    @Ignore
    @Test
    public void testLoginTaskWithUserInvLisNoAuth() throws IOException {
        User user = createUser();
        setLicenseDt(user, "1000118400000");

        String username = "japs";
        String pword = "Japs123@";
        //final User loginUser = user;
        //log in user in their designated server
        LoginTask task = new LoginTask(login, username, pword);
        task.setPostActionListener(new ActionDoneListener() {
            @Override
            public void handleDone(Object o) {
                System.out.println("DONE");
                assertFalse(UserPreference.getBoolPreference(c, Constants.LOGGED_IN));
                assertEquals(Constants.OFFLINE, UserPreference.getStringPreference(c, Constants.MODE));
            }
        });
        task.setProgressMessage(login.getString(R.string.login_progress));
        try {
            task.execute().get();
        } catch (InterruptedException | ExecutionException e) {
            System.out.println(e.getStackTrace());
        }
    }

    @Test
    public void testLoginTaskOnlineWithUser() throws IOException {
        createUser();

        String username = "japs";
        String pword = "Japs123@";
        //final User loginUser = user;
        //log in user in their designated server
        LoginTask task = new LoginTask(login, username, pword);
        task.setPostActionListener(new ActionDoneListener() {
            @Override
            public void handleDone(Object o) {
                System.out.println("DONE");
                assertEquals(Constants.ONLINE, UserPreference.getStringPreference(c, Constants.MODE));
                assertTrue(Constants.ONLINE, UserPreference.getBoolPreference(c, Constants.LOGGED_IN));
            }
        });
        task.setProgressMessage(login.getString(R.string.login_progress));
        try {
            task.execute().get();
        } catch (InterruptedException | ExecutionException e) {
            System.out.println(e.getStackTrace());
        }
    }

    @Test
    public void testLoginTaskOnlineWithUserNoLisence() throws IOException {
        User user = createUser();
        setLicenseDt(user, "1000118400000");

        String username = "japs";
        String pword = "Japs123@";
        //final User loginUser = user;
        //log in user in their designated server
        LoginTask task = new LoginTask(login, username, pword);
        task.setPostActionListener(new ActionDoneListener() {
            @Override
            public void handleDone(Object o) {
                System.out.println("DONE");
                assertEquals(Constants.ONLINE, UserPreference.getStringPreference(c, Constants.MODE));
                assertTrue(UserPreference.getBoolPreference(c, Constants.LOGGED_IN));
            }
        });
        task.setProgressMessage(login.getString(R.string.login_progress));
        try {
            task.execute().get();
        } catch (InterruptedException | ExecutionException e) {
            System.out.println(e.getStackTrace());
        }
    }

    @Test
    public void testLoginTaskOnlineNoUser(){
        String username = "japs";
        String pword = "Japs123@";
        //final User loginUser = user;
        //log in user in their designated server
        LoginTask task = new LoginTask(login, username, pword);
        task.setPostActionListener(new ActionDoneListener() {
            @Override
            public void handleDone(Object o) {
                System.out.println("DONE");
                assertEquals(Constants.ONLINE, UserPreference.getStringPreference(c, Constants.MODE));
                assertTrue(Constants.ONLINE, UserPreference.getBoolPreference(c, Constants.LOGGED_IN));
            }
        });
        task.setProgressMessage(login.getString(R.string.login_progress));
        try {
            task.execute().get();
        } catch (InterruptedException | ExecutionException e) {
            System.out.println(e.getStackTrace());
        }
    }



    private User createUser() throws IOException {
        String userJson = "{\"userId\":286,\"userName\":\"japs\",\"password\":\"8a61d71f254a4f7a9e5baf631824d50f3516e687cf76d5dc0519d99225ed9642\",\"passwordSalt\":\"dPzpVm\",\"serverUrl\":\"http://192.168.2.85:14697/simpleerp\"}";
        ObjectMapper userMapper = new ObjectMapper();
        User user = userMapper.readValue(userJson, User.class);
        UserHelper.createUser(c, user);
        return user;
    }

    private void setLicenseDt(User user, String licenseDt){
        DatabaseHelper helper = DatabaseHelper.getInstance(c);
        try(SQLiteDatabase db = helper.getReadableDatabase() ) {
            db.beginTransaction();
            ContentValues cv = new ContentValues();
            cv.put("last_license_dt", licenseDt);
            db.update("users", cv, "username=?", new String[]{user.getUserName()});
            db.setTransactionSuccessful();
            db.endTransaction();
        }
    }
}
